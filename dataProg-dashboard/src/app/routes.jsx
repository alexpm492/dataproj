import AuthGuard from 'app/auth/AuthGuard';
import dashboardRoutes from 'app/views/dashboard/DashboardRoutes';
import usersRoutes from 'app/views/users/UsersRoutes';
import profilesRoutes from 'app/views/profiles/ProfilesRoutes';
import NotFound from 'app/views/sessions/NotFound';
import sessionRoutes from 'app/views/sessions/SessionRoutes';
import { Navigate } from 'react-router-dom';
import MatxLayout from './components/MatxLayout/MatxLayout';
import reportsRoutes from './views/reports/ReportsRoutes';
import portalRoutes from './views/portal/PortalRoutes';
import consultingRoutes from './views/consulting/ConsultingRoutes';
import downloadRoutes from './views/downloads/DownloadsRoutes';

const routes = [
  {
    element: (
      <AuthGuard>

        <MatxLayout />
      </AuthGuard>
    ),
    children: [...dashboardRoutes, ...usersRoutes, ...profilesRoutes, ...reportsRoutes, ...portalRoutes, ...consultingRoutes, ...downloadRoutes],
  },
  ...sessionRoutes,
  { path: '/', element: <Navigate to="dashboard/default" /> },
  { path: '*', element: <NotFound /> },
];

export default routes;
