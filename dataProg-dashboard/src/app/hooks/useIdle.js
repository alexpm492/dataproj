import { useContext, useState } from "react"
import { useIdleTimer } from "react-idle-timer"
import useAuth from "./useAuth";


const useIdleTimeout = ({ onIdle, idleTime = 1,debounce =10}) => {
  const { logout } = useAuth();
    const idleTimeout = 1000 * idleTime;
    const [isIdle, setIdle] = useState(false)
   
    const handleIdle = () => {
        setIdle(true)
        logout()
    }
    const idleTimer = useIdleTimer({
        timeout: idleTimeout,
        promptTimeout: idleTimeout / 2,
        onPrompt: onIdle,
        onIdle: handleIdle,
        debounce: 1000 * debounce
    })
    return {
        isIdle,
        setIdle,
        idleTimer
    }
    
}
export default useIdleTimeout;