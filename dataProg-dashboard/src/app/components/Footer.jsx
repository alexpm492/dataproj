import { AppBar, ThemeProvider, Toolbar, Typography } from '@mui/material';
import { styled, useTheme } from '@mui/system';
import useSettings from 'app/hooks/useSettings';
import { topBarHeight } from 'app/utils/constant';


const AppFooter = styled(Toolbar)(() => ({
  display: 'flex',
  alignItems: 'center',
  minHeight: topBarHeight,
  '@media (max-width: 499px)': {
    display: 'table',
    width: '100%',
    minHeight: 'auto',
    padding: '1rem 0',
    '& .container': {
      flexDirection: 'column !important',
      '& a': { margin: '0 0 16px !important' },
    },
  },
}));

const FooterContent = styled('div')(() => ({
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  padding: '0px 1rem',
  maxWidth: '1170px',
  margin: '0 auto',
}));

const Footer = () => {
  const theme = useTheme();
  const { settings } = useSettings();

  const footerTheme = settings.themes[settings.footer.theme] || theme;

  return (
    <ThemeProvider theme={footerTheme}>
      <AppBar sx={{backgroundColor:'#3b3d3e', zIndex: 96}} position="static" >
        <AppFooter>
          <FooterContent>
            
            <Typography sx={{alignContent:'center',marginLeft: '5%'}} >
            Copyright © 2024 <a href="https://mantelcoa.com/" target='_blank'>Mantelcoa</a> (02) 603-7589 - 098-722-5112 / <a href="mailto: info@mantelcoa.com">info@mantelcoa.com</a> - <a href="mailto: sistemas@mantelcoa.com">sistemas@mantelcoa.com</a>  |  Desarrollado por: <a href='https://www.codedesign.ec/' target='_blank'>Codedesign S.A</a>
            </Typography>
          </FooterContent>
        </AppFooter>
      </AppBar>
    </ThemeProvider>
  );
};

export default Footer;
