export const navigations = [
  { name: 'INICIO', path: '/dashboard/default', icon: 'home', show: false, toMenu: true },
  {
    name: 'ADMINISTRADOR',
    icon: 'settings_suggest',
    children: [
      { name: 'Perfil', iconText: 'SI', path: '/perfiles', icon: 'double_arrow', show: false, toMenu: true, },
      { name: 'Nuevo Perfil', path: '/perfiles/nuevo', show: false, toMenu: false, },
      { name: 'Usuarios', iconText: 'SI', path: '/usuarios', icon: 'people', show: false, toMenu: true, },
      { name: 'Nuevo Usuario', path: '/usuarios/nuevo', show: false, toMenu: false, },
      { name: 'Ingresos',iconText: 'SI', path: '/ingresos', icon: 'how_to_reg', show: false, toMenu: true, },
      { name: 'Instituciones', iconText: 'SI', path: '/portal/nuevaInstitucion', icon: 'account_balance', show: false, toMenu: true, },
    ],
    show: false,
    toMenu: true,
  },
  {
    name: 'PORTAL',
    icon: 'dashboard',
    children: [
      { name: 'Tipo de Archivos', iconText: 'SI', path: '/portal/tiposArchivos', icon: 'double_arrow', show: false, toMenu: true, },
      { name: 'Nuevo Archivo', path: '/portal/nuevoArchivo', show: false, toMenu: false, },
      { name: 'Nuevo Grupo', iconText: 'SI', path: '/portal/nuevoGrupo', icon: 'double_arrow', show: false, toMenu: true, },
      
      { name: 'Usuario Archivo', iconText: 'SI', path: '/portal/usuarios', icon: 'double_arrow', show: false, toMenu: true, },
      { name: 'Reporte Descargas', iconText: 'SI', path: '/portal/reporteDescargas', icon: 'double_arrow', show: false, toMenu: true, },
    ],
    show: false,
    toMenu: true,
  },
  {
    name: 'CARGA CONSULTAS',
    icon: 'list',
    children: [

      { name: 'Carga Insumo SEPS', iconText: 'SI', path: '/consultas/cargarInsumoSEPS', icon: 'double_arrow', show: false, toMenu: true, },
      { name: 'Carga Insumo SB', iconText: 'SI', path: '/consultas/cargarInsumoSB', icon: 'double_arrow', show: false, toMenu: true, },
      { name: 'Cargar Providencias SEPS', iconText: 'SI', path: '/consultas/cargarProvidenciaSEPS', icon: 'double_arrow', show: false, toMenu: true, },
      { name: 'Cargar Providencias SB', iconText: 'SI', path: '/consultas/cargarProvidenciaSB', icon: 'double_arrow', show: false, toMenu: true, },
      { name: 'Reporte Providencias', iconText: 'SI', path: '/consultas/providenciasJudiciales', icon: 'double_arrow', show: false, toMenu: true, },
    ],
    show: false,
    toMenu: true,
    path: '/consultas'
  },
  
  { name: 'DESCARGA', path: '/descargas/default', icon: 'download', show: false, toMenu: true, },
  { name: 'CONSULTA SB', path: '/consultaSB', icon: 'search', show: false, toMenu: true, },
  { name: 'CONSULTA SEPS', path: '/consultaSEPS', icon: 'search', show: false, toMenu: true, },
  { name: 'SESION', path: '/sesion', icon: 'manage_accounts', show: false, toMenu: true, },
];
