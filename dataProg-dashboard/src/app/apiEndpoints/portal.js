//ReportDownloads
export const DOWNLOAD_LIST = 'reportDownloads';
export const TYPEFILE_LIST = 'portalFiles';

// Portal Files
export const CREATE_FILE = 'portalFiles';
export const UPLOAD_FILE = 'portalFiles/details';
export const GET_FILE = (id) =>(`portalFiles/${id}`);
export const DETAIL_FILE = (id) => (`portalFiles/details/${id}`);
export const DAY_DETAIL_FILE = (id) =>(`portalFiles/days/details/${id}`);
export const USER_FILE = 'userFiles';
export const CURRENT_USER_FILE = (id)=>(`userFiles/${id}`);
export const GRANT_FILE = (id) => (`portalFiles/${id}`);

//Reports
export const PROV_SEPS = 'providenceSEPS';
export const PROV_SEPS_DOWNLOAD = (id) => (`providenceSEPS/store/${id}`);
export const PROV_SB = 'providenceSB';
export const PROV_SB_DOWNLOAD = (id) => (`providenceSB/store/${id}`);
export const PROV_SB_STORE = 'providenceSB/store';
export const PROV_SB_SYNC = 'providenceSB/sync';
export const PROV_REPORT = 'supplies';
export const PROV_CONTROL_SEPS = (id) => (`providenceSEPS/control/${id}`);
export const PROV_CONTROL_SB = (id) => (`providenceSB/control/${id}`);

//downloads
export const ALLOWED_FILES_LIST = 'userFiles/list/report';
export const PENDING_FILE_LIST = 'userFiles/user/report';
export const ALLOED_FILE_DOWNLOAD = (id) => (`portalFiles/download/${id}`);

//group
export const GROUP = 'group';
export const EDIT_GROUP = (id) => (`group/${id}`);

//institutioncs
export const INSTITUTION = 'institution';
//user
export const USER = 'user';
export const USERS = 'users';
export const PROFILE = 'profile';
export const PROFILES = 'profiles';
export const CHANGE_PASSWORD = 'changeUserPassword';
export const USER_LOG = 'users/login/report';


export const LOGIN = 'login';

//ip
export const PUBLIC_IP = 'https://api.ipify.org?format=json'