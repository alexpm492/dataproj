export const USER_LIST = 'dataForms/users';
export const FILE_LIST = 'dataForms/files';
export const GROUP_LIST = 'dataForms/groups';
export const INSTITUTION_LIST = 'dataForms/institutions';