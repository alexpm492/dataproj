import axios from 'axios.js'
export const getFormData = async (endPointList) => {
    try {
        const requests = endPointList.map(url => axios.get(url));
        const responses = await Promise.all(requests);

        const result = {};
        responses.forEach(({ request, data }) => {
            const url = request?.responseURL;
            if (url) {
                const property = url.split('/').pop();
                result[property] = data;
            }
        });

        return result;
    } catch (error) {
        throw error;
    }
}