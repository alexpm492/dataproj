import Loadable from 'app/components/Loadable';
import { lazy } from 'react';
import { authRoles } from '../../auth/authRoles';

const View = Loadable(lazy(() => import('./View')));

const downloadRoutes = [
    { path: '/descargas/default', element: <View />, auth: authRoles.admin },
];

export default downloadRoutes;
