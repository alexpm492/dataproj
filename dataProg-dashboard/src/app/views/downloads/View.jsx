import { Typography, Grid, TextField, CardContent, Box, Icon, TableRow, TableHead, TableCell, TableBody, Alert, Divider, ListItemIcon, ListItem, List, ListItemText, Button, ListSubheader, Snackbar, Table } from '@mui/material';
import { useState } from 'react';
import axios from 'axios.js'
import { saveAs } from "file-saver";
import { Container, CardRoot, CardTitle, BreadcrumbName, StyledTable, StyledTableContent, StyledTableRow } from '../core/styles';

import { ALLOED_FILE_DOWNLOAD, ALLOWED_FILES_LIST, PENDING_FILE_LIST } from 'app/apiEndpoints/portal';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import "dayjs/locale/es";
import dayjs from 'dayjs';
import { LoadingButton } from '@mui/lab';


const View = () => {
    const [data, setData] = useState(null);
    const [reportDownloads, setReportDownloads] = useState(null)


    const [form, setForm] = useState({
        values: {
            date: null
        },
        error: null,
        isUpdating: false,
        hasSnackbar: false,
        isLoading: false,
        isShowDialog: false,
        isUpdating: false
    });

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };


    const getReports = async () => {
        try {
            await axios.get(PENDING_FILE_LIST, {
                params: {
                    ...form?.values

                }
            })
                .then((response) => {
                    const { status, data } = response;

                    if (status === 200) {
                        if (Array.isArray(data?.data)) {
                            if (data?.data.length > 0) {
                                setReportDownloads(data?.data);
                            } else {
                                setReportDownloads(null);
                            }
                        }
                    }
                })
        } catch (_) {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Ocurrio un error al obtener lista de archivos pendientes',
                hasSnackbar: true
            }))
        }
    }


    const getData = async () => {
        setForm(prevForm => ({
            ...prevForm,
            isLoading: !prevForm.isLoading
        }));
        if (reportDownloads !== null) setReportDownloads(null);
        await axios.get(ALLOWED_FILES_LIST, {
            params: {
                ...form?.values

            }
        }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                if (Array.isArray(data?.data)) {
                    if (data?.data.length > 0) {
                        setData(data?.data);
                    } else {
                        setData([]);
                        setForm(prevForm => ({
                            ...prevForm,
                            error: 'info|Sin datos!',
                            hasSnackbar: true
                        }));

                    }
                } else {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'error|Error al mostrar los datos!',
                        hasSnackbar: true
                    }))
                }




            } else if (status > 404) {

                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error en el servidor al obtener información, intentalo más tarde!',
                    hasSnackbar: true
                }))
            }
        }).catch(({ message }) => {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        }).finally(async () => {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: !prevForm.isLoading
            }))

            await getReports();
        })


    }

    const handleDownload = async (id, nameFile) => {
        try {
            setForm(prevForm => ({
                ...prevForm,
                error: `info|Descargando archivo....`,
                hasSnackbar: true,
                isUpdating: true
            }))
            const rs = await axios.get(ALLOED_FILE_DOWNLOAD(id), {
                responseType: "arraybuffer",
            });
            const blob = new Blob([rs.data], { type: "octet/stream" });

            saveAs(blob, nameFile);
            getReports();
        } catch ({ message }) {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error al descargar el archivo, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        } finally {
            setForm(prevForm => ({
                ...prevForm,

                isUpdating: false
            }))
        }
    }
    return (
        <>
            <LocalizationProvider

                dateAdapter={AdapterDayjs} adapterLocale="es" >
                <Container>
                    <Box className="breadcrumb">
                        <BreadcrumbName>Descarga de Archivos</BreadcrumbName>
                    </Box>

                    <CardRoot elevation={6}>


                        <CardTitle>

                            <Grid container spacing={6}>
                                <Grid item xs={2} >
                                    <strong>Fecha Archivo</strong>
                                </Grid>
                                <Grid item xs={3}>
                                    <DatePicker
                                    autoFocus
                                        onChange={(value) => setForm(prevForm => ({
                                            ...prevForm,
                                            values: {
                                                ...prevForm.values,
                                                date: value.format('YYYY-MM-DD')
                                            }
                                        }))}
                                        value={dayjs(form?.values?.date)}
                                        format="DD/MM/YYYY"
                                        slotProps={{ textField: { size: 'small', fullWidth: true, } }}
                                        label="Fecha"
                                        name="dateBegin"
                                    />
                                </Grid>
                                <Grid item>
                                    <LoadingButton loading={form?.isLoading} variant="contained" color="primary" type="button" onClick={async () => {
                                        if (form?.values?.date !== null) {
                                            await getData();
                                        } else {
                                            setForm(prevForm => ({
                                                ...prevForm,
                                                error: 'error|Debes ingresar una fecha',
                                                hasSnackbar: true
                                            }))
                                        }
                                    }}>
                                        Aceptar
                                    </LoadingButton>
                                </Grid>


                            </Grid>


                        </CardTitle>
                        {
                            reportDownloads !== null && reportDownloads?.length > 0 ? (
                                <>
                                    <Divider />
                                    <CardTitle sx={{ my: 4 }}>
                                        <Alert severity="warning">

                                            <List sx={{ mb: 6 }} subheader={
                                                <ListSubheader component="div" id="nested-list-subheader" sx={{ mb: 4 }}>
                                                    Archivos pendientes de descarga
                                                </ListSubheader>
                                            } >
                                                {
                                                    reportDownloads?.map((r, ix) => (

                                                        <ListItem key={ix + 'l'} disablePadding >

                                                            <ListItemIcon>
                                                                <Icon>description</Icon>
                                                            </ListItemIcon>
                                                            <ListItemText primary={`El archivo ${r?.descripcion_arc} de la fecha: ${r?.fechaArchivo_dta} no ha sido descargada! `} />

                                                        </ListItem>
                                                    ))
                                                }

                                            </List>

                                        </Alert>
                                    </CardTitle>
                                    <Divider />
                                </>
                            ) : null
                        }



                    </CardRoot>
                </Container >
                {
                    form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                        <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                            {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                        </Alert>
                    </Snackbar>) : null
                }

            </LocalizationProvider>
            <Container >


                {
                    form?.values?.date == null ? (
                        <Alert severity="warning">
                            <Typography alignItems="center">
                                Selecciona una fecha
                            </Typography>
                        </Alert>
                    ) : null
                }

                {
                    data !== null && Array.isArray(data) && data?.length > 0 ? (
                       
                        <>
                        <CardRoot sx={{ mt: 4 }} >
                            {data?.map((ele, index) => (
                                <Grid container key={index} justifyContent="space-between" alignItems="center" spacing={2}>
                                  
                        <Typography variant="h6" gutterBottom sx={{mt:3, mb: 3}}>
                        {ele?.name}
                    </Typography>
                                    <Box sx={{ overflow: "auto" }}>
                                        <Box sx={{ width: "100%", display: "table", tableLayout: "fixed" }}>
                                            <StyledTableContent sx={{width: '100%'}}>
                                                <TableHead >
                                                    <TableRow>
                                                        <TableCell align="left">N.</TableCell>
                                                        <TableCell align="left">Nombre Archivo</TableCell>
                                                        <TableCell  align="left">Descripción</TableCell>
                                                        <TableCell align="left" >Fecha Carga</TableCell>
                                                        <TableCell align="left">Hora Carga</TableCell>
                                                        <TableCell   align="center">Acción</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {ele?.files?.map((file, index) => (
                                                        <TableRow key={index + file?.nombre_dta} hover>
                                                            <TableCell    align="left">{index + 1}</TableCell>
                                                            <TableCell  align="left">{file?.nombre_dta}</TableCell>
                                                            <TableCell align="left">{file?.descripcion_dta}</TableCell>
                                                            <TableCell   align="left">{file?.fecha_dta}</TableCell>
                                                            <TableCell  align="left">{file?.hora_dta}</TableCell>
                                                            <TableCell>

  
                                                                        <LoadingButton loading={form?.isUpdating} startIcon={<Icon>download</Icon>} size="small" variant="contained" color="warning" type="button" onClick={() => handleDownload(file?.id_dta, file?.nombre_dta)}>
                                                                            Descargar
                                                                        </LoadingButton>




                                                            </TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </StyledTableContent>
                                        </Box>
                                    </Box>
                                </Grid>



                            ))}
                        </CardRoot>
                        </>

                    ) : null
                }

                {
                    data !== null && Array.isArray(data) && data?.length === 0 ? (
                        <div style={{ width: '100%' }}>
                            <Alert severity="info">
                                <Typography alignItems="center" sx={{ width: '100%', textAlign: 'center' }}>
                                    Sin datos
                                </Typography>
                            </Alert>
                        </div>
                    ) : null
                }



            </Container>
        </>
    )
}

export default View;