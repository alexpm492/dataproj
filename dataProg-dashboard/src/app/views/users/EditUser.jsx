import {
  Grid,
  Icon,
  Box,
  CircularProgress,
  MenuItem,
  Snackbar,
  Button
} from "@mui/material";
import { Breadcrumb } from "app/components";
import { Span } from "app/components/Typography";
import { useEffect, useState } from "react";
import { ValidatorForm } from "react-material-ui-form-validator";
import axios from 'axios.js'
import { useParams } from 'react-router-dom';
import { LoadingButton } from '@mui/lab';
import { Container, TextField, AutoComplete, StyledButton, Alert, BreadcrumbName } from '../core/styles'
import { PROFILES, USER } from "app/apiEndpoints/portal";
import { GROUP_LIST, INSTITUTION_LIST } from "app/apiEndpoints/dataForms";
import { getFormData } from "app/apiEndpoints/requestUtil";
import { useNavigate } from "react-router-dom";
const statusList = ['Activo', 'Inactivo'];

const optionsSiNo = ['Si', 'No'];

const SimpleForm = () => {
  const { id } = useParams();
  const [state, setState] = useState({
    id: id,
    username: '',
    documentNumber: '',
    name: '',
    lastname: '',
    email: '',
    status: '',
    financialInstitution: '',
    portalDownload: '',
    consultingJudicialOrders: 'No',
    profile: '',
    group: ''
  });

  const [loading, setLoading] = useState(false);
  const [openProfile, setOpenProfile] = useState(false);
  const [profiles, setProfiles] = useState([]);
  const loadingProfiles = openProfile && profiles.length === 0;
  const [groups, setGroups] = useState(null);
  const [institutions, setInstitutions] = useState(null);
  const [openMessage, setOpenMessage] = useState(false);
  const navigate = useNavigate();
  const [error, setError] = useState(null);

  useEffect(() => {
    axios.get(USER + '/' + id)
      .then(res => {
        setState({
          ...state, username: res.data.username, documentNumber: res.data.documentNumber, name: res.data.name, lastname: res.data.lastname,
          email: res.data.email, status: res.data.status, financialInstitution: res.data.financialInstitution, portalDownload: res.data.portalDownload,
          consultingJudicialOrders: res.data.consultingJudicialOrders, profile: res.data.profile, group: res?.data?.group ?? 'Todos'
        });
      })
      .catch()
  }, [])

  const handleSubmit = async (values) => {
    setLoading(true);
    try {
      await axios.put(USER + '/' + id, state)
      setOpenMessage(true);
      setTimeout(() => {
        setLoading(false);
        navigate('/usuarios');
      }, 2000);
    } catch ({ message }) {
      setOpenMessage(true);
      setError(`error|${message ?? 'Ocurrio un error al actualizar el usuario, verifica los datos y vuelve a intentarlo'}`);
    } finally {
      setLoading(false);
    }
  };
  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenMessage(false);
  };
  useEffect(() => {
    getFormData([GROUP_LIST, INSTITUTION_LIST]).then((result) => {
      if (result?.groups && Array.isArray(result?.groups)) setGroups(result?.groups?.filter((g) => g?.name_group !== 'Default'));
      if (result?.institutions && Array.isArray(result?.institutions)) setInstitutions(result?.institutions);
    });
  }, [])

  const handleChange = (event) => {
    event.persist();
    setState({ ...state, [event.target.name]: event.target.value });
  };

  const handleChangeStatus = (newValue) => {
    if (newValue) {
      setState({ ...state, 'status': newValue });
    }
  };

  const handleChangePortal = (newValue) => {
    if (newValue) {
      setState({ ...state, 'portalDownload': newValue });
    }
  };

  const handleChangeConsulting = (newValue) => {
    if (newValue) {
      setState({ ...state, 'consultingJudicialOrders': newValue });
    }
  };

  const handleChangeProfile = (newValue) => {
    if (newValue) {
      setState({ ...state, 'profile_id': newValue.id });
    }
  };

  const {
    username,
    documentNumber,
    name,
    lastname,
    financialInstitution,
    email,
    status,
    portalDownload,
    consultingJudicialOrders,
    profile
  } = state;

  useEffect(() => {
    let active = true;

    if (!loadingProfiles) {
      return undefined;
    }

    (async () => {
      const response = await axios.get(PROFILES)
      if (active) {
        setProfiles(response.data?.data)
      }
    })();

    return () => {
      active = false;
    };
  }, [loadingProfiles]);

  useEffect(() => {
    if (!openProfile) {
      setProfiles([]);
    }
  }, [openProfile]);

  return (
    <Container>

<Box className="breadcrumb">
                <BreadcrumbName>Editar Usuario</BreadcrumbName>
            </Box>

      <ValidatorForm onSubmit={handleSubmit} onError={() => null}>
        <Grid container spacing={6}>
          <Grid item lg={3} md={3} sm={12} xs={12} sx={{ mt: 2 }}>
            <TextField
              type="text"
              name="username"
              id="standard-basic"
              disabled
              inputProps={{
                minLength: 4,
                maxLength: 15
              }}
              sx={{ textTransform: 'uppercase' }}
              value={username || ""}
              onChange={(e) => {
                const { name, value } = e.target;
                const newValue = value.replace(/[^a-zA-Z0-9]/g, '').toUpperCase();
                setState(prevState => ({
                  ...prevState,
                  [name]: newValue
                }));
              }}
              errorMessages={["Campo obligatorio"]}
              label="Usuario (Min 4, Max 15)"
              validators={["required", "minStringLength: 4", "maxStringLength: 15"]}
            />

            <TextField
              type="text"
              name="documentNumber"
              label="Número Documento"
              inputProps={{
                maxLength: 10
              }}
              onChange={(e) => {
                const { name, value } = e.target;
                const newValue = value.replace(/[^0-9]/g, '');
                setState(prevState => ({
                    ...prevState,
                    [name]: newValue
                }));
            }}
              value={documentNumber || ""}
              validators={["required", "minStringLength: 4", "maxStringLength: 15"]}
              errorMessages={["Campo obligatorio"]}
            />

            <TextField
              type="text"
              name="name"
              label="Nombre"
              onChange={(e) => {
                const { name, value } = e.target;
                const newValue = value.replace(/[^a-zA-Z0-9ñÑ ]/g, '').toUpperCase();
                setState(prevState => ({
                    ...prevState,
                    [name]: newValue
                }));
            }}

              value={name || ""}
              validators={["required"]}
              errorMessages={["Campo obligatorio"]}
            />

            <TextField
              type="text"
              name="lastname"
              label="Apellido"
              onChange={(e) => {
                const { name, value } = e.target;
                const newValue = value.replace(/[^a-zA-Z0-9ñÑ ]/g, '').toUpperCase();
                setState(prevState => ({
                    ...prevState,
                    [name]: newValue
                }));
            }}

              value={lastname || ""}
              validators={["required"]}
              errorMessages={["Campo obligatorio"]}
            />

            <TextField
              type="email"
              name="email"
              label="Email"
              value={email || ""}
              onChange={handleChange}
              validators={["required", "isEmail"]}
              errorMessages={["Campo obligatorio", "email no válido"]}
            />
            {/*<TextField
              name="financialInstitution"
              type="text"
              label="Institución Financiera"
              value={financialInstitution || ""}
              onChange={handleChange}
  />*/}
            {
              institutions !== null && Array.isArray(institutions) ? (
                <TextField
                  sx={{ mb: 1 }}
                  label="Institución"
                  select
                  value={financialInstitution}
                  fullWidth
                  defaultValue={institutions?.find(i => i?.name === state?.financialInstitution)?.id}
                  onChange={(e) => setState(prevState => ({
                    ...prevState,
                    financialInstitution: e.target.value
                  }))}


                >
                  {
                    institutions?.map((group, index) => (
                      <MenuItem key={'i' + index} value={group?.name}>{group?.name}</MenuItem>

                    ))
                  }
                </TextField>

              ) : null
            }

          </Grid>

          <Grid item lg={3} md={3} sm={12} xs={12} sx={{ mt: 2 }}>

            <AutoComplete
            sx={{ width: '100%' }}
              name="status"
              id="status"
              onChange={(event, newValue) => {
                handleChangeStatus(newValue);
              }}
              options={statusList}
              value={status}
              renderInput={(params) => (
                <TextField {...params} label="Estado" variant="outlined" fullWidth />
              )}
            />

            <AutoComplete
             sx={{ width: '100%' }}
              name="portalDownload"
              onChange={(event, newValue) => {
                handleChangePortal(newValue);
              }}
              options={optionsSiNo}
              value={portalDownload}
              renderInput={(params) => (
                <TextField {...params} label="Descarga del Portal" variant="outlined" fullWidth />
              )}
            />

            <AutoComplete
            sx={{ width: '100%' }}
              open={openProfile}
              options={profiles}
              loading={loadingProfiles}
              id="profile"
              onOpen={() => setOpenProfile(true)}
              onClose={() => setOpenProfile(false)}
              onChange={(event, newValue) => {
                handleChangeProfile(newValue);
                setState(prev => ({
                  ...prev,
                  profile: newValue
                }))
              }}
              value={profile || ''}
              isOptionEqualToValue={(option, value) => option.description === value.description}
              getOptionLabel={(option) => option.description || ""}
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="outlined"

                  label="Perfil"
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <>
                        {loadingProfiles ? <CircularProgress color="inherit" size={20} /> : null}
                        {params.InputProps.endAdornment}
                      </>
                    ),
                  }}
                />
              )}
            />
            { /*  <AutoComplete
              name="consultingJudicialOrders"
              onChange={(event, newValue) => {
                handleChangeConsulting(newValue);
              }}
              options={optionsSiNo}
              value={consultingJudicialOrders}
              renderInput={(params) => (
                <TextField {...params} label="Consulta de Providencias Judiciales" variant="outlined" fullWidth />
              )}
              />*/}
            {
              groups !== null && Array.isArray(groups) ? (

                  <TextField
                    sx={{ mb: 1 }}
                    label="Grupos"
                    select

                    fullWidth
                    value={state?.group}
                    onChange={(e) => setState(prevState => ({
                      ...prevState,
                      group: e.target.value
                    }))}


                  >
                    {
                      state?.profile?.id && state?.profile?.id == 1 ? (
                        <MenuItem value="Todos">{"Todos"}</MenuItem>
                      ) : null
                    }
                    {
                      groups?.map((group, index) => (
                        <MenuItem key={'g' + index} value={group?.id}>{group?.name_group}</MenuItem>

                      ))
                    }
                  </TextField>
              ) : null
            }


          </Grid>
        </Grid>
        <LoadingButton
          type="submit"
          color="primary"
          loading={loading}
          variant="contained"
          sx={{ my: 2 }}
        >
          <Icon>done</Icon>
          <Span sx={{ pl: 1, textTransform: "capitalize" }}>Guardar</Span>
        </LoadingButton>
       
        <StyledButton color="inherit" variant="contained" type="button" href="/usuarios" disabled={loading}>
          <Icon>cancel</Icon>
          <Span sx={{ pl: 1, textTransform: "capitalize" }}>Cancelar</Span>
        </StyledButton>
        <Snackbar open={openMessage} vertical="top" horizontal="right" autoHideDuration={6000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
          <Alert onClose={handleCloseMessage} severity={error != null ? error?.split('|')[0] : 'success'} sx={{ width: '100%' }}>
            {error != null ? error?.split('|')[1] : 'Guardado con éxito!'}
          </Alert>
        </Snackbar>
      </ValidatorForm>
    </Container>
  );
};

export default SimpleForm;