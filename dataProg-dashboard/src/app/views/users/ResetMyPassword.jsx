import { Alert, Box, Button, CardContent, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid,  IconButton, Snackbar,  TextField,   InputAdornment } from '@mui/material';
import { Container, CardRoot, BreadcrumbName, } from '../core/styles'
import { Span } from 'app/components/Typography';
import { USER } from 'app/apiEndpoints/portal';
import { useState } from 'react';
import { LoadingButton } from '@mui/lab';
import axios from 'axios.js'
import { Visibility, VisibilityOff } from '@mui/icons-material';



const ResetMyPassword = () => {
    const [form, setForm] = useState({
        values: {
            current_password: '',
            new_password: '',
            confirm_password: '',
            showPassword: false
        },
        error: null,
        isUpdating: false,
        hasSnackbar: false,
        isLoading: false,
        isShowDialog: false,
    });

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };

    const handleSubmit = async () => {
        const { current_password, new_password, confirm_password } = form?.values
        if (current_password === '') {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes ingresar el password actual!',
                hasSnackbar: true
            }))
            return;
        } else if (new_password === '' || confirm_password === '') {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes ingresar la nueva contraseña!',
                hasSnackbar: true
            }))
            return;
        } else if (new_password !== confirm_password) {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes debes confirmar la contraseña!',
                hasSnackbar: true
            }))
            return;
        }
        await axios.put(USER, form?.values).then(() => {
            setForm(prevForm => ({
                ...prevForm,
                values: {
                    confirm_password: '',
                    new_password: '',
                    current_password: '',
                    showPassword: false
                },
                hasSnackbar: true
            }))
        }).catch(({ message }) => {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        })
        setForm(prevForm => ({
            ...prevForm,
            isLoading: false,
        }))
    }



    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Cambiar mi Contraseña</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>


                <CardContent>
                    <Grid container spacing={2} justifyContent="center">
                        <Grid item md={3}>
                            <TextField

                                fullWidth
                                type={!form?.values?.showPassword ? 'password' : 'text'}
                                label="Contraseña actual"
                                name="currentPassword"
                                value={form?.values?.current_password}
                                InputProps={{
                                    endAdornment: <InputAdornment position="end">
                                        <IconButton
                                            aria-label="Mostrar Contraseña"
                                            onClick={() => setForm(prevState => ({
                                                ...prevState,
                                                values: {
                                                    ...prevState.values,
                                                    showPassword: !prevState?.values.showPassword
                                                }
                                            }))}

                                            edge="end"
                                        >
                                            {!form?.values?.showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>,
                                }}
                                onChange={({ target }) => setForm(prevState => ({
                                    ...prevState,
                                    values: {
                                        ...prevState.values,
                                        current_password: target.value
                                    }
                                }))}
                            />
                        </Grid>
                        <Grid item md={3}>
                            <TextField
                                fullWidth
                                InputProps={{
                                    endAdornment: <InputAdornment position="end">
                                        <IconButton
                                            aria-label="Mostrar Contraseña"
                                            onClick={() => setForm(prevState => ({
                                                ...prevState,
                                                values: {
                                                    ...prevState.values,
                                                    showPassword: !prevState?.values.showPassword
                                                }
                                            }))}

                                            edge="end"
                                        >
                                            {!form?.values?.showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>,
                                }}
                                type={!form?.values?.showPassword ? 'password' : 'text'}
                                label="Nueva contraseña (Mínimo 8)"
                                name="newPassword"
                                value={form?.values?.new_password}
                                onChange={({ target }) => setForm(prevState => ({
                                    ...prevState,
                                    values: {
                                        ...prevState.values,
                                        new_password: target.value
                                    }
                                }))}
                            />
                        </Grid>
                        <Grid item md={3}>
                            <TextField
                                fullWidth
                                InputProps={{
                                    endAdornment: <InputAdornment position="end">
                                        <IconButton
                                            aria-label="Mostrar Contraseña"
                                            onClick={() => setForm(prevState => ({
                                                ...prevState,
                                                values: {
                                                    ...prevState.values,
                                                    showPassword: !prevState?.values.showPassword
                                                }
                                            }))}

                                            edge="end"
                                        >
                                            {!form?.values?.showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>,
                                }}
                                type={!form?.values?.showPassword ? 'password' : 'text'}
                                label="Confirmar contraseña"
                                name="confirmPassword"
                                value={form?.values?.confirm_password}
                                onChange={({ target }) => setForm(prevState => ({
                                    ...prevState,
                                    values: {
                                        ...prevState.values,
                                        confirm_password: target.value
                                    }
                                }))}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container justifyContent="center">
                                <Grid item>
                                    <LoadingButton
                                        loading={form?.isLoading} // Establece a 'true' cuando se está procesando el envío
                                        variant="contained"
                                        color="primary"
                                        onClick={() => setForm(prevForm => ({
                                            ...prevForm,
                                            isShowDialog: true
                                        }))}
                                    >
                                        Actualizar
                                    </LoadingButton>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </CardContent>


            </CardRoot>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }

            <Dialog onClose={(_, reason) => {
                if (reason === 'backdropClick' || reason === 'escapeKeyDown') return;
            }} open={form?.isShowDialog} disableEscapeKeyDown>
                <DialogTitle>¿Confirmar acción?</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <Span>{`Esta seguro que desea actualizar su clave?`}</Span>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setForm(prevForm => ({ ...prevForm, isShowDialog: false }))}>Cancelar</Button>
                    <Button onClick={async () => {
                        setForm(prevForm => ({
                            ...prevForm,
                            isShowDialog: false
                        }));
                        handleSubmit();
                    }} autoFocus>
                        Continuar
                    </Button>
                </DialogActions>
            </Dialog>


        </Container >
    )
}

export default ResetMyPassword