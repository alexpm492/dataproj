import { Grid, Icon } from '@mui/material';
import { Box } from '@mui/system';
import { Breadcrumb } from "app/components";
import UsersSummary from "./UsersSummary";
import {BreadcrumbName, Container, StyledButton, } from '../core/styles';
import { dataToXLSX } from 'app/utils/utils';
import { useState } from 'react';



const UsersTable = () => {
  const [dataReport,setDataReport] = useState(null);
  const createReport = () =>{
    if(dataReport !== null){
      dataToXLSX(dataReport,'ReporteUsuarios');
    }
  }
  return (
    <Container>
    <Box className="breadcrumb">
                <BreadcrumbName>Lista de Usuarios</BreadcrumbName>
            </Box>

      <Grid container spacing={2}>
      <Grid item>
        <StyledButton size="small" variant="contained" color="primary" href="/usuarios/nuevo">
        <Icon>add</Icon>
        Nuevo Usuario
      </StyledButton>
      </Grid>
      <Grid item>
        <StyledButton disabled={dataReport === null} onClick={createReport} size="small"  variant="contained" color="success">
        <Icon>grid_view</Icon>
        Exportar
      </StyledButton>
      </Grid>
      </Grid>
      <UsersSummary xlsx={(data)=>setDataReport(data)}/>
    </Container>
  );
};

export default UsersTable;
