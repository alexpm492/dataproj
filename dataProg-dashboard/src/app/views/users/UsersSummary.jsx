import {
  Box,
  Icon,
  Button,
  IconButton,
  styled,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Grid,
  InputAdornment,
  Input
} from "@mui/material";
import dayjs from 'dayjs';
import { LoadingButton } from '@mui/lab';
import SearchIcon from '@mui/icons-material/Search';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { ValidatorForm } from "react-material-ui-form-validator";
import { Span } from "app/components/Typography";
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { useState, useEffect } from "react";
import axios from 'axios.js'
import { CHANGE_PASSWORD, USER, USERS } from "app/apiEndpoints/portal";
import { TextField, StyledButton, StyledTableContent, BreadcrumbName } from '../core/styles'



const UsersSummary = ({xlsx}) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [openDialog, setOpen] = useState(false);
  const [password, setPassword] = useState();
  const [confirmPassword, setConfirmPassword] = useState();
  const [userId, setUserId] = useState();
  const [loading, setLoading] = useState(false);
  const [openDialogDelete, setOpenDialogDelete] = useState(false);
  const [search, setSearch] = useState(null);

  const [passwordVisible, setPasswordVisible] = useState(false);
  useEffect(() => {
    ValidatorForm.addValidationRule("isPasswordMatch", (value) => {
      if (value !== password) return false;
      return true;
    });
    return () => ValidatorForm.removeValidationRule("isPasswordMatch");
  }, [password]);

  const handleClickOpen = () => {
    setPassword('')
    setConfirmPassword('')
    setOpen(true);
  };
  /*const handleOpenDialogDelete = () => {
    setOpenDialogDelete(true)
  }*/

  const handleCloseDelete = () => {
    setOpenDialogDelete(false);
  };
  const handleUserId = (id) => {
    setUserId(id);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangePage = (_, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const [usersList, setListUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  useEffect(() => {
    if(usersList?.length > 0) {
      const report = usersList?.map((r,ix)=>({
        'N': ix + 1,
        'Usuario': r?.username,
        'Apellidos': r?.lastname,
        'Nombres': r?.name,
        'Documento': r?.documentNumber,
        'Institucion': r?.financialInstitution,
        'Estado': r?.status,
        'Perfil': r?.description,
        'Creado': dayjs(r?.created_at).format('DD-MM-YYYY')
      }))
      xlsx(report);
    }else{
      xlsx(null);
    }
  }, [usersList]);

  const getUsers = async () => {
    const response = await axios.get(USERS, {
      params: {
        search
      }
    })
    setListUsers(response.data?.data)
  
    
  }

  const handleChangePass = (event) => {
    event.persist();
    setPassword(event.target.value);
  };
  const handleChangePassConfirm = (event) => {
    event.persist();
    setConfirmPassword(event.target.value);
  };

  const editPassword = async () => {
    setLoading(true);
    const user = { 'password': password };
    try {
      await axios.patch(CHANGE_PASSWORD + '/' + userId, user)
      setLoading(false);
      setOpen(false);
    } catch (e) {
      //setOpenMessage(true);
    }
  };

  useEffect(() => {
    if (search === '') {
      getUsers();
    }
  }, [search])


  const deleteUser = async () => {
    setLoading(true);
    try {
      await axios.delete(USER + '/' + userId)
      setLoading(false);
      setListUsers((current) =>
        current.filter((row) => row.id !== userId)
      );
      setOpenDialogDelete(false);
    } catch (e) {
      //setOpenMessage(true);
    }
  };

  return (
    <Box width="100%" overflow="auto">
        
      <Grid container justifyContent="flex-end" alignItems="center" spacing={2} mb={4}>


        <Grid item>
          <Input
          autoFocus
            value={search ?? ''}
            onChange={({ target }) => setSearch(target.value)}
            placeholder="Buscar..."
            endAdornment={

              <InputAdornment position="end">
                <IconButton onClick={() => {
                  if (search != null && search?.trim()?.length > 0) {
                    getUsers()
                    setPage(0);
                    setRowsPerPage(5);
                  }
                }}>
                  <SearchIcon />
                </IconButton>
              </InputAdornment>

            }
          />
        </Grid>
        {
          search !== null && search.trim()?.length > 0 ? (
            <Grid item>
              <IconButton color="error" onClick={() => setSearch('')}>
                <Icon>delete</Icon>
              </IconButton>
            </Grid>
          ) : null
        }
      </Grid>

      <Box sx={{ overflow: "auto" }}>
        <Box sx={{ width: "100%", display: "table", tableLayout: "fixed" }}>
      <StyledTableContent sx={{width: '100%'}} >
        <TableHead>
          <TableRow>
            <TableCell  align="left">N.</TableCell>
            <TableCell align="left">Login</TableCell>
            <TableCell align="left">Apellidos Nombre</TableCell>
            <TableCell align="center">Num. Documento</TableCell>
            <TableCell align="center">Entidad Financiera</TableCell>
            <TableCell  align="center">Estado</TableCell>
            <TableCell  align="center">Acción</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {usersList?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((subscriber, index) => (
              <TableRow key={index}>
                <TableCell align="left">{index + 1}</TableCell>
                <TableCell align="left">{subscriber.username}</TableCell>
                <TableCell align="left">{subscriber.lastname} {subscriber.name}</TableCell>
                <TableCell align="center">{subscriber.documentNumber}</TableCell>
                <TableCell align="center">{subscriber.financialInstitution}</TableCell>
                <TableCell align="center">{subscriber.status}</TableCell>
                <TableCell align="right">
                  <a href={`/usuarios/editar/${subscriber.id}`}>
                    <Button>
                      <Icon>edit</Icon>
                    </Button>
                  </a>
                  <IconButton
                    onClick={() => {
                      const funcion1 = handleClickOpen;
                      handleUserId(subscriber.id);
                      funcion1();
                    }}
                  >
                    <Icon>password</Icon>
                  </IconButton>

                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </StyledTableContent>

      <TablePagination
  sx={{ px: 2 }}
  page={page}
  component="div"
  rowsPerPage={rowsPerPage}
  count={usersList?.length}
  onPageChange={handleChangePage}
  rowsPerPageOptions={[5, 10, 25, , { value: 5000, label: 'Todos' }]}
  onRowsPerPageChange={handleChangeRowsPerPage}
  nextIconButtonProps={{ "aria-label": "Siguiente Página" }}
  backIconButtonProps={{ "aria-label": "Página Anterior" }}
  labelRowsPerPage="Filas por página:"
  labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
/></Box></Box>

      <Dialog open={openDialog} onClose={handleClose}>
        <DialogTitle>Resetear Password</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Cambiar el password de acceso al usuario
          </DialogContentText>
          <ValidatorForm onSubmit={editPassword} onError={() => null}>
            <Grid container spacing={6}>
              <Grid item lg={12} md={12} sm={12} xs={12} sx={{ mt: 2 }}>
                <TextField
                  name="password"
                  type={!passwordVisible ? 'password' : 'text'}
                  label="Password (Mínimo 8)"
                  value={password || ""}
                  onChange={handleChangePass}
                  InputProps={{
                    endAdornment: <InputAdornment position="end">
                      <IconButton
                        aria-label="Mostrar Contraseña"
                        onClick={() => setPasswordVisible(!passwordVisible)}
    
                        edge="end"
                      >
                        {!passwordVisible ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>,
                  }}
                  validators={["required", "minStringLength: 8"]}
                  errorMessages={["Campo obligatorio"]}
                />

                <TextField
                  type={!passwordVisible ? 'password' : 'text'}
                  name="confirmPassword"
                  onChange={handleChangePassConfirm}
                  label="Confirm Password"
                  InputProps={{
                    endAdornment: <InputAdornment position="end">
                      <IconButton
                        aria-label="Mostrar Contraseña"
                        onClick={() => setPasswordVisible(!passwordVisible)}
    
                        edge="end"
                      >
                        {!passwordVisible ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>,
                  }}
                  value={confirmPassword || ""}
                  validators={["required", "isPasswordMatch"]}
                  errorMessages={["Campo obligatorio", "password no coincide"]}
                />

              </Grid>
            </Grid>

            <LoadingButton
              type="submit"
              color="primary"
              loading={loading}
              variant="contained"
              sx={{ my: 2 }}
            >
              <Icon>done</Icon>
              <Span sx={{ pl: 1, textTransform: "capitalize" }}>Guardar</Span>
            </LoadingButton>

            <StyledButton color="inherit" variant="contained" type="button" onClick={handleClose}>
              <Icon>cancel</Icon>
              <Span sx={{ pl: 1, textTransform: "capitalize" }}>Cerrar</Span>
            </StyledButton>

          </ValidatorForm>
        </DialogContent>
      </Dialog>

      <Dialog open={openDialogDelete} onClose={handleCloseDelete}>
        <DialogTitle>Eliminar Usuario</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Está seguro de eliminar usuario?
          </DialogContentText>
          <ValidatorForm onSubmit={deleteUser} onError={() => null}>

            <LoadingButton
              type="submit"
              color="primary"
              loading={loading}
              variant="contained"
              sx={{ my: 2 }}
            >
              <Icon>done</Icon>
              <Span sx={{ pl: 1, textTransform: "capitalize" }}>Eliminar</Span>
            </LoadingButton>

            <StyledButton color="inherit" variant="contained" type="button" onClick={handleCloseDelete}>
              <Icon>cancel</Icon>
              <Span sx={{ pl: 1, textTransform: "capitalize" }}>Cerrar</Span>
            </StyledButton>

          </ValidatorForm>
        </DialogContent>
      </Dialog>
    </Box>
  );
};

export default UsersSummary;