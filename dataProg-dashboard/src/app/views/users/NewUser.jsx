import {
  Grid,
  Icon,
  Box,
  CircularProgress,
  MenuItem,
  InputAdornment,
  IconButton,
  Button,
} from "@mui/material";
import { Breadcrumb } from "app/components";
import { Span } from "app/components/Typography";
import { useEffect, useState, useRef } from "react";
import { ValidatorForm } from "react-material-ui-form-validator";
import axios from 'axios.js'
import { LoadingButton } from '@mui/lab';
import Snackbar from '@mui/material/Snackbar';
import { Alert, Container, StyledButton, AutoComplete, TextField, BreadcrumbName } from '../core/styles';
import { PROFILES, USER } from "app/apiEndpoints/portal";
import { getFormData } from "app/apiEndpoints/requestUtil";
import { GROUP_LIST, INSTITUTION_LIST } from "app/apiEndpoints/dataForms";
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { useNavigate } from "react-router-dom";



const statusList = ['Activo', 'Inactivo'];

const optionsSiNo = ['Si', 'No'];



const SimpleForm = () => {
  const [state, setState] = useState({ date: new Date(), status: 'Activo', portalDownload: 'No', consultingJudicialOrders: 'No', group: '', financialInstitution: '' });
  const [openMessage, setOpenMessage] = useState(false);
  const [groups, setGroups] = useState(null);
  const [institutions, setInstitutions] = useState(null);
  const [error, setError] = useState(null);
  const formRef = useRef(null);
  const [status] = useState();
  const [portalDownload] = useState();
  //const [consultingJudicialOrders] = useState('No');
  const [loading, setLoading] = useState(false);
  const [openProfile, setOpenProfile] = useState(false);
  const [profiles, setProfiles] = useState([]);
  const loadingProfiles = openProfile && profiles.length === 0;
  const navigate = useNavigate();
  const [passwordVisible, setPasswordVisible] = useState(false);

  useEffect(() => {
    ValidatorForm.addValidationRule("isPasswordMatch", (value) => {
      if (value !== state.password) return false;
      return true;
    });
    return () => ValidatorForm.removeValidationRule("isPasswordMatch");
  }, [state.password]);

  useEffect(() => {
    getFormData([GROUP_LIST, INSTITUTION_LIST]).then((result) => {
      if (result?.groups && Array.isArray(result?.groups)) setGroups(result?.groups?.filter((g) => g?.name_group !== 'Default'));
      if (result?.institutions && Array.isArray(result?.institutions)) setInstitutions(result?.institutions);
    });
  }, [])


  const handleSubmit = async (values) => {
    setLoading(true);
    try {
      await axios.post(USER, state)
      setOpenMessage(true);
      setState({
        date: new Date(), status: 'Activo', portalDownload: 'No', consultingJudicialOrders: 'No', group: '', financialInstitution: ''

      })
      formRef.current?.resetValidations();
      
      setTimeout(() => {
        setLoading(false);
        navigate('/usuarios');
      }, 2000);
    } catch ({ message }) {
      setOpenMessage(true);
      setLoading(false);
      setError(`error|${message ?? 'Ocurrio un error al registrar el usuario, verifica los datos y vuelve a intentarlo'}`);
    }
  }

  const handleChange = (event) => {
    event.persist();
    setState({ ...state, [event.target.name]: event.target.value });
  };

  const handleChangeStatus = (newValue) => {
    if (newValue) {
      setState({ ...state, 'status': newValue });
    }
  };

  const handleChangePortal = (newValue) => {
    if (newValue) {
      setState({ ...state, 'portalDownload': newValue });
    }
  };

  /*const handleChangeConsulting = (newValue) => {
    if (newValue) {
      setState({ ...state, 'consultingJudicialOrders': newValue });
    }
  };*/

  const handleChangeProfile = (newValue) => {
    if (newValue) {
      setState({ ...state, 'profile_id': newValue.id });
    }
  };

  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenMessage(false);
    setError(null);
  };

  const {
    username,
    documentNumber,
    name,
    lastname,
    financialInstitution,
    password,
    confirmPassword,
    email
  } = state;




  useEffect(() => {
    let active = true;

    if (!loadingProfiles) {
      return undefined;
    }

    (async () => {
      const response = await axios.get(PROFILES)
      if (active) {
        setProfiles(response.data?.data)
      }
    })();

    return () => {
      active = false;
    };
  }, [loadingProfiles]);

  useEffect(() => {
    if (!openProfile) {
      setProfiles([]);
    }
  }, [openProfile]);

  return (
    <Container>
  <Box className="breadcrumb">
                <BreadcrumbName>Nuevo Usuario</BreadcrumbName>
            </Box>
     

      <ValidatorForm onSubmit={handleSubmit} onError={() => null} ref={formRef} >
        <Grid container spacing={6}>
          <Grid item lg={3} md={3} sm={12} xs={12} sx={{ mt: 2 }}>
            <TextField
            autoFocus
              type="text"
              inputProps={{
                minLength: 4,
                maxLength: 15
              }}
              name="username"
              id="standard-basic"
              sx={{ textTransform: 'uppercase' }}
              value={username ?? ""}
              onChange={(e) => {
                const { name, value } = e.target;
                const newValue = value.replace(/[^a-zA-Z0-9]/g, '').toUpperCase();
                setState(prevState => ({
                    ...prevState,
                    [name]: newValue
                }));
            }}
              errorMessages={["Campo obligatorio"]}
              label="Usuario (Min 4, Max 15)"
              validators={["required", "minStringLength: 4", "maxStringLength: 15"]}
            />

            <TextField
              type="text"
              name="documentNumber"
              label="Número Documento"
              onChange={(e) => {
                const { name, value } = e.target;
                const newValue = value.replace(/[^0-9]/g, '');
                setState(prevState => ({
                    ...prevState,
                    [name]: newValue
                }));
            }}
              inputProps={{
                maxLength: 10
              }}
              value={documentNumber ?? ""}
              errorMessages={["Campo obligatorio"]}
              validators={["required", "minStringLength: 10", "maxStringLength: 13"]}
            />

            <TextField
              type="text"
              name="name"
              label="Nombre"
              onChange={(e) => {
                const { name, value } = e.target;
                const newValue = value.replace(/[^a-zA-Z0-9ñÑ ]/g, '').toUpperCase();
                setState(prevState => ({
                    ...prevState,
                    [name]: newValue
                }));
            }}
              value={name || ""}
              validators={["required"]}
              errorMessages={["Campo obligatorio"]}
            />
            <TextField
              type="text"
              name="lastname"
              label="Apellido"
              onChange={(e) => {
                const { name, value } = e.target;
                const newValue = value.replace(/[^a-zA-Z0-9ñÑ ]/g, '').toUpperCase();
                setState(prevState => ({
                    ...prevState,
                    [name]: newValue
                }));
            }}
              value={lastname ?? ""}
              validators={["required"]}
              errorMessages={["Campo obligatorio"]}
            />

            <TextField
              type="email"
              name="email"
              label="Email"
              value={email ?? ""}
              onChange={handleChange}
              validators={["required", "isEmail"]}
              errorMessages={["Campo obligatorio", "email no válido"]}
            />
            {/*<TextField
              name="financialInstitution"
              type="text"
              label="Institución Financiera"
              value={financialInstitution || ""}
              onChange={handleChange}
              validators={["required"]}
              errorMessages={["Campo obligatorio"]}
  />*/}
            {
              institutions !== null && Array.isArray(institutions) ? (
                <TextField
                  sx={{ mb: 1 }}
                  label="Institución"
                  select

                  fullWidth
                  value={state?.financialInstitution ?? ''}
                  onChange={(e) => setState(prevState => ({
                    ...prevState,
                    financialInstitution: e.target.value
                  }))}


                >
                  {
                    institutions?.map((group, index) => (
                      <MenuItem key={'g' + index} value={group?.name}>{group?.name}</MenuItem>

                    ))
                  }
                </TextField>

              ) : null
            }

          </Grid>

          <Grid item lg={3} md={3} sm={12} xs={12} sx={{ mt: 2 }}>

            <AutoComplete
              sx={{ width: '100%' }}
              name="status"
              id="status"
              onChange={(event, newValue) => {
                handleChangeStatus(newValue);
              }}
              options={statusList}
              value={status}
              renderInput={(params) => (
                <TextField {...params} label="Estado" variant="outlined" fullWidth />
              )}
            />

            <TextField
              name="password"
              type={!passwordVisible ? 'password' : 'text'}
              label="Password (Mínimo 8)"
              InputProps={{
                endAdornment: <InputAdornment position="end">
                  <IconButton
                    aria-label="Mostrar Contraseña"
                    onClick={() => setPasswordVisible(!passwordVisible)}

                    edge="end"
                  >
                    {!passwordVisible ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>,
              }}
              value={password ?? ""}
              onChange={handleChange}
              validators={["required", "minStringLength: 8"]}
              errorMessages={["Campo obligatorio"]}
            />

            <TextField
              type={!passwordVisible ? 'password' : 'text'}
              name="confirmPassword"
              onChange={handleChange}
              InputProps={{
                endAdornment: <InputAdornment position="end">
                  <IconButton
                    aria-label="Mostrar Contraseña"
                    onClick={() => setPasswordVisible(!passwordVisible)}

                    edge="end"
                  >
                    {!passwordVisible ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>,
              }}
              label="Confirm Password"
              value={confirmPassword ?? ""}
              validators={["required", "isPasswordMatch"]}
              errorMessages={["Campo obligatorio", "password no coincide"]}
            />

            <AutoComplete
              sx={{ width: '100%' }}
              name="portalDownload"
              onChange={(event, newValue) => {
                handleChangePortal(newValue);
              }}
              options={optionsSiNo}
              value={portalDownload}
              renderInput={(params) => (
                <TextField {...params} label="Descarga del Portal" variant="outlined" fullWidth />
              )}
            />

            <AutoComplete
              sx={{ width: '100%' }}
              open={openProfile}
              options={profiles}
              loading={loadingProfiles}
              id="profile"
              onOpen={() => setOpenProfile(true)}
              onClose={() => setOpenProfile(false)}
              onChange={(event, newValue) => {
                handleChangeProfile(newValue);
              }}
              isOptionEqualToValue={(option, value) => option.description === value.description}
              getOptionLabel={(option) => option.description}
              renderInput={(params) => (
                <TextField
                  {...params}
                  fullWidth
                  variant="outlined"
                  label="Perfil"
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <>
                        {loadingProfiles ? <CircularProgress color="inherit" size={20} /> : null}
                        {params.InputProps.endAdornment}
                      </>
                    ),
                  }}
                />
              )}
            />
            {  /*   <AutoComplete
              name="consultingJudicialOrders"
              onChange={(event, newValue) => {
                handleChangeConsulting(newValue);
              }}
              options={optionsSiNo}
              value={consultingJudicialOrders}
              renderInput={(params) => (
                <TextField {...params} label="Consulta de Providencias Judiciales" variant="outlined" fullWidth />
              )}
            />*/
            }

            {
              groups !== null && Array.isArray(groups) ? (
                <TextField
                  sx={{ mb: 1 }}
                  label="Grupos"
                  select

                  fullWidth
                  value={state?.group}
                  onChange={(e) => setState(prevState => ({
                    ...prevState,
                    group: e.target.value
                  }))}


                >
                  {
                    state?.profile_id && state?.profile_id == 1 ? (
                      <MenuItem value="Todos">{"Todos"}</MenuItem>
                    ) : null
                  }
                  {

                    groups?.map((group, index) => (
                      <MenuItem key={'g' + index} value={group?.id}>{group?.name_group}</MenuItem>

                    ))



                  }
                </TextField>

              ) : null
            }

          </Grid>
        </Grid>
        <LoadingButton
          type="submit"
          color="primary"
          loading={loading}
          variant="contained"
          sx={{ my: 2 }}
        >
          <Icon>done</Icon>
          <Span sx={{ pl: 1, textTransform: "capitalize" }}>Guardar</Span>
        </LoadingButton>
        

        <StyledButton color="inherit" variant="contained" type="button" href="/usuarios" disabled={loading}>
          <Icon>cancel</Icon>
          <Span sx={{ pl: 1, textTransform: "capitalize" }}>Cancelar</Span>
        </StyledButton>
      </ValidatorForm>
      <Snackbar open={openMessage} vertical="top" horizontal="right" autoHideDuration={6000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
        <Alert onClose={handleCloseMessage} severity={error != null ? error?.split('|')[0] : 'success'} sx={{ width: '100%' }}>
          {error != null ? error?.split('|')[1] : 'Guardado con éxito!'}
        </Alert>
      </Snackbar>
    </Container>
  );
};

export default SimpleForm;