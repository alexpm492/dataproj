import Loadable from 'app/components/Loadable';
import { lazy } from 'react';
import { authRoles } from '../../auth/authRoles';

const UsersTable = Loadable(lazy(() => import('./UsersTable')));
const NewUser = Loadable(lazy(() => import('./NewUser')));
const EditUser = Loadable(lazy(() => import('./EditUser')));
const ReportUserLogin = Loadable(lazy(() => import('./ReportUserLoginLogs')));
const ResetMyPassword = Loadable(lazy(() => import('./ResetMyPassword')));

const usersRoutes = [
  { path: '/usuarios', element: <UsersTable />, auth: authRoles.admin },
  { path: '/usuarios/nuevo', element: <NewUser />, auth: authRoles.admin },
  { path: '/usuarios/editar/:id', element: <EditUser />, auth: authRoles.admin },
  { path: '/ingresos', element: <ReportUserLogin />, auth: authRoles.admin },
  { path: '/sesion', element: <ResetMyPassword />, auth: authRoles.guest },
];

export default usersRoutes;