import { Typography, Grid, TextField, MenuItem, CardContent, Alert, Snackbar, IconButton, Dialog, DialogTitle, DialogActions, Button, DialogContent, DialogContentText } from '@mui/material';
import { Box } from '@mui/system';

import Icon from '@mui/material/Icon';

import Divider from '@mui/material/Divider';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';

import TableHead from '@mui/material/TableHead';

import TableRow from '@mui/material/TableRow';
import { CardRoot, CardTitle, BreadcrumbName, Container, StyledTable, StyledButton, StyledTableContent } from '../core/styles';
import axios from 'axios.js'
import { useState } from 'react';
import { PROV_CONTROL_SB, PROV_CONTROL_SEPS, PROV_REPORT } from 'app/apiEndpoints/portal';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import 'dayjs/locale/es-mx';
import { LoadingButton } from '@mui/lab';
import { Span } from 'app/components/Typography';






const ReportProvidences = () => {

    const [data, setData] = useState(null);
    const [dataControl, setDataControl] = useState(null);
    const [selectedProvidence, setSelectedProvidence] = useState('');

    const [form, setForm] = useState({
        values: {
            dateBegin: null,
            dateEnd: null,
            typeProvidence: '0'
        },
        error: null,
        isValid: false,
        isUpdating: false,
        hasSnackbar: false,
        isLoading: false,
        isShowDialog: false,
    });


    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };

    const getData = async () => {
        setForm(prevForm => ({
            ...prevForm,
            isLoading: !prevForm.isLoading
        }))
        setData(null);
        setDataControl(null)
        await axios.get(PROV_REPORT, {
            params: {
                ...form?.values
            }
        }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                if (Array.isArray(data?.data)) {
                    if (data?.data.length > 0) {
                        setData(data?.data);
                    } else {
                        setData(null);
                        setForm(prevForm => ({
                            ...prevForm,
                            error: 'info|Sin datos!',
                            hasSnackbar: true
                        }));
                        setData([]);
                    }
                } else {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'error|Error al mostrar los datos!',
                        hasSnackbar: true
                    }))
                }




            } else if (status > 404) {

                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error en el servidor al obtener información, intentalo más tarde!',
                    hasSnackbar: true
                }))
            }

        }).catch(({ message }) => {

            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        }).finally(() => {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: !prevForm.isLoading
            }))
        })
    }



    const handleSubmit = async () => {
        const { dateBegin, dateEnd } = form?.values
        if (dateBegin != null && dateEnd != null) {
            await getData();
        } else {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes seleccionar unas fechas!',
                hasSnackbar: true
            }))
        }
    }

    const getDataControl = async (type, id) => {
        setForm(prevForm => ({
            ...prevForm,
            isUpdating: true
        }))
        setDataControl(null);
        setSelectedProvidence(data?.find((e) => e?.id_cin === id) ?? '');
        await axios.get(type === '1' ? PROV_CONTROL_SEPS(id) : PROV_CONTROL_SB(id)).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                if (Array.isArray(data?.data)) {
                    if (data?.data.length > 0) {
                        setDataControl(data?.data);

                    } else {
                        setForm(prevForm => ({
                            ...prevForm,
                            error: 'info|Sin datos!',
                            hasSnackbar: true
                        }));

                    }
                } else {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'error|Error al mostrar los datos!',
                        hasSnackbar: true
                    }))
                }




            } else if (status > 404) {

                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error en el servidor al obtener información, intentalo más tarde!',
                    hasSnackbar: true
                }))
            }

        }).catch(({ message }) => {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        }).finally(() => {
            setForm(prevForm => ({
                ...prevForm,
                isUpdating: false
            }))
        })
    }

    const deleteProvidence = async (type, id) => {
        setForm(prevForm => ({
            ...prevForm,
            isUpdating: true,
        }))

        await axios.delete(type === '1' ? PROV_CONTROL_SEPS(id) : PROV_CONTROL_SB(id)).then(() => {
            setForm(prevForm => ({
                ...prevForm,

                hasSnackbar: true
            }))
        }).catch(() => {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Error al eliminar la providencia!',
                hasSnackbar: true
            }))
        });
        setForm(prevForm => ({
            ...prevForm,
            isUpdating: false,
        }))
        await getData();
        setDataControl(null);
    }



    return (
        <>
            <LocalizationProvider

                locale="es-MX"
                dateAdapter={AdapterDayjs} >
                <Container>
                    <Box className="breadcrumb">
                        <BreadcrumbName>Reporte de Providencias Judiciales Cargadas</BreadcrumbName>
                    </Box>

                    <CardRoot elevation={6}>


                        <CardTitle>

                            <Grid container spacing={6}>
                                <Grid item xs={1} />
                                <Grid item xs={3}>
                                    <DatePicker
                                        autoFocus
                                        onChange={(value) => setForm(prevForm => ({
                                            ...prevForm,
                                            values: {
                                                ...prevForm.values,
                                                dateBegin: value.format('YYYY-MM-DD')
                                            }
                                        }))}
                                        value={form?.values?.dateBegin ?? ''}
                                        format="DD/MM/YYYY"
                                        slotProps={{ textField: { size: 'small', fullWidth: true, } }}

                                        label="Fecha Desde"
                                        name="dateBegin"
                                    />
                                </Grid>
                                <Grid item xs={1} />
                                <Grid item xs={3}>
                                    <DatePicker
                                        format="DD/MM/YYYY"
                                        onChange={(value) => setForm(prevForm => ({
                                            ...prevForm,
                                            values: {
                                                ...prevForm.values,
                                                dateEnd: value.format('YYYY-MM-DD')
                                            }
                                        }))}
                                        value={form?.values?.dateEnd ?? ''}
                                        slotProps={{ textField: { size: 'small', fullWidth: true } }}

                                        label="Fecha Hasta"
                                        name="dateEnd"
                                    />
                                </Grid>
                                <Grid item xs={1} />
                                <Grid item xs={3}>
                                    <TextField
                                        size="small"
                                        label="Seleccionar"
                                        select
                                        fullWidth
                                        value={form?.values?.typeProvidence}
                                        onChange={(e) => setForm(prevForm => ({
                                            ...prevForm,
                                            values: {
                                                ...prevForm?.values,
                                                typeProvidence: e.target.value
                                            }
                                        }))}
                                    >

                                        <MenuItem value="0">Todos</MenuItem>
                                        <MenuItem value="1">SEPS</MenuItem>
                                        <MenuItem value="2">SB</MenuItem>
                                    </TextField>
                                </Grid>


                            </Grid>
                            <Grid container spacing={2} mt={4} justifyContent="center" sx={{ mb: 4 }}>


                                <LoadingButton loading={form?.isLoading} variant="contained" startIcon={<Icon>check</Icon>} color="primary" type="button" onClick={handleSubmit} sx={{ marginRight: 5 }}>
                                    Aceptar
                                </LoadingButton>
                                <Button size="medium" variant="contained" color="inherit" type="button" startIcon={<Icon>cancel</Icon>} disabled={form?.isLoading || form?.isUpdating} onClick={() => {
                                    setForm(prevForm => ({
                                        ...prevForm,
                                        values: {
                                            dateBegin: null,
                                            dateEnd: null,
                                            typeProvidence: '0'
                                        }
                                    }))
                                    setData(null);
                                    setDataControl(null);
                                }}>

                                    Cancelar
                                </Button>

                            </Grid>

                        </CardTitle>
                        <Divider />

                        <CardContent>
                            <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
                                <div style={{ overflowX: 'auto', width: '100%' }}>
                                    {
                                        data == null ? (

                                            <Alert severity="warning">
                                                <Typography alignItems="center">
                                                    Selecciona una fecha inicial y final
                                                </Typography>
                                            </Alert>
                                        ) : data?.length > 0 ? (

                                            <StyledTableContent sx={{ width: '100%', mt: 4 }}>
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell align="left">N.</TableCell>
                                                        <TableCell align="center">Usuario</TableCell>
                                                        <TableCell align="left">Fecha y Hora de Carga</TableCell>
                                                        <TableCell align="center">Nombre Archivo</TableCell>
                                                        <TableCell align="center">Fecha Archivo</TableCell>
                                                        <TableCell align="center">Entidad</TableCell>
                                                        <TableCell align="center">Acción</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {data?.map((providence, index) => (
                                                        <TableRow key={index + providence?.id_cin}>
                                                            <TableCell align="left">{index + 1}</TableCell>
                                                            <TableCell align="center">{providence?.user?.username}</TableCell>
                                                            <TableCell align="left">{`${providence?.fecha_cin} ${providence?.hora_cin}`}</TableCell>
                                                            <TableCell align="center">{providence?.nombre_cin}</TableCell>
                                                            <TableCell align="center">{providence?.fechaArchivo_cin}</TableCell>
                                                            <TableCell align="center">{providence?.tipoInsumo_cin}</TableCell>
                                                            <TableCell>
                                                                <Grid container spacing={1} justifyContent="center">
                                                                    <Grid item>
                                                                        <IconButton disabled={form?.isUpdating} size="small" color="warning" type="button" onClick={() => getDataControl(providence?.id_tpv + '', providence?.id_cin)} >
                                                                            <Icon>visibility</Icon>
                                                                        </IconButton>

                                                                    </Grid>

                                                                    <Grid item>
                                                                        <IconButton disabled={form?.isUpdating} size="small" color="error" type="button" onClick={() => {
                                                                            setSelectedProvidence(data?.find((e) => e?.id_cin === providence?.id_cin) ?? '');

                                                                            setForm(prevForm => ({
                                                                                ...prevForm,
                                                                                isShowDialog: true
                                                                            }))
                                                                        }}>
                                                                            <Icon>delete</Icon>
                                                                        </IconButton>

                                                                    </Grid>
                                                                </Grid>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </StyledTableContent>

                                        ) : null
                                    }
                                </div>
                            </Grid>
                        </CardContent>


                    </CardRoot>
                    {
                        form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                            <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                                {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                            </Alert>
                        </Snackbar>) : null
                    }

                    <Dialog onClose={(_, reason) => {
                        if (reason === 'backdropClick' || reason === 'escapeKeyDown') return;
                    }} open={form?.isShowDialog} disableEscapeKeyDown>
                        <DialogTitle>¿Confirmar acción?</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                <Span>{`Esta seguro que desea eliminar la providencia ${selectedProvidence?.nombre_cin}, esta acción no se puede reversar`}</Span>
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={() => setForm(prevForm => ({ ...prevForm, isShowDialog: false }))}>Cancelar</Button>
                            <Button onClick={async () => {
                                setForm(prevForm => ({
                                    ...prevForm,
                                    isShowDialog: false
                                }));
                                deleteProvidence(selectedProvidence?.id_tpv + '', selectedProvidence?.id_cin)
                            }} autoFocus>
                                Continuar
                            </Button>
                        </DialogActions>
                    </Dialog>
                </Container >
            </LocalizationProvider>
            {
                dataControl !== null ? (
                    <Container sx={{ mt: 4 }}>
                        <CardTitle sx={{ mt: 2 }}>
                            <Typography variant="h6">{selectedProvidence?.nombre_cin}</Typography>
                        </CardTitle>
                        <CardRoot elevation={6}>

                            <CardContent>
                                <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
                                    <div style={{ overflowX: 'auto', width: '100%' }}>
                                        <StyledTable>
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell align="left" sx={{ paddingLeft: 5, minWidth: 100 }}>N.</TableCell>
                                                    {
                                                        Object.keys(dataControl[0]).map((key, index) => (
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }}  sx={{width:'auto'}}  align="center" key={key + index}>{key?.split('_')[0]}</TableCell>
                                                        ))
                                                    }


                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    dataControl?.map((row, index) => (
                                                        <TableRow component="th" scope="row" key={index + 'a'}>
                                                            <TableCell align="left" sx={{ paddingLeft:5 ,minWidth:100 }}>{index + 1}</TableCell>
                                                            {
                                                                Object.keys(row).map((k, ik) => (
                                                                    <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }}    align="center" key={ik + 'b'}>
                                                                    <Typography variant="body1">{row[k]}</Typography>
                                                                    </TableCell>
                                                                ))
                                                            }
                                                        </TableRow>
                                                    ))
                                                }
                                            </TableBody>
                                        </StyledTable>


                                    </div>
                                </Grid>
                            </CardContent>


                        </CardRoot>

                    </Container >
                ) : null
            }
        </>
    )

}

export default ReportProvidences