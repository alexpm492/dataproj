import { Button, Typography, Grid, Snackbar, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Alert, IconButton, Icon } from '@mui/material';
import { Box } from '@mui/system';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { useRef, useState } from 'react';
import axios from 'axios.js'
import {Container, CardRoot, CardTitle, BreadcrumbName,VisuallyHiddenInput} from '../core/styles'
import { PROV_SB } from 'app/apiEndpoints/portal';
import { LoadingButton } from '@mui/lab';
import { Span } from 'app/components/Typography';



const UploadFileSB = () => {
    const fileInputRef = useRef(null);

    const handleKeyDown = (event) => {
      if (event.key === 'Enter') {
        fileInputRef.current.click();
      }
    };
    const [form, setForm] = useState({
        values: {
            id_tpv: 2,
            nombreArchivo: null,
            archivo: null,
            keyFile: 0
        },
        error: null,
        hasSnackbar: false,
        isLoading: false,
        isShowDialog: false,
    });

    const resetKeyFile =() => {
        setForm(prevForm =>({
            ...prevForm,
            values:{
                ...prevForm?.values,
                keyFile: Math.random().toString(36)
            }
        }))
    }

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };

 

    const handleUpload = async () => {
        

        const { id_tpv, nombreArchivo, archivo } = form?.values;
        if (id_tpv !== null && nombreArchivo !== null && archivo !== null) {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: true,
            }))
            try {
                const formData = new FormData();
                Object.keys(form?.values).forEach((k) => {
                    formData.append(k, form?.values[k]);
                });

                await axios.post(PROV_SB, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                }).then(() => {
                    setForm(prevForm => ({
                        ...prevForm,
                        hasSnackbar: true
                    }))
                }).catch(({message}) => {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: `error|${message ?? 'Ocurrio un error al registrar el archivo'}`,
                        hasSnackbar: true
                    }))
                });


            } catch (error) {
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error al conectarse a la api!',
                    hasSnackbar: true
                }))

            } finally {
                setForm(prevForm => ({
                    ...prevForm,
                    isLoading: false,
                    values: {
                        ...prevForm?.values,
                        nombreArchivo: null,
                        archivo: null
                    }
                }))
                resetKeyFile();
            }
        } else {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes llenar el formulario',
                hasSnackbar: true
            }))
        }
    };

    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Carga Insumo SB</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>


            <CardTitle>
                    <Grid container spacing={2} direction="column" >
                        <Grid item xs={9}>
                            <Typography variant="button" gutterBottom>
                                Seleccione un archivo
                            </Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <Button size="small"  onKeyDown={handleKeyDown} component="label" color="warning" variant="contained" startIcon={<CloudUploadIcon />}>
                                Escoger archivo
                                <VisuallyHiddenInput
                                 ref={fileInputRef}
                                    type="file"
                                    accept=".txt"
                                    key={form?.values?.keyFile}
                                    onChange={({ target }) => {
                                        const selectedFile = target?.files[0];

                                        setForm(prevForm => ({
                                            ...prevForm,
                                            values: {
                                                ...prevForm?.values,
                                                archivo: selectedFile,
                                                nombreArchivo: selectedFile?.name?.split('.')[0]
                                            }
                                        }));

                                        resetKeyFile();
                                    }}
                                />
                            </Button>
                        </Grid>


                    </Grid>
                    <Grid container>
                        {form?.values?.archivo && (
                            <Grid item md={5} sm={12} xs={12} mt={4} sx={{backgroundColor: '#ddf5dd'}}>
                              
                                    <Grid container alignItems="center">
                                        <Grid item xs={11}>
                                            <Typography variant="body1" gutterBottom>
                                                <b>Nombre:</b> {form.values.nombreArchivo} <br/>  <b>Tamaño:</b> {(form?.values?.archivo.size / 1024).toFixed(2)} KB
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={1} style={{ textAlign: 'right' }}>
                                            <IconButton
                                                size="small"
                                                color="error"
                                                onClick={() => {
                                                    setForm(prevForm => ({
                                                        ...prevForm,
                                                        values: {
                                                            ...prevForm?.values,
                                                            archivo: null,
                                                            nombreArchivo: null,
                                                        }
                                                    }))
                                                    resetKeyFile();
                                                }}
                                            >
                                                <Icon>delete</Icon>
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                       
                            </Grid>
                        )}
                    </Grid>
                    <Grid container mt={4}>
                        <Grid item xs={3} >
                            <LoadingButton loading={form?.isLoading} size="medium" variant="contained" color="primary" type="button" onClick={() => {
                                setForm(prevForm => ({
                                    ...prevForm,
                                    isShowDialog: true
                                }))
                            }} >

                                Procesar
                            </LoadingButton>
                        </Grid>
                    </Grid>


                </CardTitle>



            </CardRoot>
            <>
                    {
                        form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                            <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                                {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                            </Alert>
                        </Snackbar>) : null
                    }
                    <Dialog onClose={(_, reason) => {
                        if (reason === 'backdropClick' || reason === 'escapeKeyDown') return;
                    }} open={form?.isShowDialog} disableEscapeKeyDown>
                        <DialogTitle>¿Confirmar acción?</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                <Span>Esta seguro de subir el archivo?</Span>
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={() => setForm(prevForm => ({ ...prevForm, isShowDialog: false }))}>Cancelar</Button>
                            <Button onClick={async () => {
                                setForm(prevForm => ({
                                    ...prevForm,
                                    isShowDialog: false
                                }));
                                handleUpload();
                            }} autoFocus>
                                Continuar
                            </Button>
                        </DialogActions>
                    </Dialog>


                </>
        </Container>
    )
}

export default UploadFileSB