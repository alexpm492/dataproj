import { Card, Button, Typography, Grid, Input, Alert, List, ListItemText, ListItem, Icon, IconButton, Snackbar, TextField } from '@mui/material';
import { Box, styled } from '@mui/system';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';

import { useRef, useState } from 'react';
import axios from 'axios.js'
import { Container, CardRoot, CardTitle, BreadcrumbName, VisuallyHiddenInput } from '../core/styles'
import { LoadingButton } from '@mui/lab';
import { PROV_SEPS_DOWNLOAD } from 'app/apiEndpoints/portal';


const MAX_FILE_SIZE_MB = 500;
const UploadProvidenceSEPS = () => {

    const fileInputRef = useRef(null);

    const handleKeyDown = (event) => {
      if (event.key === 'Enter') {
        fileInputRef.current.click();
      }
    };
    const [form, setForm] = useState({
        values: {
            code: '',
            selectedFiles: [],
            totalSize: 0,
            keyFile: 0
        },
        error: null,
        isValid: false,
        hasSnackbar: false,
        isLoading: false,
    });
    const resetKeyFile = () => {
        setForm(prevForm => ({
            ...prevForm,
            values: {
                ...prevForm?.values,
                keyFile: Math.random().toString(36)
            }
        }))
    }


    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };

    const handleFileInputChange = (event) => {
        const files = Array.from(event.target.files);
        const filesExceedingSizeLimit = files.filter((file) => file.size > MAX_FILE_SIZE_MB * 1024 * 1024);

        if (filesExceedingSizeLimit.length > 0) {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Los archivos seleccionados superan los 100MB!',
                hasSnackbar: true
            }));
        } else {
            const totalFileSize = files.reduce((acc, file) => acc + file.size, 0);
            const totalSizeInMB = totalFileSize / (1024 * 1024);
            setForm(prevForm => ({
                ...prevForm,
                values: {
                    ...prevForm?.values,
                    selectedFiles: [...files],
                    totalSize:  totalSizeInMB
                }
            }));
            resetKeyFile();
        }
    };

    const handleDeleteFile = (index) => {
        const newFiles = [...form?.values?.selectedFiles];
        newFiles.splice(index, 1);
        const totalFileSize = newFiles.reduce((acc, file) => acc + file.size, 0);
        const totalSizeInMB = totalFileSize > 0 ? totalFileSize / (1024 * 1024) : 0;
        setForm(prevForm => ({
            ...prevForm,
            values: {
                ...prevForm?.values,
                selectedFiles: newFiles,
                totalSize: totalSizeInMB
            }
        }))
    };


    const handleUploadFiles = async () => {
        const { code, selectedFiles } = form?.values
        if (selectedFiles.length === 0) {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|No se han seleccionado archivos',
                hasSnackbar: true
            }));
            return
        }
        if (code.trim() === '') {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes agregar el código circular',
                hasSnackbar: true
            }));
            return
        }
        try {

            const formData = new FormData();
            selectedFiles.forEach((file) => {
                formData.append('files[]', file);
            });
            setForm(prevForm => ({
                ...prevForm,
                isLoading: true,
            }))
            // You can replace 'YOUR_UPLOAD_ENDPOINT' with the actual server endpoint for file upload
            await axios.post(PROV_SEPS_DOWNLOAD(code), formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            });
            setForm(prevForm => ({
                ...prevForm,
                values: {
                    ...prevForm?.values,
                    code: '',
                    selectedFiles: []

                },

                hasSnackbar: true
            }))



        } catch ({ message }) {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|No se pudo registrar los archivos intentalo mas tarde!',
                hasSnackbar: true
            }))
        } finally {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: false,
            }))

            resetKeyFile();
        }
    };


    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Carga Providencias SEPS</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>


                <CardTitle>
                    <Grid container spacing={2} direction="column" mb={2}>
                        <Grid item xs={9}>
                            <Typography variant="button" gutterBottom>
                                Número de Circular
                            </Typography>
                        </Grid>
                        <Grid item >
                        <TextField size="small" variant="outlined"
                        sx={{width:350}}
                                            autoFocus value={form?.values?.code} onChange={(e) => setForm(prevForm => ({
                                ...prevForm,
                                values: {
                                    ...prevForm?.values,
                                    code: e.target.value
                                }
                            }))} />
                        </Grid>

                    </Grid>
                    <Grid container spacing={2} direction="column" mb={5}>
                        <Grid item xs={9}>
                            <Typography variant="button" gutterBottom>
                                Seleccione un archivo
                            </Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <Button size="small"  onKeyDown={handleKeyDown}  disabled={form?.isLoading} component="label" color="warning" variant="contained" startIcon={<CloudUploadIcon />}>
                                Escoger archivo
                                <VisuallyHiddenInput   ref={fileInputRef} key={form?.values?.keyFile} multiple type="file" accept="application/pdf" onChange={handleFileInputChange} />
                            </Button>
                        </Grid>
                    </Grid>


                    {form?.values?.selectedFiles.length > 0 && (
                        <>
                            <Typography variant="h6" ><b>Total Archivos:</b>{form?.values?.selectedFiles.length}{"    "}<b>Tamaño:</b> {form?.values?.totalSize.toFixed(2)} MB</Typography>
                            <Grid container spacing={2}>
                                {form?.values?.selectedFiles.map((file, index) => (
                                    <Grid item xs={12} sm={6} md={2} key={index}>
                                        <List>
                                            <ListItem>
                                                <ListItemText primaryTypographyProps={{
                                                    sx: {
                                                        overflow: 'hidden',
                                                        textOverflow: 'ellipsis'
                                                    }
                                                }} primary={file.name} />
                                                <IconButton variant="warning" edge="end" aria-label="delete" onClick={() => handleDeleteFile(index)}>
                                                    <Icon>delete</Icon>
                                                </IconButton>
                                            </ListItem>
                                        </List>
                                    </Grid>
                                ))}
                            </Grid>
                            <Grid container mt={3}>
                                <LoadingButton loading={form?.isLoading} variant="contained" color="primary" onClick={handleUploadFiles} sx={{ marginRight: 5 }}>
                                    Procesar
                                </LoadingButton>
                                <Button startIcon={
                                    <Icon>cancel</Icon>
                                }

                                    variant="contained" disabled={form?.isLoading} color="inherit" type="button" onClick={() => {
                                        setForm(prevForm => ({
                                            prevForm,
                                            values: {
                                                ...prevForm?.values,
                                                code: '',
                                                selectedFiles: [],
                                                totalSize: 0,
                                                keyFile: 0
                                            }
                                        }));
                                    }}
                                >
                                    Cancelar
                                </Button>
                            </Grid>

                        </>
                    )}



                </CardTitle>

                {
                    form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                        <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                            {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                        </Alert>
                    </Snackbar>) : null
                }

            </CardRoot>
        </Container>
    )
}

export default UploadProvidenceSEPS