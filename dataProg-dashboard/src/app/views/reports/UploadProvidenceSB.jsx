import { Button, Typography, Grid, Snackbar, Alert, List, ListItem, ListItemText, IconButton, Icon } from '@mui/material';
import { Box } from '@mui/system';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { Container, CardRoot, CardTitle, BreadcrumbName, VisuallyHiddenInput } from '../core/styles'
import { LoadingButton } from '@mui/lab';
import axios from 'axios.js'
import { PROV_SB_STORE, PROV_SB_SYNC } from 'app/apiEndpoints/portal';
import { useRef, useState } from 'react';
const MAX_FILE_SIZE_MB = 500;
const UploadProvidenceSB = () => {
    const fileInputRef = useRef(null);

    const handleKeyDown = (event) => {
      if (event.key === 'Enter') {
        fileInputRef.current.click();
      }
    };
    
    const [form, setForm] = useState({
        values: {
            selectedFiles: [],
            totalSize: 0,
            keyFile: 0
        },
        error: null,
        isValid: false,
        hasSnackbar: false,
        isLoading: false,
        isUpdating: false
    });

    const resetKeyFile = () => {
        setForm(prevForm => ({
            ...prevForm,
            values: {
                ...prevForm?.values,
                keyFile: Math.random().toString(36)
            }
        }))
    }

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };

    const handleFileInputChange = (event) => {
        const files = Array.from(event.target.files);
        const filesExceedingSizeLimit = files.filter((file) => file.size > MAX_FILE_SIZE_MB * 1024 * 1024);

        if (filesExceedingSizeLimit.length > 0) {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Los archivos seleccionados superan los 100MB!',
                hasSnackbar: true
            }));
        } else {
            const totalFileSize = files.reduce((acc, file) => acc + file.size, 0);
            const totalSizeInMB = totalFileSize / (1024 * 1024);
            setForm(prevForm => ({
                ...prevForm,
                values: {
                    ...prevForm?.values,
                    selectedFiles: [...files],
                    totalSize: totalSizeInMB
                }
            }))

            resetKeyFile();
        }
    };

    const handleDeleteFile = (index) => {
        const newFiles = [...form?.values?.selectedFiles];
        newFiles.splice(index, 1);
        const totalFileSize = newFiles.reduce((acc, file) => acc + file.size, 0);
        const totalSizeInMB = totalFileSize > 0 ? totalFileSize / (1024 * 1024) : 0;
        setForm(prevForm => ({
            ...prevForm,
            values: {
                ...prevForm?.values,
                selectedFiles: newFiles,
                totalSize: totalSizeInMB
            }
        }))
    };


    const handleUploadFiles = async () => {
        const { selectedFiles } = form?.values
        if (selectedFiles.length === 0) {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|No se han seleccionado archivos',
                hasSnackbar: true
            }));
            return
        }

        try {

            const formData = new FormData();
            selectedFiles.forEach((file) => {
                formData.append('files[]', file);
            });
            setForm(prevForm => ({
                ...prevForm,
                isLoading: true,
            }))
            // You can replace 'YOUR_UPLOAD_ENDPOINT' with the actual server endpoint for file upload
            await axios.post(PROV_SB_STORE, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            });
            setForm(prevForm => ({
                ...prevForm,
                values: {
                    ...prevForm?.values,
                    selectedFiles: []

                },

                hasSnackbar: true
            }))



        } catch ({ message }) {

            setForm(prevForm => ({
                ...prevForm,
                error: 'error|No se pudo registrar los archivos intentalo mas tarde!',
                hasSnackbar: true
            }))
        } finally {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: false,
            }))
            resetKeyFile();
        }
    };

    const syncFiles = async () => {
        setForm(prevForm => ({
            ...prevForm,
            isUpdating: true,
        }))
        await axios.get(PROV_SB_SYNC).then(() => {
            setForm(prevForm => ({
                ...prevForm,
                hasSnackbar: true
            }))
        }).catch(({ message }) => {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        }).finally(() => {
            setForm(prevForm => ({
                ...prevForm,
                isUpdating: false,
            }))
        })
    }



    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Cargar Providencias SB</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>


                <CardTitle>
                    <Grid container>
                    <Typography variant="button" gutterBottom>
                                Seleccione un archivo
                            </Typography>
                    </Grid>
                    <Grid container spacing={2} mt={4}>
                       
                        <Grid item >
                            <Button size="small" onKeyDown={handleKeyDown} component="label" color="warning" disabled={form?.isLoading} variant="contained" startIcon={<CloudUploadIcon />}>
                                Escoger archivo
                                <VisuallyHiddenInput  ref={fileInputRef} key={form?.values?.keyFile} multiple type="file" accept=".txt,.pdf,.zip,.rar,.doc,.docx,.xlsx,.xls,.csv" onChange={handleFileInputChange} />
                            </Button>
                        </Grid>
                        <Grid item>
                            <LoadingButton loading={form?.isUpdating} variant="contained" color="primary" onClick={syncFiles}>
                                Sincronizar
                            </LoadingButton>
                        </Grid>
                    </Grid>
                    {form?.values?.selectedFiles.length > 0 && (
                        <>
                            <Typography variant="h6"><b>Total Archivos:</b>{form?.values?.selectedFiles.length}{"    "}<b>Tamaño:</b> {form?.values?.totalSize.toFixed(2)} MB</Typography>
                            <Grid container spacing={2}>
                                {form?.values?.selectedFiles.map((file, index) => (
                                    <Grid item xs={12} sm={6} md={2} key={index}>
                                        <List>
                                            <ListItem>
                                                <ListItemText primaryTypographyProps={{
                                                    sx: {
                                                        overflow: 'hidden',
                                                        textOverflow: 'ellipsis'
                                                    }
                                                }} primary={file.name} />
                                                <IconButton variant="warning" edge="end" aria-label="delete" onClick={() => handleDeleteFile(index)}>
                                                    <Icon>delete</Icon>
                                                </IconButton>
                                            </ListItem>
                                        </List>
                                    </Grid>
                                ))}
                            </Grid>
                            <Grid container mt={3}>
                                <LoadingButton loading={form?.isLoading} variant="contained" color="primary" onClick={handleUploadFiles} sx={{ marginRight: 5 }}>
                                    Procesar
                                </LoadingButton>
                                <Button startIcon={
                                    <Icon>cancel</Icon>
                                }

                                    variant="contained" disabled={form?.isLoading} color="inherit" type="button" onClick={() => {
                                        setForm(prevForm => ({
                                            prevForm,
                                            values: {

                                                selectedFiles: [],
                                                totalSize: 0,
                                                keyFile: 0
                                            }
                                        }));
                                    }}
                                >
                                    Cancelar
                                </Button>
                            </Grid>
                        </>
                    )}




                </CardTitle>



            </CardRoot>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }
        </Container>
    )
}

export default UploadProvidenceSB