import Loadable from 'app/components/Loadable';
import { lazy } from 'react';
import { authRoles } from '../../auth/authRoles';

const UploadFileSEPS = Loadable(lazy(() => import('./UploadFileSEPS')));
const UploadFileSB = Loadable(lazy(() => import('./UploadFileSB')));
const UploadProvidenceSB = Loadable(lazy(() => import('./UploadProvidenceSB')));
const UploadProvidenceSEPS = Loadable(lazy(() => import('./UploadProvidenceSEPS')));
const ReportProvidences = Loadable(lazy(() => import('./ReportProvidences')));


const reportsRoutes = [
    { path: '/consultas/cargarInsumoSEPS', element: <UploadFileSEPS />, auth: authRoles.admin },
    { path: '/consultas/cargarInsumoSB', element: <UploadFileSB />, auth: authRoles.guest },
    { path: '/consultas/cargarProvidenciaSEPS', element: <UploadProvidenceSEPS />, auth: authRoles.guest },
    { path: '/consultas/cargarProvidenciaSB', element: <UploadProvidenceSB />, auth: authRoles.guest },
    { path: '/consultas/providenciasJudiciales', element: <ReportProvidences />, auth: authRoles.guest },


];

export default reportsRoutes;