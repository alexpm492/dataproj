import Loadable from 'app/components/Loadable';
import { lazy } from 'react';
import { authRoles } from '../../auth/authRoles';

const ReportDownloads = Loadable(lazy(() => import('./ReportDownloads')));
const TypeFiles = Loadable(lazy(() => import('./TypeFiles')));
const UsersFiles = Loadable(lazy(() => import('./UsersFiles')));
const PortalUploadFile = Loadable(lazy(() => import('./PortalUploadFile')));
const PortalFileDetail = Loadable(lazy(() => import('./PortalFileDetail')));
const PortalEditFile = Loadable(lazy(() => import('./PortalEditFile')));
const PortalAddFile = Loadable(lazy(() => import('./PortalAddFile-old')));
const PortalUserFiles = Loadable(lazy(() => import('./PortalUserFiles')));
const PortalAddGroup = Loadable(lazy(() => import('./PortalGroupFile')));
const PortalInstitution = Loadable(lazy(() => import('./PortalInstitution')));



const portalRoutes = [
    { path: '/portal/reporteDescargas', element: <ReportDownloads />, auth: authRoles.admin },
    { path: '/portal/tiposArchivos', element: <TypeFiles />, auth: authRoles.admin },
    { path: '/portal/tiposArchivos/agregar/:nombre/:id', element: <PortalUploadFile />, auth: authRoles.admin },
    { path: '/portal/tiposArchivos/detalle/:id', element: <PortalFileDetail />, auth: authRoles.admin },
    { path: '/portal/tiposArchivos/editar/:id', element: <PortalEditFile />, auth: authRoles.admin },
    { path: '/portal/nuevoArchivo', element: <PortalAddFile />, auth: authRoles.editor },
    { path: '/portal/nuevaInstitucion', element: <PortalInstitution />, auth: authRoles.admin },
    { path: '/portal/usuarios', element: <UsersFiles />, auth: authRoles.admin },
    { path: '/portal/nuevoGrupo', element: <PortalAddGroup />, auth: authRoles.admin },
    { path: '/portal/usuarios/permisos/:id', element: <PortalUserFiles />, auth: authRoles.admin },
    // { path: '/archivos/cargarInsumoSB', element: <UploadFileSB />, auth: authRoles.guest },
];

export default portalRoutes;