import { Card, Button, Typography, Grid, TextField, MenuItem, CardContent, Alert, CircularProgress,Snackbar, DialogContent } from '@mui/material';
import { Box, styled } from '@mui/system';
import axios from 'axios.js'
import Icon from '@mui/material/Icon';
import { useEffect, useState } from 'react';
import Divider from '@mui/material/Divider';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';

import TableHead from '@mui/material/TableHead';

import TableRow from '@mui/material/TableRow';
import { useParams } from 'react-router-dom';
import { IconButton } from '@mui/material';
import {GET_FILE,DAY_DETAIL_FILE, DETAIL_FILE} from '../../apiEndpoints/portal'
import { Span } from 'app/components/Typography';
import { DialogTitle } from '@mui/material';
import { DialogActions } from '@mui/material';
import { Dialog } from '@mui/material';
import { DialogContentText } from '@mui/material';
import { StyledTableContent } from '../core/styles';


const Container = styled("div")(({ theme }) => ({
    margin: "30px",
    [theme.breakpoints.down("sm")]: { margin: "16px" },
    "& .breadcrumb": {
        marginBottom: "30px",
        [theme.breakpoints.down("sm")]: { marginBottom: "16px" },
    },
}));

const CardRoot = styled(Card)(() => ({
    height: '100%',
    padding: '20px 24px',
}));

const CardTitle = styled('div')(({ subtitle }) => ({
    fontSize: '1rem',
    fontWeight: '500',
    textTransform: 'capitalize',
    marginBottom: !subtitle && '16px',
}));


const BreadcrumbName = styled('h4')(() => ({
    margin: 0,
    fontSize: '16px',
    paddingBottom: '1px',
    verticalAlign: 'middle',
    textTransform: 'capitalize',
}));



const tableLabels = ['Nº', 'Día', 'Fecha Providencia', 'Nombre Archivo', 'Descripción', 'Fecha Carga', 'Hora Carga'];
const widthColumns = [75,100,120, 'auto', 'auto', 100, 100];
const PortalFileDetail = () => {
    const { id } = useParams();

    const [selectedId, setSelectedId] = useState(null)

    const [data, setData] = useState({
        currentFile: null,
        detailFile: null,
    })

    const [form, setForm] = useState({
        error: null,
        isValid: false,
        hasSnackbar: false,
        isLoading: false,
        isUpdating: false,
        isShowDialog: false
    });


    useEffect(() => {
        
        getData();
        getInfoFile();
       
    }, []);


    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };

    const getInfoFile = async () => {
        await axios.get(GET_FILE(id)).then((response)=>{
            const { status, data } = response;
            if (status === 200) {
                setData(prevData=>({
                    ...prevData,
                    currentFile: data?.data
                }))
            }else{
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'info|Sin datos - Detalle Archivo!',
                    hasSnackbar: true
                }));
            }

        }).catch(() => {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Error al obtener los datos - Detalle Archivo!',
                hasSnackbar: true
            }))
        })
        

    }

    const deleteFile = async (id) => {
        setForm(prevForm => ({
            ...prevForm,
            isUpdating: true,
        }))
        await axios.delete(DETAIL_FILE(id)).then(()=>{
            setForm(prevForm => ({
                ...prevForm,
               
                hasSnackbar: true
            }))
        }).catch(() => {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Error al eliminar el archivo!',
                hasSnackbar: true
            }))
        });
        setForm(prevForm => ({
            ...prevForm,
            isUpdating: false,
        }))
        await getData();
    }

    const getData = async () => {
        setForm(prevForm => ({
            ...prevForm,
            isLoading: true,
        }))
        await axios.get(DAY_DETAIL_FILE(id)).then((response)=>{
            const { status, data } = response;
            if (status === 200) {
                setData(prevData=>({
                    ...prevData,
                    detailFile: data?.data
                }))
            }else{
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'info|Sin datos - Dias Archivo!',
                    hasSnackbar: true
                }));
            }

        }).catch(() => {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Error al obtener los datos - Dias Archivo!',
                hasSnackbar: true
            }))
        })
        setForm(prevForm => ({
            ...prevForm,
            isLoading: false,
        }))
    }



    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Detalle de Carga de Providencias Judiciales</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>


                <CardTitle>
                    <Grid container mb={4}>
                           
                            <Grid item md={1} xs={4} mt={3}>
                                <Button size="medium" variant="contained" color="inherit" type="button" startIcon={<Icon>cancel</Icon>} href="/portal/tiposArchivos" disabled={form?.isUpdating}>

                                    Cancelar
                                </Button>
                            </Grid>
                        </Grid>
                        <Divider />
                   {
                    !form?.isLoading && data?.currentFile !== null ? (
                        <Grid container spacing={6} >
                        <Grid item xs={12} >
                            <Typography variant="h6">{`Archivos - ${data?.currentFile?.descripcion_arc ?? ''}`}</Typography>
                        </Grid>

                    </Grid>
                    ):null
                   }

                </CardTitle>
                <Divider />

                <CardContent>
                    <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
                        <>
                        {
                            Array.isArray(data?.detailFile) && data?.detailFile.length === 0 ?(
                                    <Alert severity="warning" sx={{width: '100%', mt: 4, textAlign: 'center'}} >Sin archivos </Alert>
                            ): null
                        }
                        {
                            form.isLoading ? (<Grid sx={{ my: 4 }} container justify="center" alignItems="center">
                                <Grid item xs={12} align="center">
                                    <CircularProgress />
                                </Grid>
                            </Grid>) : null
                        }
                        {

                            Array.isArray(data?.detailFile) && data?.detailFile.length > 0 ? (
                                <StyledTableContent  sx={{width: '100%'}} >
                                     <TableHead>
                                            <TableRow>
                                            {
                                                        Object.keys(data?.detailFile[0]).map((_, index) => (
                                                            <TableCell key={index} sx={{width:widthColumns[index] }}    align={index === 0 || index === 1 ? 'left': 'center'} >{tableLabels[index]}</TableCell>
                                                        ))
                                                    
                                                    }
                                                <TableCell align="center" sx={{width: 80}}>Opción</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                data?.detailFile.map((row, index)=>(
                                                    <TableRow key={index + row?.id_dta}>
                                                    <TableCell sx={{width: widthColumns[0]}} align="left" >{index + 1}</TableCell>
                                                    <TableCell sx={{width: widthColumns[1]}} align="left">{`${row?.descripcion_dar}`}</TableCell>
                                                    <TableCell align="center" sx={{width: widthColumns[2]}}>{row?.fechaArchivo_dta}</TableCell>
                                                    <TableCell align="center" sx={{width: widthColumns[3]}}>{row?.nombre_dta}</TableCell>
                                                    <TableCell align="center" sx={{width: widthColumns[4]}}>{row?.descripcion_dta}</TableCell>
                                                    <TableCell align="center" sx={{width: widthColumns[5]}}>{row?.fecha_dta}</TableCell>
                                                    <TableCell align="center" sx={{width: widthColumns[6]}}>{row?.hora_dta}</TableCell>
                                                    <TableCell align="center" sx={{width: 80}}>
                                                        <Grid container spacing={1} justifyContent="center">
                                                            <Grid item>
                                                                <IconButton
                                                                    disabled={form?.isUpdating}
                                                                    color="error" title="Eliminar" type="button" onClick={()=>{
                                                                        setSelectedId(row?.id_dta);
                                                                        setForm(prevForm=>({
                                                                            ...prevForm,
                                                                            isShowDialog: true
                                                                        }))
                                                                    }}
                                                                >
                                                                    <Icon>delete</Icon>
                                                                </IconButton>

                                                            </Grid>


                                                        </Grid>
                                                    </TableCell>
                                                </TableRow>
                                                ))
                                            }
                                        </TableBody>
                                </StyledTableContent>

                            ) : null
                        }
                        
                        </>
                        
                    </Grid>
                </CardContent>


            </CardRoot>
            <>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }
           <Dialog onClose={(_, reason)=>{
            if(reason === 'backdropClick' || reason === 'escapeKeyDown') return; 
           }} open={form?.isShowDialog} disableEscapeKeyDown>
      <DialogTitle>¿Confirmar acción?</DialogTitle>
      <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <Span>Esta seguro que desea eliminar este archivo, esta acción no se puede reversar</Span>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={()=>setForm(prevForm=>({...prevForm, isShowDialog: false}))}>Cancelar</Button>
          <Button onClick={async()=>{
            setForm(prevForm=>({
                ...prevForm,
                isShowDialog: false
            }));
            deleteFile(selectedId)
          }} autoFocus>
            Continuar
          </Button>
        </DialogActions>
    </Dialog>
            </>
        </Container >
    )
}

export default PortalFileDetail