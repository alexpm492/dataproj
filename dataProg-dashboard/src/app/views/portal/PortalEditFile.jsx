import { LoadingButton } from '@mui/lab';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Icon } from '@mui/material';
import { Alert } from '@mui/material';
import { Snackbar } from '@mui/material';
import { Card, Button, Grid,  MenuItem, CardContent, Select,  TextField } from '@mui/material';
import { Box, styled } from '@mui/system';
import { GROUP_LIST } from 'app/apiEndpoints/dataForms';
import { GET_FILE } from 'app/apiEndpoints/portal';
import { getFormData } from 'app/apiEndpoints/requestUtil';
import { Span } from 'app/components/Typography';
import axios from 'axios.js'
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import {Container, CardRoot, CardTitle, BreadcrumbName} from '../core/styles';



const PortalEditFile = () => {
    const { id } = useParams();
    const [data, setData] = useState({
        currentFile: null,
        groups: null
    })
    const [form, setForm] = useState({
        values: {
            descripcion_arc: '',
            group_id: '',
            estado_arc: '',
        },
        error: null,
        hasSnackbar: false,
        isLoading: false,
        isShowDialog: false,
    })


    useEffect(() => {
        getFormData([GROUP_LIST]).then((result) => {
            if (result?.groups && Array.isArray(result?.groups)) setData(prevData=>({...prevData, groups:result?.groups}));
        });
        getInfoFile();
    }, [])

  
const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };


    const getInfoFile = async () => {
        await axios.get(GET_FILE(id)).then((response)=>{
            const { status, data } = response;
            if (status === 200) {
                setData(prevData=>({
                    ...prevData,
                    currentFile: data?.data
                }))
                setForm(prevForm=>({
                    ...prevForm,
                    values: {
                        ...prevForm.values,
                        descripcion_arc: data?.data?.descripcion_arc ?? '',
                        estado_arc : data?.data?.estado_arc ?? '',
                        group_id : data?.data?.group_id ?? '',
                    }
                }))
            }else{
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'info|Sin datos - Detalle Archivo!',
                    hasSnackbar: true
                }));
            }
        }).catch(() => {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Error al obtener los datos - Detalle Archivo!',
                hasSnackbar: true
            }))
        })
       

    }

    const handleSubmit = async () => {
        const {descripcion_arc, estado_arc, group_id} = form?.values
        if (descripcion_arc !== '' && estado_arc !== '' && group_id !== '' ) {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: true,
            }))
            await axios.put(GET_FILE(id), form?.values).then(() => {
                setForm(prevForm => ({
                    ...prevForm,
                   
                    hasSnackbar: true
                }))
            }).catch(() => {
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|No se pudo actualizar la información del archivo !',
                    hasSnackbar: true
                }))
            })
            setForm(prevForm => ({
                ...prevForm,
                isLoading: false,
            }))
        }else {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes llenar los datos del formulario!',
                hasSnackbar: true
            }))
        }
    }

    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Editar Tipo de Archivo</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>


                <CardTitle>
                    <Grid container spacing={2} mt={2}>

                        <Grid item xs={12} sm={4} md={3}>
                            <TextField
                            autoFocus
                                size="small"
                                label="Descripción"
                                inputProps={{
                                    maxLength: 50
                                  }}
                                value={form?.values?.descripcion_arc}
                                onChange={(e) => setForm(prevForm=>({
                                    ...prevForm,
                                    values:{
                                        ...prevForm?.values,
                                        descripcion_arc: e.target.value
                                    }
                                }))}
                                type="text"
                                fullWidth
                                
                            />
                        </Grid>
                        <Grid item xs={12} sm={4} md={3}>
                            <TextField

                                size="small"
                                select
                                fullWidth
                                id="group-label"
                                label="grupo"
                                value={form?.values?.group_id}
                                onChange={(e) => setForm(prevForm=>({
                                    ...prevForm,
                                    values:{
                                        ...prevForm?.values,
                                        group_id: e.target.value
                                    }
                                }))}
                            >

                                {
                                    data?.groups !== null && Array.isArray(data?.groups) ? data?.groups.map((ele, ix) => (

                                        <MenuItem value={ele?.id} key={ix}>{ele?.name_group}</MenuItem>


                                    )) : null
                                }

                            </TextField>

                        </Grid>
                        <Grid item xs={12} sm={4} md={3}>
                            <TextField
                                size="small"
                                label="Estado"
                                select
                                fullWidth
                                value={form?.values?.estado_arc}
                                onChange={(e) => setForm(prevForm=>({
                                    ...prevForm,
                                    values:{
                                        ...prevForm?.values,
                                        estado_arc: e.target.value
                                    }
                                }))}
                            >

                                <MenuItem value="A">Activo</MenuItem>
                                <MenuItem value="I">Inactivo</MenuItem>

                            </TextField>
                        </Grid>

                    </Grid>

                </CardTitle>
                <CardContent>
                    <Container mt={4}>
                    
                        <LoadingButton loading={form?.isLoading} size="medium" variant="contained" color="primary"  sx={{marginRight: 5}} type="button" onClick={()=>setForm(prevForm=>({
                            ...prevForm,
                            isShowDialog: true
                        }))}>

                            Procesar
                        </LoadingButton>
                
                                <Button size="medium" variant="contained" color="inherit" type="button" startIcon={<Icon>cancel</Icon>} href="/portal/tiposArchivos" disabled={form?.isLoading}>

                                    Cancelar
                                </Button>

                    </Container>
                </CardContent>


            </CardRoot>
            <>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }
             <Dialog onClose={(_, reason)=>{
            if(reason === 'backdropClick' || reason === 'escapeKeyDown') return; 
           }} open={form?.isShowDialog} disableEscapeKeyDown>
      <DialogTitle>¿Confirmar acción?</DialogTitle>
      <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <Span>Esta seguro que desea modificar este archivo</Span>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={()=>setForm(prevForm=>({...prevForm, isShowDialog: false}))}>Cancelar</Button>
          <Button onClick={async()=>{
            setForm(prevForm=>({
                ...prevForm,
                isShowDialog: false
            }));
            handleSubmit();
          }} autoFocus>
            Continuar
          </Button>
        </DialogActions>
    </Dialog>
            
            
            </>
        </Container >
    )
}

export default PortalEditFile