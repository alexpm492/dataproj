import { Grid, TextField, CardContent, IconButton, Icon, Snackbar, Alert, CircularProgress } from '@mui/material';
import { Box } from '@mui/system';
import Autocomplete from '@mui/material/Autocomplete';
import axios from 'axios.js'
import { useEffect, useState } from 'react';


import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';



import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';

import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';


import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import "dayjs/locale/es";

import dayjs from 'dayjs';
import { dataToXLSX } from '../../utils/utils';

import { pageLimit } from '../../utils/constant';
import { FILE_LIST, USER_LIST } from '../../apiEndpoints/dataForms';
import { DOWNLOAD_LIST } from '../../apiEndpoints/portal';
import { getFormData } from '../../apiEndpoints/requestUtil';
import { LoadingButton } from '@mui/lab';
import { Span } from 'app/components/Typography';
import { Container, CardRoot, CardTitle, StyledButton, BreadcrumbName, StyledTableContent} from '../core/styles';



const tableLabels = ['Nº', 'Apellidos y Nombres', 'Estado Usuario', 'Entidad Financiera', 'TipoArchivo', 'Detalle Archivo', 'Fecha Descarga'];
const tableWidths = [10,350, 108, 250, 250, 350, 120];
const ReportDownloads = () => {
    const [userListCombo, setUserListCombo] = useState([]);
    const [fileListCombo, setFileListCombo] = useState([]);
    const [data, setData] = useState(null);

    const [form, setForm] = useState({
        values: {
            dateBegin: dayjs(new Date()).add(-7, 'day').format('YYYY-MM-DD'),
            dateEnd: dayjs(new Date()).format('YYYY-MM-DD'),
            user_id: null,
            arc_id: null
        },
        error: null,
        isValid: false,
        hasSnackbar: false,
        isLoading: false,
    });

    const [dataControl, setDataControl] = useState({
        page: 1,
        pageLimit,
        isAsc: 0,
        orderBy: null,
        search: null,
        total: null,
        nextPage: null
    });


    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };



    useEffect(() => {
        getFormData([FILE_LIST, USER_LIST]).then((result) => {
            if (result?.users && Array.isArray(result?.users)) setUserListCombo(result?.users);
            if (result?.files && Array.isArray(result?.files)) setFileListCombo(result?.files);
        });
        getData();

    }, [])

    useEffect(() => {
        if (data != null) {
            getData();
        }
    }, [dataControl?.isAsc, dataControl?.orderBy, dataControl?.page, dataControl?.pageLimit, dataControl?.search])


    const getData = async () => {
        const { dateBegin, dateEnd } = form?.values
        if (dateBegin !== null && dateEnd !== null) {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: !prevForm.isLoading
            }))
            if (data != null) setData([]);
            await axios.get(DOWNLOAD_LIST, {
                params: {
                    ...dataControl,
                    ...form.values,
                    user_id: form?.values?.user_id?.id,
                    arc_id: form?.values?.arc_id?.id
                }
            }).then((response) => {
                const { status, data } = response;

                if (status === 200) {
                    setDataControl(prevData => ({
                        ...prevData,
                        orderBy: data?.orderBy,
                        page: data?.currentPage,
                        total: data?.total,
                        nextPage: data?.nextPage
                    }));
                    if (Array.isArray(data?.data)) {
                        if (data?.data.length > 0) {
                            setData(data?.data.map((row, id) => {
                                row['lastname'] = `${row?.lastname} ${row?.name}`;
                                delete row?.name;
                                delete row?.id;
                                id++;
                                return {
                                    id,
                                    ...row,

                                }
                            }));
                        } else {
                            setForm(prevForm => ({
                                ...prevForm,
                                error: 'info|Sin datos!',
                                hasSnackbar: true
                            }));

                        }
                    } else {
                        setForm(prevForm => ({
                            ...prevForm,
                            error: 'error|Error al mostrar los datos!',
                            hasSnackbar: true
                        }))
                    }




                } else if (status > 404) {

                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'error|Error en el servidor al obtener información, intentalo más tarde!',
                        hasSnackbar: true
                    }))
                }

            }).catch((error) => {
                setForm(prevForm => ({
                    ...prevForm,
                    error: `error|${error?.message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                    hasSnackbar: true
                }))
            });
            setForm(prevForm => ({
                ...prevForm,
                isLoading: !prevForm.isLoading
            }))
        } else {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes llenar el formulario!',
                hasSnackbar: true
            }))
        }
    }



    const handleOrderBy = async (orderBy) => {

        setDataControl(prevControl => ({
            ...prevControl,
            orderBy
        }));

    }

    const handleSubmit = async () => {
        await getData();
    }

    const formatData = (data) => data?.map((r, i) => ({ ...Object.fromEntries(Object.keys(r).map((k, index) => [tableLabels[index], r[k]])) }));

    const resetControlData = () =>{
        setDataControl({ page: 1,
            pageLimit,
            isAsc: 0,
            orderBy: null,
            search: null,
            total: null,
            nextPage: null});
    }
    return (
        <>
            <LocalizationProvider

              LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="es">
                <Container>
                    <Box className="breadcrumb">
                        <BreadcrumbName>Reporte Descargas del Portal</BreadcrumbName>
                    </Box>

                    <CardRoot elevation={6}>


                        <CardTitle>

                            <Grid container spacing={6}>

                                <Grid item xs={3}>
                                    <DatePicker
                                        onChange={(value) => setForm(prevForm => ({
                                            ...prevForm,
                                            values: {
                                                ...prevForm.values,
                                                dateBegin: value.format('YYYY-MM-DD')
                                            }
                                        }))}
                                        value={dayjs(form?.values?.dateBegin)}
                                        format="DD/MM/YYYY"
                                        slotProps={{ textField: { size: 'small', fullWidth: true, } }}
                                        autoFocus
                                        label="Fecha Desde"
                                        name="dateBegin"
                                    />


                                </Grid>

                                <Grid item xs={3}>
                                    <DatePicker
                                        format="DD/MM/YYYY"
                                        onChange={(value) => setForm(prevForm => ({
                                            ...prevForm,
                                            values: {
                                                ...prevForm.values,
                                                dateEnd: value.format('YYYY-MM-DD')
                                            }
                                        }))}
                                        value={dayjs(form?.values?.dateEnd ?? '')}
                                        slotProps={{ textField: { size: 'small', fullWidth: true } }}

                                        label="Fecha Hasta"
                                        name="dateEnd"
                                    />
                                </Grid>

                                <Grid item xs={3}>
                                    <Autocomplete
                                        onChange={(_, value) => {
                                            setForm(prevForm => ({
                                                ...prevForm,
                                                values: {
                                                    ...prevForm?.values,
                                                    user_id: value
                                                }
                                            }));
                                            resetControlData();
                                        }}

                                        disablePortal
                                        value={form?.values?.user_id ?? 'Todos'}
                                        disabled={!userListCombo.length}
                                        options={userListCombo}
                                        renderInput={(params) => <TextField {...params} label="Usuario" size="small" fullWidth name="user" />}
                                    />
                                </Grid>

                                <Grid item xs={3}>
                                    <Autocomplete

                                        onChange={(_, value) =>{
                                            setForm(prevForm => ({
                                                ...prevForm,
                                                values: {
                                                    ...prevForm?.values,
                                                    arc_id: value
                                                }
                                            }))

                                            resetControlData();
                                        }}
                                        value={form?.values?.arc_id ?? 'Todos'}
                                        disablePortal
                                        disabled={!fileListCombo.length}
                                        options={fileListCombo}
                                        renderInput={(params) => <TextField {...params} label="Tipo de Archivos" size="small" fullWidth name="file" />}
                                    />
                                </Grid>


                            </Grid>
                            <Grid container spacing={2} mt={2} justifyContent="center">

                                <Grid item>
                                    <LoadingButton type="button" color="primary" loading={form?.isLoading} variant="contained" sx={{ my: 1 }} onClick={handleSubmit}>
                                        <Icon fontSize="small">check</Icon>
                                        <Span>Aceptar</Span>
                                    </LoadingButton>

                                </Grid>
                                <Grid item>
                                    <StyledButton onClick={() => dataToXLSX(formatData(data), 'ReporteDescargas')} disabled={!(!form.isLoading && Array.isArray(data) && data.length > 0)} variant="contained" color="success">
                                        <Icon fontSize="small">grid_view</Icon>{"  "} Exportar
                                    </StyledButton>
                                </Grid>

                                <Grid item>
                                    <StyledButton variant="contained" color="inherit" type="button" disabled={form?.isLoading} onClick={() => {
                                        setForm(prevForm => ({
                                            ...prevForm,
                                            values: {
                                                dateBegin: null,
                                                dateEnd: null,
                                                user_id: null,
                                                arc_id: null
                                            }
                                        }))
                                        setData(null);
                                        resetControlData();
                                    }}>
                                        <Icon fontSize="small">cancel</Icon>{"  "} Cancelar
                                    </StyledButton>
                                </Grid>

                            </Grid>


                        </CardTitle>





                    </CardRoot>


                </Container >
                {
                    form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                        <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                            {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                        </Alert>
                    </Snackbar>) : null
                }
            </LocalizationProvider>
            <>
                {
                    form.isLoading ? (<Grid sx={{ my: 4 }} container justify="center" alignItems="center">
                        <Grid item xs={12} align="center">
                            <CircularProgress />
                        </Grid>
                    </Grid>) : null
                }
                {
                    (Array.isArray(data) && data?.length > 0) ? (

                        <Container sx={{ mt: 4, mb: 6 }}>
                            <CardRoot elevation={6}>
                                <CardContent>
                                    <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
                                        <div style={{ overflowX: 'auto', width: '100%' }}>
                                            <StyledTableContent sx={{width: '100%'}} >
                                                <TableHead >
                                                    <TableRow>
                                                        {
                                                            Object.keys(data[0]).map((k, index) => (
                                                                <TableCell  sx={{ cursor: index > 0 ? 'pointer' : 'inherit', width: tableWidths[index] }} align="left" key={k + index} onClick={() => {
                                                                    return index > 0 ? handleOrderBy(k) : void 0;
                                                                }}>
                                                                    {tableLabels[index]}
                                                                    {(dataControl?.orderBy === k && index > 0) && (
                                                                        <IconButton sx={{ cursor: "pointer" }} size="small" aria-label="direction" onClick={() => setDataControl(prevControl => ({
                                                                            ...prevControl,
                                                                            isAsc: Number(!prevControl.isAsc)
                                                                        }))}>
                                                                            {!dataControl?.isAsc ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />}
                                                                        </IconButton>
                                                                    )}
                                                                </TableCell>
                                                            ))
                                                        }

                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {
                                                        data.map((row, index) => (
                                                            <TableRow key={index + row?.id}>
                                                                <TableCell >{row?.id}</TableCell>
                                                                <TableCell align='left'>{row?.lastname}</TableCell>
                                                                <TableCell align='left'>{row?.status}</TableCell>
                                                                <TableCell align='left'>{row?.financialInstitution}</TableCell>
                                                                <TableCell align='left'>{row?.descripcion_arc}</TableCell>
                                                                <TableCell align='left'>{row?.dta_detail}</TableCell>
                                                                <TableCell align='left'>{row?.download_at}</TableCell>
                                                            </TableRow>
                                                        ))
                                                    }
                                                </TableBody>
                                            </StyledTableContent>
                                        </div>
                                    </Grid>
                                </CardContent>
                            </CardRoot>
                        </Container>

                    ) : null
                }
                {
                    (data != null && data.length > 0) ? (
                        <Grid item xs={12} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <TablePagination
                                rowsPerPageOptions={[1, 10, 25, 50, 100, { value: 5000, label: 'Todos' }]}
                                component="div"
                                count={dataControl?.total ?? 0}
                                rowsPerPage={dataControl?.pageLimit}
                                page={dataControl?.page != null ? dataControl.page - 1 : 0}
                                onPageChange={(e, page) => {

                                    setDataControl(prevData => ({
                                        ...prevData,
                                        page: page + 1
                                    }))
                                }}
                                onRowsPerPageChange={(e) => setDataControl(prevData => ({
                                    ...prevData,
                                    pageLimit: parseInt(e.target.value, 10),
                                    page: 1
                                }))}
                                labelRowsPerPage="Filas por página:" // Personaliza el texto para las filas por página
                                labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`} // Personaliza el texto para las filas mostradas
                                backIconButtonText="Anterior" // Personaliza el texto del botón de retroceso
                                nextIconButtonText="Siguiente" // Personaliza el texto del botón de siguiente
                            />
                        </Grid>
                    ) : null
                }</>
        </>

    )
}

export default ReportDownloads;