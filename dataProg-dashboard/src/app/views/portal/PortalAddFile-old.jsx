import { Button, Grid,  MenuItem, CardContent, TextField, Snackbar, Alert, Icon } from '@mui/material';
import { Box} from '@mui/system';
import axios from 'axios.js'
import { useEffect, useState } from 'react';

import { GROUP_LIST } from '../../apiEndpoints/dataForms';
import { CREATE_FILE } from '../../apiEndpoints/portal';
import { LoadingButton } from '@mui/lab';
import { getFormData } from 'app/apiEndpoints/requestUtil';
import { Container, CardRoot, CardTitle, BreadcrumbName } from '../core/styles'


const PortalAddFile = () => {

    const [dataGroups, setDataGroups] = useState(null);
    const [form, setForm] = useState({
        values: {
            descripcion: '',
            group_id: ''
        },
        error: null,
        isValid: false,
        hasSnackbar: false,
        isLoading: false,
    })



    useEffect(() => {
        getFormData([GROUP_LIST]).then((result) => {
            if (result?.groups && Array.isArray(result?.groups)) setDataGroups(result?.groups);
        });

    }, [])



    const handleSubmit = async () => {
        const { descripcion, group_id } = form?.values;
        if (descripcion.trim() !== '' && group_id !== '') {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: true,
            }))
            await axios.post(CREATE_FILE, {
                ...form?.values
            }).then(() => {
                setForm(prevForm => ({
                    ...prevForm,
                    values: {
                        descripcion: '',
                        group_id: ''
                    },
                    hasSnackbar: true
                }))
            }).catch(() => {
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error al registrar nuevo archivo!',
                    hasSnackbar: true
                }))
            })


            setForm(prevForm => ({
                ...prevForm,
                isLoading: false,
            }))
        } else {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes llenar el formulario',
                hasSnackbar: true
            }))
        }


    }

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };
    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Agregar Tipo Archivo</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>


                <CardTitle>


                </CardTitle>
                <CardContent>
                    <Grid container spacing={2} >
                        <Grid item xs={6} sm={6} md={3}>
                            <TextField
                            autoFocus
                                size="small"
                                inputProps={{
                                    maxLength: 50
                                  }}
                                label="Descripción"
                                value={form?.values?.descripcion}
                                onChange={(e) => setForm(prevForm => ({
                                    ...prevForm,
                                    values: {
                                        ...prevForm.values,
                                        descripcion: e.target.value,

                                    }
                                }))}
                                type="text"
                                fullWidth

                            />
                        </Grid>
                        <Grid item xs={6} sm={6} md={3}>
                            <TextField
                                size="small"
                                label="Grupo"
                                select
                                value={form?.values.group_id}
                                fullWidth
                                onChange={(e) => setForm(prevForm => ({
                                    ...prevForm,
                                    values: {
                                        ...prevForm.values,
                                        group_id: e.target.value,

                                    }
                                }))}
                            >
                                {
                                    dataGroups !== null && Array.isArray(dataGroups) ? dataGroups.map((ele, ix) => (
                                        <MenuItem value={ele?.id} key={ix}>{ele?.name_group}</MenuItem>
                                    )) : null
                                }
                            </TextField>

                        </Grid>

                    </Grid>
                    <Container sx={{ mt: 4 }}>

                        <LoadingButton size="medium" variant="contained" loading={form?.isLoading} color="primary" type="button" onClick={handleSubmit} sx={{ marginRight: 5 }}>

                            Procesar
                        </LoadingButton>


                        <Button size="medium" variant="contained" color="inherit" type="button" startIcon={<Icon>cancel</Icon>} href="/portal/tiposArchivos" disabled={form?.isLoading}>

                            Cancelar
                        </Button>
                    </Container>

                </CardContent>


            </CardRoot>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }
        </Container >
    )
}

export default PortalAddFile