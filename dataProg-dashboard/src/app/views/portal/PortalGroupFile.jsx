import { Alert, Box, Button, CardContent, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, Grid, Icon, IconButton, Snackbar, TableCell, TableRow, TextField, TableBody, TableHead } from '@mui/material';
import { Container, CardRoot, CardTitle, StyledTable, BreadcrumbName, StyledButton, } from '../core/styles'
import { Span } from 'app/components/Typography';
import { EDIT_GROUP, GROUP } from 'app/apiEndpoints/portal';
import { useEffect, useState } from 'react';
import { LoadingButton } from '@mui/lab';
import axios from 'axios.js'
import dayjs from 'dayjs';
const PortalGroupFile = () => {
    const [data, setData] = useState(null);
    const [selectedGroup, setSelectedGroup] = useState(null);
    const [form, setForm] = useState({
        values: {
            name_group: '',
        },
        error: null,
        isUpdating: false,
        hasSnackbar: false,
        isLoading: false,
        isShowDialog: false,
        isShowDialogForm: false
    });

    useEffect(() => {
        getData();
    }, [])


    const getData = async () => {
        setForm(prevForm => ({
            ...prevForm,
            isLoading: !prevForm.isLoading
        }))

        await axios.get(GROUP).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                if (Array.isArray(data?.data)) {
                    if (data?.data?.length > 0) {
                        setData(data?.data);
                    } else {
                        setForm(prevForm => ({
                            ...prevForm,
                            error: 'info|Sin datos!',
                            hasSnackbar: true
                        }));

                    }
                } else {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'error|Error al mostrar los datos!',
                        hasSnackbar: true
                    }))
                }




            } else if (status > 404) {

                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error en el servidor al obtener información, intentalo más tarde!',
                    hasSnackbar: true
                }))
            }

        }).catch(({ message }) => {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        }).finally(() => {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: !prevForm.isLoading
            }))
        })
    }

    const handleSubmit = async () => {
        const { name_group } = form?.values;
        if (name_group.trim() !== '') {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: true,
            }))
            await axios.post(GROUP, {
                ...form?.values
            }).then(() => {
                setForm(prevForm => ({
                    ...prevForm,
                    values: {
                        descripcion: '',
                        group_id: ''
                    },

                    hasSnackbar: true
                }))
            }).catch(() => {
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error al registrar nuevo grupo!',
                    hasSnackbar: true
                }))
            })


            setForm(prevForm => ({
                ...prevForm,
                isLoading: false,
            }))
        } else {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes llenar el formulario',
                hasSnackbar: true
            }))
        }
        await getData();

    }

    const deleteGroup = async (id) => {
        setForm(prevForm => ({
            ...prevForm,
            isUpdating: true,
        }))
        await axios.delete(EDIT_GROUP(id)).then(() => {
            setForm(prevForm => ({
                ...prevForm,

                hasSnackbar: true
            }))
        }).catch(({message}) => {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        });
        setForm(prevForm => ({
            ...prevForm,
            isUpdating: false,
        }))
        await getData();
    }

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };
    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Grupos Archivos</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>

                <CardTitle>
                    <Button disabled={form?.isLoading} variant="contained" color="warning" type="button" onClick={()=>setForm(prevForm=>({
                        ...prevForm,
                        isShowDialogForm: true
                    }))}>
                        + Nuevo Grupo
                    </Button>
                </CardTitle>

                <Divider />

                <CardContent>
                    <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
                        <div style={{ overflowX: 'auto', width: '100%' }}>
                            {
                               data !== null  & data?.length > 0 ? (

                                    <StyledTable sx={{width:'100%'}}>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="left" sx={{width:100}}>N.</TableCell>
                                                <TableCell align="left">Nombre</TableCell>
                                                <TableCell align="center">Creado por</TableCell>
                                                <TableCell align="center">Fecha</TableCell>
                                                <TableCell align="center">Acción</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {data?.map((group, index) => (
                                                <TableRow key={index + group?.id}>
                                                    <TableCell align="left">{index + 1}</TableCell>
                                                    <TableCell align="left">{group?.name_group}</TableCell>
                                                    <TableCell align="center">{group?.created_by}</TableCell>
                                                    <TableCell align="center">{dayjs(group?.created_at).format('DD/MM/YYYY HH:mm:ss')}</TableCell>

                                                    <TableCell>
                                                        <Grid container spacing={1} justifyContent="center">
                                                            <Grid item>
                                                                <IconButton disabled={form?.isUpdating || group?.name_group ==='Default'} size="small" color="error" type="button" onClick={() => {
                                                                    setSelectedGroup(group);

                                                                    setForm(prevForm => ({
                                                                        ...prevForm,
                                                                        isShowDialog: true
                                                                    }))
                                                                }}>
                                                                    <Icon>delete</Icon>
                                                                </IconButton>

                                                            </Grid>
                                                        </Grid>
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </StyledTable>

                                ) : null
                            }
                        </div>
                    </Grid>
                </CardContent>


            </CardRoot>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }

            <Dialog onClose={(_, reason) => {
                if (reason === 'backdropClick' || reason === 'escapeKeyDown') return;
            }} open={form?.isShowDialog} disableEscapeKeyDown>
                <DialogTitle>¿Confirmar acción?</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <Span>{`Esta seguro que desea eliminar el grupo ${selectedGroup?.name_group}, esta acción no se puede reversar, y puede afectar a otros usuarios`}</Span>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setForm(prevForm => ({ ...prevForm, isShowDialog: false }))}>Cancelar</Button>
                    <Button onClick={async () => {
                        setForm(prevForm => ({
                            ...prevForm,
                            isShowDialog: false
                        }));
                        deleteGroup(selectedGroup?.id);
                    }} autoFocus>
                        Continuar
                    </Button>
                </DialogActions>
            </Dialog>

            <Dialog onClose={(_, reason) => {
                if (reason === 'backdropClick' || reason === 'escapeKeyDown') return;
            }} open={form?.isShowDialogForm} disableEscapeKeyDown maxWidth={'sm'} fullWidth>
                <DialogTitle>Nuevo Grupo</DialogTitle>
                <DialogContent>
                <TextField
                autoFocus
                                size="small"
                                label="Nombre Grupo"
                                sx={{mt:2}}
                                value={form?.values?.name_group}
                                onChange={(e) =>{
                                    const { value } = e.target;
                                    const newValue = value.replace(/[^a-zA-Z0-9 ]/g, '').toUpperCase();
                                    setForm(prevForm => ({
                                        ...prevForm,
                                        values: {
                                            ...prevForm.values,
                                            name_group: newValue,
    
                                        }
                                    }))
                                }}
                                type="text"
                                fullWidth

                            />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setForm(prevForm => ({ ...prevForm, values:{
                        ...prevForm.values,
                        name_group: ''
                    }, isShowDialogForm: false }))}>Cancelar</Button>
                    <Button onClick={async () => {
                        setForm(prevForm => ({
                            ...prevForm,
                            isShowDialogForm: false
                        }));
                        handleSubmit();
                    }} autoFocus>
                        Continuar
                    </Button>
                </DialogActions>
            </Dialog>
        </Container >
    )
}

export default PortalGroupFile