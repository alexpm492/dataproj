import { Icon, Button, Typography, Grid, TextField, Snackbar, Alert, IconButton } from '@mui/material';
import { Box} from '@mui/system';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { useRef, useState } from 'react';
import axios from 'axios.js'
import { useParams } from 'react-router-dom';

import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import 'dayjs/locale/es-mx';
import dayjs from 'dayjs';
import { DatePicker } from '@mui/x-date-pickers';
import { LoadingButton } from '@mui/lab';
import { UPLOAD_FILE } from '../../apiEndpoints/portal';
import {Container, CardRoot, CardTitle, BreadcrumbName, VisuallyHiddenInput} from '../core/styles';


const PortalFileUpload = () => {
  
    const fileInputRef = useRef(null);

    const handleKeyDown = (event) => {
      if (event.key === 'Enter') {
        fileInputRef.current.click();
      }
    };


    const { nombre, id } = useParams();
   
    const [form, setForm] = useState({
        values: {
            id_arc: id,
            descripcion_dta: '',
            fechaArchivo_dta: dayjs(new Date()).format('YYYY-MM-DD'),
            archivo: null,
            keyFile: 0
        },
        error: null,
        isValid: false,
        hasSnackbar: false,
        isLoading: false,
    });
    const resetKeyFile =() => {
        setForm(prevForm =>({
            ...prevForm,
            values:{
                ...prevForm?.values,
                keyFile: Math.random().toString(36)
            }
        }))
    }

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };




    const handleUpload = async () => {
        const { id_arc, fechaArchivo_dta, archivo } = form?.values;
        if (id_arc !== null && fechaArchivo_dta !== null && archivo !== null) {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: true,
            }))
            try {
                const formData = new FormData();
                Object.keys(form?.values).forEach((k) => {
                    formData.append(k, form?.values[k]);
                });

                await axios.post(UPLOAD_FILE, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                }).then(() => {
                    setForm(prevForm => ({
                        ...prevForm,
                        values: {
                            ...prevForm?.values,
                            descripcion_dta: '',
                            fechaArchivo_dta: dayjs(new Date()).format('YYYY-MM-DD'),
                            archivo: null

                        },

                        hasSnackbar: true
                    }))
                }).catch((error) => {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: `error|${error?.message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                        hasSnackbar: true
                    }))
                });


            } catch (error) {
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error al conectarse a la api!',
                    hasSnackbar: true
                }))

            } finally {
                setForm(prevForm => ({
                    ...prevForm,
                    isLoading: false,
                }))
            }
        } else {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes llenar el formulario',
                hasSnackbar: true
            }))
        }
    };

    return (
        <LocalizationProvider

            locale="es-MX"
            dateAdapter={AdapterDayjs} >
            <Container>
                <Box className="breadcrumb">
                    <BreadcrumbName>Carga Providencias Portal</BreadcrumbName>
                </Box>

                <CardRoot elevation={6}>


                    <CardTitle>
                        <Typography variant="h6" my={2}><b>{nombre}</b></Typography>
                        <Grid container spacing={2} >
                            <Grid item xs={12}>
                                <Typography variant="button" gutterBottom>
                                    Seleccione un archivo
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                autoFocus
                                    size="small"
                                    label="Descripción"
                                    value={form?.values?.descripcion_dta}
                                    onChange={(e) => setForm(prevForm => ({
                                        ...prevForm,
                                        values: {
                                            ...prevForm.values,
                                            descripcion_dta: e.target.value
                                        }
                                    }))}
                                    type="text"
                                    fullWidth
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                           
                         
                           

                        </Grid>
                        <Grid container mt={1} mb={2} spacing={4}>
                        <Grid item xs={12} md={3}>
                                <DatePicker
                                    onChange={(value) => setForm(prevForm => ({
                                        ...prevForm,
                                        values: {
                                            ...prevForm.values,
                                            fechaArchivo_dta: value.format('YYYY-MM-DD')
                                        }
                                    }))}

                                    value={dayjs(form?.values?.fechaArchivo_dta)}
                                    format="DD/MM/YYYY"
                                    slotProps={{ textField: { size: 'small', fullWidth: true, } }}

                                    label="Fecha Providencia"

                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                            <Button
        size="small"
        component="label"
        color="warning"
        variant="contained"
        startIcon={<CloudUploadIcon />}
        onKeyDown={handleKeyDown}
      >
        Escoger archivo
        <VisuallyHiddenInput 
          ref={fileInputRef}
          key={form?.values?.keyFile}
          type="file"
          accept=".txt, .pdf, .zip, .rar, .ppt, .pptx, .doc, .docx, .xls, .xlsx, .7zip, .tar" 
          onChange={({ target }) => {
            setForm(prevForm => ({
              ...prevForm,
              values: {
                ...prevForm?.values,
                archivo: target.files[0]
              }
            }));
            resetKeyFile();
          }}
        />
      </Button>
                            </Grid>
                        </Grid>
                        <Grid container>
                        {form?.values?.archivo && (
                            <Grid item md={5} sm={12} xs={12} mt={4} sx={{backgroundColor: '#ddf5dd'}}>
                   
                                    <Grid container alignItems="center">
                                        <Grid item md={10}>
                                            <Typography variant="body1" gutterBottom>
                                                <b>Nombre:</b> {form?.values?.archivo.name} <br/>  <b>Tamaño:</b> {(form?.values?.archivo?.size / 1024).toFixed(2)} KB
                                            </Typography>
                                        </Grid>
                                        <Grid item md={2} style={{ textAlign: 'right' }}>
                                            <IconButton
                                                size="small"
                                                color="error"
                                                onClick={() => {
                                                    setForm(prevForm => ({
                                                        ...prevForm,
                                                        values: {
                                                            ...prevForm?.values,
                                                            archivo: null,
                                                        }
                                                    }))
                                                    resetKeyFile();
                                                }}
                                            >
                                                <Icon>delete</Icon>
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                
                            </Grid>
                        )}
                    </Grid>
                        <Container sx={{mt:3}}>
                            
                                <LoadingButton loading={form?.isLoading} size="medium" variant="contained" color="primary" type="button" onClick={handleUpload} sx={{marginRight: 5}}>

                                    Procesar
                                </LoadingButton>

                                
                                <Button size="medium" variant="contained" color="inherit" type="button" startIcon={<Icon>cancel</Icon>} href="/portal/tiposArchivos" disabled={form?.isLoading}>

                                    Regresar
                                </Button>
                           
                        </Container>
                    </CardTitle>



                </CardRoot>
            </Container>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }
        </LocalizationProvider>
    )
}

export default PortalFileUpload