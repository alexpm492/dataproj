import { Alert, Box, Button, CardContent, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, Grid, Icon, IconButton, Snackbar, TableCell, TableRow, TextField, TableBody, TableHead, Input, InputAdornment, TablePagination } from '@mui/material';
import { Container, CardRoot, CardTitle, StyledTable, BreadcrumbName, StyledTableContent, } from '../core/styles'

import { INSTITUTION } from 'app/apiEndpoints/portal';
import { useEffect, useState } from 'react';
import { LoadingButton } from '@mui/lab';
import axios from 'axios.js'
import SearchIcon from '@mui/icons-material/Search';
import dayjs from 'dayjs';
const PortalInstitution = () => {
    const [data, setData] = useState(null);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [search, setSearch] = useState(null);
    const [form, setForm] = useState({
        values: {
            name: '',
        },
        error: null,
        isUpdating: false,
        hasSnackbar: false,
        isLoading: false,
        isShowDialog: false,
        isShowDialogForm: false
    });

    useEffect(() => {
        getData();
    }, [])

    useEffect(() => {
        if (search === '') {
            getData();
        }
      }, [search])


    const getData = async () => {
        setForm(prevForm => ({
            ...prevForm,
            isLoading: !prevForm.isLoading
        }))

        await axios.get(INSTITUTION, {
            params: {
              search
            }
          }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                if (Array.isArray(data?.data)) {
                    if (data?.data?.length > 0) {
                        setData(data?.data);
                    } else {
                        setForm(prevForm => ({
                            ...prevForm,
                            error: 'info|Sin datos!',
                            hasSnackbar: true
                        }));

                    }
                } else {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'error|Error al mostrar los datos!',
                        hasSnackbar: true
                    }))
                }




            } else if (status > 404) {

                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error en el servidor al obtener información, intentalo más tarde!',
                    hasSnackbar: true
                }))
            }

        }).catch(({ message }) => {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        }).finally(() => {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: !prevForm.isLoading
            }))
        })
    }

    const handleSubmit = async () => {
        const { name: name_group } = form?.values;
        if (name_group.trim() !== '') {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: true,
            }))
            await axios.post(INSTITUTION, {
                ...form?.values
            }).then(() => {
                setForm(prevForm => ({
                    ...prevForm,
                    values: {
                       name: ''
                    },

                    hasSnackbar: true
                }))
            }).catch(() => {
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error al registrar nueva institución!',
                    hasSnackbar: true
                }))
            })


            setForm(prevForm => ({
                ...prevForm,
                isLoading: false,
            }))
        } else {
            setForm(prevForm => ({
                ...prevForm,
                error: 'error|Debes llenar el formulario',
                hasSnackbar: true
            }))
        }
        await getData();

    }


    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };


    const handleChangePage = (_, newPage) => {
        setPage(newPage);
      };
    
      const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };

    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Instituciones</BreadcrumbName>
            </Box>

            

            <CardRoot elevation={6}>

                <CardTitle>
                    <LoadingButton loading={form?.isLoading} variant="contained" color="warning" type="button" onClick={()=>setForm(prevForm=>({
                        ...prevForm,
                        isShowDialogForm: true
                    }))}>
                        + Nueva Institución
                    </LoadingButton>


                </CardTitle>

                <Divider />

                <CardContent>
                    <Grid container justifyContent="flex-end" alignItems="center" spacing={2}>
                    <Grid item>
          <Input
          autoFocus
          disabled={form?.isLoading}
            value={search ?? ''}
            onChange={({ target }) => setSearch(target.value)}
            placeholder="Buscar..."
            endAdornment={

              <InputAdornment position="end">
                <IconButton onClick={() => {
                  if (search != null && search?.trim()?.length > 0) {
                    getData();
                    setPage(0);
                    setRowsPerPage(5);
                  }
                }}>
                  <SearchIcon />
                </IconButton>
              </InputAdornment>

            }
          />
        </Grid>
        {
          search !== null && search.trim()?.length > 0 ? (
            <Grid item>
              <IconButton color="error" onClick={() => {
                setSearch('')
                setPage(0);
                    setRowsPerPage(5);
              }}>
                <Icon>delete</Icon>
              </IconButton>
            </Grid>
          ) : null
        }
                        <div style={{ overflowX: 'auto', width: '100%' }}>
                            {
                               data !== null  & data?.length > 0 ? (
<>
                                    <StyledTableContent sx={{width:'100%'}}>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="left" >N.</TableCell>
                                                <TableCell align="left">Nombre</TableCell>
                                                <TableCell align="center">Creado por</TableCell>
                                                <TableCell align="center">Fecha</TableCell>
                                               
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                          
{data?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((institution, index) => (
                <TableRow key={index + institution?.id}>
                <TableCell align="left" >{index + 1}</TableCell>
                <TableCell align="left">{institution?.name}</TableCell>
                <TableCell align="center">{institution?.created_by}</TableCell>
                <TableCell align="center">{dayjs(institution?.created_at).format('DD/MM/YYYY HH:mm:ss')}</TableCell>

               
            </TableRow>
            ))}

                                        </TableBody>
                                    </StyledTableContent>
<TablePagination
  sx={{ px: 2 }}
  page={page}
  component="div"
  rowsPerPage={rowsPerPage}
  count={data?.length}
  onPageChange={handleChangePage}
  rowsPerPageOptions={[5, 10, 25,  { value: 5000, label: 'Todos' }]}
  onRowsPerPageChange={handleChangeRowsPerPage}
  nextIconButtonProps={{ "aria-label": "Siguiente Página" }}
  backIconButtonProps={{ "aria-label": "Página Anterior" }}
  labelRowsPerPage="Filas por página:"
  labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
/>
</>
                                ) : null
                            }
                        </div>
                    </Grid>
                </CardContent>


            </CardRoot>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }

            <Dialog onClose={(_, reason) => {
                if (reason === 'backdropClick' || reason === 'escapeKeyDown') return;
            }} open={form?.isShowDialogForm} disableEscapeKeyDown maxWidth={'sm'} fullWidth>
                <DialogTitle>Nueva Institucion</DialogTitle>
                <DialogContent>
                <TextField sx={{mt:4}}
                autoFocus
                                size="small"
                                label="Nombre Institucion"
                                value={form?.values?.name}
                                onChange={(e) =>{
                                    const { value } = e.target;
                                    const newValue = value.toUpperCase();
                                    setForm(prevForm => ({
                                        ...prevForm,
                                        values: {
                                            ...prevForm.values,
                                            name: newValue,
    
                                        }
                                    }))
                                }}
                                type="text"
                                fullWidth

                            />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setForm(prevForm => ({ ...prevForm, values:{
                     ...prevForm?.values,
                     name: ''
                    } ,isShowDialogForm: false }))}>Cancelar</Button>
                    <Button onClick={async () => {
                        setForm(prevForm => ({
                            ...prevForm,
                            isShowDialogForm: false
                        }));
                        handleSubmit();
                    }} autoFocus>
                        Continuar
                    </Button>
                </DialogActions>
            </Dialog>
        </Container >
    )
}

export default PortalInstitution