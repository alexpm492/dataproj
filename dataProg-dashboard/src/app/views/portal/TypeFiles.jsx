import { Button, Grid, Input, CardContent, IconButton, InputAdornment, CircularProgress, Snackbar, Alert, Icon } from '@mui/material';
import { Box } from '@mui/system';
import axios from 'axios.js'

import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';

import { pageLimit } from '../../utils/constant';
import SearchIcon from '@mui/icons-material/Search';

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';

import Paper from '@mui/material/Paper';
import { useEffect, useState } from 'react';
import { TYPEFILE_LIST } from '../../apiEndpoints/portal';
import { Span } from 'app/components/Typography';
import { Container, CardRoot, CardTitle, BreadcrumbName, StyledTableContent } from '../core/styles';




const tableLabels = ['Nº', 'Descripción', 'Grupo', 'Estado Archivo', 'Última actualización'];


const TypeFiles = () => {
    const [data, setData] = useState(null);
    const [form, setForm] = useState({
        error: null,
        isValid: false,
        hasSnackbar: false,
        isLoading: false,
    });

    const [dataControl, setDataControl] = useState({
        page: 1,
        pageLimit,
        isAsc: 0,
        orderBy: null,
        search: null,
        total: null,
        nextPage: null
    });


    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };


    useEffect(() => {
        getData();
    }, [])

    const getData = async (wasSearch = false) => {
        var pagination = dataControl;
        if (wasSearch) {
            pagination = {
                page: 1,
                pageLimit,
                isAsc: 0,
                orderBy: null,
                search: dataControl.search,
                total: null,
                nextPage: null
            };
            setDataControl(pagination);
        }
        setForm(prevForm => ({
            ...prevForm,
            isLoading: !prevForm.isLoading
        }))
        if (data != null) setData([]);

        await axios.get(TYPEFILE_LIST, {
            params: {
                ...pagination,

            }
        }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                setDataControl(prevData => ({
                    ...prevData,
                    orderBy: data?.orderBy,
                    page: data?.currentPage,
                    total: data?.total,
                    nextPage: data?.nextPage
                }));
                if (Array.isArray(data?.data)) {
                    if (data?.data.length > 0) {
                        setData(data?.data.map((row, id) => {

                            id++;
                            return {
                                id,
                                ...row,

                            }
                        }));
                    } else {
                        setForm(prevForm => ({
                            ...prevForm,
                            error: 'info|Sin datos!',
                            hasSnackbar: true
                        }));

                    }
                } else {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'error|Error al mostrar los datos!',
                        hasSnackbar: true
                    }))
                }




            } else if (status > 404) {

                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error en el servidor al obtener información, intentalo más tarde!',
                    hasSnackbar: true
                }))
            }

        }).catch((error) => {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${error?.message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        });
        setForm(prevForm => ({
            ...prevForm,
            isLoading: !prevForm.isLoading
        }))


    }
    useEffect(() => {
        if (data != null) {
            getData();
        }
    }, [dataControl?.isAsc, dataControl?.orderBy, dataControl?.page, dataControl?.pageLimit])

    const handleOrderBy = async (orderBy) => {

        setDataControl(prevControl => ({
            ...prevControl,
            orderBy
        }));

    }


    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Lista de Archivos</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>


                <CardTitle>
                    <Grid container spacing={2} mt={2}>

                        <Grid item>

                            <Button variant="contained" color="warning" type="button" href="/portal/nuevoArchivo" disabled={form?.isLoading}>
                                + Archivo Nuevo
                            </Button>

                        </Grid>

                    </Grid>

                </CardTitle>
                <CardContent>
                    <Grid container justifyContent="flex-end" alignItems="center" spacing={2}>


                        <Grid item>
                            <Input
                                autoFocus
                                disabled={form?.isLoading}
                                onChange={({ target }) => setDataControl(prevData => ({
                                    ...prevData,
                                    search: target.value
                                }))}
                                value={dataControl?.search ?? ''}
                                placeholder="Buscar..."
                                endAdornment={
                                    dataControl?.search !== null && dataControl.search?.trim() !== "" ? (
                                        <InputAdornment position="end">
                                            <IconButton onClick={() => {
                                                if (dataControl?.search !== null && dataControl.search?.trim() !== "") {
                                                    getData(true);
                                                }
                                            }}>
                                                <SearchIcon />
                                            </IconButton>
                                        </InputAdornment>
                                    ) : null
                                }
                            />
                        </Grid>
                        {
                            dataControl?.search !== null && dataControl?.search.trim()?.length > 0 ? (
                                <Grid item>
                                    <IconButton color="error" onClick={() => {
                                        setDataControl(prevData => ({
                                            ...prevData,
                                            orderBy: null,
                                            search: null
                                        }))
                                    }}>
                                        <Icon>delete</Icon>
                                    </IconButton>
                                </Grid>
                            ) : null
                        }



                        {
                            form.isLoading ? (<Grid sx={{ my: 4 }} container justify="center" alignItems="center">
                                <Grid item xs={12} align="center">
                                    <CircularProgress />
                                </Grid>
                            </Grid>) : null
                        }

                        {
                            (Array.isArray(data) && data?.length > 0) ? (
                                <CardContent>

                                    <TableContainer component={Paper} >

                                        <StyledTableContent sx={{ width: '100%' }} >
                                            <TableHead >
                                                <TableRow>
                                                    {
                                                        Object.keys(data[0]).map((k, index) => {
                                                            if (k === 'id_arc') return null;

                                                            return (
                                                                <TableCell sx={{ cursor: index > 0 && k !== 'last_update' ? 'pointer' : 'inherit', width: index === 3 ? '110px' : 'auto' }} align={index === 0 ? 'left' : 'left'} key={k + index} onClick={() => {
                                                                    return index > 0 && k !== 'last_update' ? handleOrderBy(k) : void 0;
                                                                }}>
                                                                    {tableLabels[index]}
                                                                    {(dataControl?.orderBy === k && index !== 0 && k !== 'last_update') && (
                                                                        <IconButton sx={{ cursor: "pointer" }} size="small" aria-label="direction" onClick={() => setDataControl(prevControl => ({
                                                                            ...prevControl,
                                                                            isAsc: Number(!prevControl.isAsc)
                                                                        }))}>
                                                                            {!dataControl?.isAsc ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />}
                                                                        </IconButton>
                                                                    )}
                                                                </TableCell>
                                                            );


                                                        })
                                                    }
                                                    <TableCell align="center" sx={{ width: 350 }}>
                                                        Opciones
                                                    </TableCell>

                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    data.map((row, index) => (
                                                        <TableRow key={index + row?.id}>
                                                            <TableCell align="left" >{row?.id}</TableCell>
                                                            <TableCell align="left">{row?.descripcion_arc}</TableCell>
                                                            <TableCell align="left">{row?.name_group}</TableCell>
                                                            <TableCell align="left" >{row?.estado_arc}</TableCell>
                                                            <TableCell align="left">{row?.last_update}</TableCell>
                                                            <TableCell align="left">
                                                                <Grid container alignItems="center" justifyContent="center" spacing={2}>
                                                                    <Grid item>
                                                                        <Button disabled={row?.estado_arc === 'I'} href={`/portal/tiposArchivos/agregar/${row?.descripcion_arc}/${row?.id_arc}`} size="small" startIcon={<Icon>file_upload</Icon>} variant="contained" color="warning" type="button">
                                                                            <Span sx={{ fontSize: 10 }}>Cargar archivo</Span>
                                                                        </Button>
                                                                    </Grid>
                                                                    <Grid item>
                                                                        <Button disabled={row?.estado_arc === 'I'} href={`/portal/tiposArchivos/detalle/${row?.id_arc}`} size="small" startIcon={<Icon>list</Icon>} variant="contained" color="secondary" type="button">
                                                                            <Span sx={{ fontSize: 10 }}>Detalles</Span>
                                                                        </Button>
                                                                    </Grid>
                                                                    <Grid item>
                                                                        <Button size="small" startIcon={<Icon>edit</Icon>} href={`/portal/tiposArchivos/editar/${row?.id_arc}`} variant="contained" color="primary" type="button">
                                                                            <Span sx={{ fontSize: 10 }}>Editar</Span>
                                                                        </Button>
                                                                    </Grid>
                                                                </Grid>
                                                            </TableCell>

                                                        </TableRow>
                                                    ))
                                                }
                                            </TableBody>
                                        </StyledTableContent>

                                    </TableContainer>

                                </CardContent>

                            ) : null
                        }





                        {
                            (data != null && data.length > 0) ? (
                                <Grid item xs={12} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                                    <TablePagination
                                        rowsPerPageOptions={[1, 10, 25, 50, 100, { value: 5000, label: 'Todos' }]}
                                        component="div"
                                        count={dataControl?.total ?? 0}
                                        rowsPerPage={dataControl?.pageLimit}
                                        page={dataControl?.page != null ? dataControl.page - 1 : 0}
                                        onPageChange={(e, page) => {

                                            setDataControl(prevData => ({
                                                ...prevData,
                                                page: page + 1
                                            }))
                                        }}
                                        onRowsPerPageChange={(e) => setDataControl(prevData => ({
                                            ...prevData,
                                            pageLimit: parseInt(e.target.value, 10),
                                            page: 1
                                        }))}
                                        labelRowsPerPage="Filas por página:" // Personaliza el texto para las filas por página
                                        labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`} // Personaliza el texto para las filas mostradas
                                        backIconButtonText="Anterior" // Personaliza el texto del botón de retroceso
                                        nextIconButtonText="Siguiente" // Personaliza el texto del botón de siguiente
                                    />
                                </Grid>
                            ) : null
                        }
                    </Grid>
                </CardContent>


            </CardRoot>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }
        </Container >
    )
}

export default TypeFiles;