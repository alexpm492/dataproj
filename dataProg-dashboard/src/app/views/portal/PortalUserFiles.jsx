import { LoadingButton } from '@mui/lab';
import { Checkbox, DialogActions, DialogContent, DialogContentText,Dialog, DialogTitle, FormControlLabel, Snackbar, Icon } from '@mui/material';
import { Alert } from '@mui/material';
import { Card, Button, Grid, CardContent, TextField } from '@mui/material';
import { Box, styled } from '@mui/system';
import { CURRENT_USER_FILE } from 'app/apiEndpoints/portal';
import { Span } from 'app/components/Typography';
import axios from 'axios.js'
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import {Container, CardRoot, CardTitle, BreadcrumbName} from '../core/styles'


const PortalUserFiles = () => {
    const { id } = useParams();
    const [allowedIds, setAllowedIds] = useState([]);
    const [deleteIds, setDeleteIds] = useState([]);
    const [currentUser, setCurrentUser] = useState(null);
    const [form, setForm] = useState({
        error: null,
        hasSnackbar: false,
        isLoading: false,
        isShowDialog: false,
    })



    useEffect(() => {
        getCurrentUser();
    }, [])

    const handleCheckboxChange = (id, checked) => {
        if (checked) {
            setAllowedIds(prevState => [...prevState, id]);
            setDeleteIds(prevState => prevState.filter(deleteId => deleteId !== id));
        } else {
            setDeleteIds(prevState => [...prevState, id]);
            setAllowedIds(prevState => prevState.filter(allowedId => allowedId !== id));
        }
    };

    const getCurrentUser = async () => {
        await axios.get(CURRENT_USER_FILE(id))
            .then((response) => {
                const { status, data } = response;
                if (status === 200) {
                    setCurrentUser(data);
                    setAllowedIds(data?.data?.filter((item) => item.allowed)?.map(e => e.id))
                } else {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'info|Sin datos - Dias Archivo!',
                        hasSnackbar: true
                    }));
                }
            }).catch(() => {
                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error al obtener los datos - Dias Archivo!',
                    hasSnackbar: true
                }))
            });

    }



    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };

    const handleSubmit = async () => {
       try {
        setForm(prevForm => ({
            ...prevForm,
            isLoading: true,
        }));
        if (allowedIds.length > 0) {
            await axios.post(CURRENT_USER_FILE(id), {
                arc_ids: allowedIds
            })
        }
        if (deleteIds.length > 0) {
            await axios.put(CURRENT_USER_FILE(id), {
                allowed_ids: deleteIds
            });
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: true,
           
        }));
       } catch (error) {
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: true,
            error: 'error|Ocurrio un error al actualizar los accesos'
        }));
       }finally{
        setForm(prevForm => ({
            ...prevForm,
            isLoading: false,
        }));
       }
    }

    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Editar Tipo de Archivo</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>


                <CardTitle>
                    <Grid container spacing={2} mt={2}>
                        <Grid item xs={3}>
                            <TextField
                                size="small"
                                label="Nombres"
                                disabled
                                value={currentUser?.user?.name ?? ''}

                                type="text"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <TextField
                                size="small"
                                label="Apellidos"
                                disabled
                                value={currentUser?.user?.lastname ?? ''}

                                type="text"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>

                    </Grid>

                    <Grid container spacing={2} mt={2}>
                        <Grid item xs={4}>
                            <TextField
                                size="small"
                                label="Institución Financiera"
                                disabled
                                value={currentUser?.user?.financialInstitution ?? ''}

                                type="text"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>


                    </Grid>

                </CardTitle>
                <CardContent>
                    <Grid container spacing={2}>
                        {currentUser?.data?.map(item => (
                            <Grid item xs={12} sm={6} md={4} key={item.id}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={allowedIds.includes(item.id)}
                                            onChange={(event) => handleCheckboxChange(item.id, event.target.checked)}
                                        />
                                    }
                                    label={item.description}
                                />
                            </Grid>
                        ))}
                    </Grid>
                    <Container sx={{mt:3}}>
                       
                       <LoadingButton sx={{marginRight: 5}} loading={form?.isLoading} size="medium" variant="contained" color="primary" type="button" onClick={() => setForm(prevForm => ({
                            ...prevForm,
                            isShowDialog: true
                        }))}>

                            Procesar
                        </LoadingButton>
 

                                <Button size="medium" variant="contained" color="inherit" type="button" startIcon={<Icon>cancel</Icon>} href="/portal/usuarios">

                                    Cancelar
                                </Button>
            
                    </Container>
                </CardContent>


            </CardRoot>
            <>
                {
                    form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                        <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                            {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                        </Alert>
                    </Snackbar>) : null
                }
                <Dialog onClose={(_, reason) => {
                    if (reason === 'backdropClick' || reason === 'escapeKeyDown') return;
                }} open={form?.isShowDialog} disableEscapeKeyDown>
                    <DialogTitle>¿Confirmar acción?</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <Span>Esta seguro de cambiar los permisos de acceso del usuario?</Span>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setForm(prevForm => ({ ...prevForm, isShowDialog: false }))}>Cancelar</Button>
                        <Button onClick={async () => {
                            setForm(prevForm => ({
                                ...prevForm,
                                isShowDialog: false
                            }));
                            handleSubmit();
                        }} autoFocus>
                            Continuar
                        </Button>
                    </DialogActions>
                </Dialog>
            </>
        </Container >
    )
}

export default PortalUserFiles