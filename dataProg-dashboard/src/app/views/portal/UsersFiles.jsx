import {
    Card,
    Button,
    Grid,
    CardContent,
    IconButton,
    Alert,
    Snackbar,
    CircularProgress,
    Icon,
    InputAdornment,
    Input
} from '@mui/material';
import { Box } from '@mui/system';
import axios from 'axios.js'

import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import SearchIcon from '@mui/icons-material/Search';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';

import Paper from '@mui/material/Paper';
import { useEffect, useState } from 'react';
import { USER_FILE } from 'app/apiEndpoints/portal';
import { pageLimit } from '../../utils/constant';
import { Span } from 'app/components/Typography';

import { Container, CardRoot, BreadcrumbName, StyledTableContent } from '../core/styles'


const tableLabels = ['Nº', 'Apellidos', 'Nombres', 'Entidad Financiera', 'Estado Usuario'];

const UsersFiles = () => {

    const [data, setData] = useState(null);

    const [form, setForm] = useState({
        error: null,
        hasSnackbar: false,
        isLoading: false,
    });

    const [dataControl, setDataControl] = useState({
        page: 1,
        pageLimit,
        isAsc: 0,
        orderBy: null,
        search: null,
        total: null,
        nextPage: null
    });


    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };


    const handleOrderBy = async (orderBy) => {

        setDataControl(prevControl => ({
            ...prevControl,
            orderBy
        }));

    }


    useEffect(() => {
        getData();
    }, [])

    useEffect(() => {
        if(dataControl?.search ===null){
            getData();
        }
      }, [dataControl?.search])

    const getData = async (wasSearch = false) => {
        var pagination = dataControl;
        if(wasSearch)
        {
            pagination = {
                page: 1,
                pageLimit,
                isAsc: 0,
                orderBy: null,
                search: dataControl.search,
                total: null,
                nextPage: null
            };
            setDataControl(pagination);
        }
        setForm(prevForm => ({
            ...prevForm,
            isLoading: !prevForm.isLoading
        }))
        if (data != null) setData([]);

        await axios.get(USER_FILE, {
            params: {
                ...pagination,

            }
        }).then((response) => {
            const { status, data } = response;

            if (status === 200) {
                setDataControl(prevData => ({
                    ...prevData,
                    orderBy: data?.orderBy,
                    page: data?.currentPage,
                    total: data?.total,
                    nextPage: data?.nextPage
                }));
                if (Array.isArray(data?.data)) {
                    if (data?.data.length > 0) {
                        setData(data?.data.map((row, ix) => {

                            ix++;
                            return {
                                ix,
                                ...row,

                            }
                        }));
                    } else {
                        setForm(prevForm => ({
                            ...prevForm,
                            error: 'info|Sin datos!',
                            hasSnackbar: true
                        }));

                    }
                } else {
                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'error|Error al mostrar los datos!',
                        hasSnackbar: true
                    }))
                }




            } else if (status > 404) {

                setForm(prevForm => ({
                    ...prevForm,
                    error: 'error|Error en el servidor al obtener información, intentalo más tarde!',
                    hasSnackbar: true
                }))
            }

        }).catch((error) => {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${error?.message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        });
        setForm(prevForm => ({
            ...prevForm,
            isLoading: !prevForm.isLoading
        }))


    }

    useEffect(() => {
        if (data != null) {
            getData();
        }
    }, [dataControl?.isAsc, dataControl?.orderBy, dataControl?.page, dataControl?.pageLimit])




    return (
        <Container>
            <Box className="breadcrumb">
                <BreadcrumbName>Usuarios Portal</BreadcrumbName>
            </Box>

            <CardRoot elevation={6}>

                <CardContent>
                    <Grid container justifyContent="flex-end" alignItems="center" spacing={2}>

                    <Grid item>
                            <Input
                            autoFocus
                                value={dataControl?.search ?? ''}
                                onChange={({ target }) => setDataControl(prev => ({
                                    ...prev,
                                    search: target.value
                                }))}
                                placeholder="Buscar..."
                                endAdornment={

                                    <InputAdornment position="end">
                                        <IconButton onClick={() => {
                                            if (dataControl?.search != null && dataControl?.search?.trim()?.length > 0) {
                                                getData(true)
                                            }
                                        }}>
                                            <SearchIcon />
                                        </IconButton>
                                    </InputAdornment>

                                }
                            />
                        </Grid>
                        {
                            dataControl?.search !== null && dataControl?.search.trim()?.length > 0 ? (
                                <Grid item>
                                    <IconButton color="error" onClick={() => setDataControl(prev => ({
                                        ...prev,
                                        search: null
                                    }))}>
                                        <Icon>delete</Icon>
                                    </IconButton>
                                </Grid>
                            ) : null
                        }
                    </Grid>
                    <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
                        



                        <div style={{ overflowX: 'auto', width: '100%' }}>
                            {
                                form.isLoading ? (<Grid sx={{ my: 4 }} container justify="center" alignItems="center">
                                    <Grid item xs={12} align="center">
                                        <CircularProgress />
                                    </Grid>
                                </Grid>) : null
                            }

                            {
                                (Array.isArray(data) && data?.length > 0) ? (
                                    <CardContent>
                                        <TableContainer component={Paper}>
                                            <StyledTableContent sx={{width: '100%'}}>
                                                <TableHead >
                                                    <TableRow>
                                                        {
                                                            Object.keys(data[0]).filter((k) => k !== 'id').map((k, index) => (

                                                                <TableCell
                                                                    sx={{ paddingLeft: index === 0 ? 5 : 0, cursor: index > 0 ? 'pointer' : 'inherit', width: index === 0 ? 100 : 'auto' }}
                                                                    align={index === 0 ? 'left' : 'left'} key={k + index}
                                                                    onClick={() => {
                                                                        return index > 0 ? handleOrderBy(k) : void 0;
                                                                    }}>
                                                                    {tableLabels[index]}
                                                                    {(dataControl?.orderBy === k && index !== 0) && (
                                                                        <IconButton
                                                                            sx={{ cursor: "pointer" }}
                                                                            size="small"
                                                                            aria-label="direction"
                                                                            onClick={() => setDataControl(prevControl => ({
                                                                                ...prevControl,
                                                                                isAsc: Number(!prevControl.isAsc)
                                                                            }))}>
                                                                            {!dataControl?.isAsc ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />}
                                                                        </IconButton>
                                                                    )}
                                                                </TableCell>

                                                            ))
                                                        }
                                                        <TableCell align="center">
                                                            Opciones
                                                        </TableCell>

                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {
                                                        data.map((row, index) => (
                                                            <TableRow key={index + row?.ix}>
                                                                <TableCell align="left" sx={{ paddingLeft: 5 }}>{row?.ix}</TableCell>
                                                                <TableCell align="left">{row?.lastname}</TableCell>
                                                                <TableCell align="left">{row?.name}</TableCell>
                                                                <TableCell align="left">{row?.financialInstitution}</TableCell>
                                                                <TableCell align="left">{row?.status}</TableCell>
                                                                <TableCell align="center">
                                                                    <Grid container alignItems="center" justifyContent="center" spacing={2}>
                                                                        <Grid item>
                                                                            <Button
                                                                                disabled={row?.status === 'Inactivo'}
                                                                                href={`/portal/usuarios/permisos/${row?.id}`}
                                                                                size="small" startIcon={<Icon>checklist</Icon>}
                                                                                variant="contained"
                                                                                color="warning"
                                                                                type="button">
                                                                                <Span
                                                                                    sx={{ fontSize: 10 }}
                                                                                >
                                                                                    Asignación
                                                                                </Span>
                                                                            </Button>
                                                                        </Grid>

                                                                    </Grid>
                                                                </TableCell>

                                                            </TableRow>
                                                        ))
                                                    }
                                                </TableBody>
                                            </StyledTableContent>
                                        </TableContainer>
                                    </CardContent>
                                ) : null
                            }




                        </div>
                        {
                            (data != null && data.length > 0) ? (
                                <Grid item xs={12} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                                    <TablePagination
                                        rowsPerPageOptions={[1, 10, 25, 50, 100, { value: 5000, label: 'Todos' }]}
                                        component="div"
                                        count={dataControl?.total ?? 0}
                                        rowsPerPage={dataControl?.pageLimit}
                                        page={dataControl?.page != null ? dataControl.page - 1 : 0}
                                        onPageChange={(e, page) => {

                                            setDataControl(prevData => ({
                                                ...prevData,
                                                page: page + 1
                                            }))
                                        }}
                                        onRowsPerPageChange={(e) => setDataControl(prevData => ({
                                            ...prevData,
                                            pageLimit: parseInt(e.target.value, 10),
                                            page: 1
                                        }))}
                                        labelRowsPerPage="Filas por página:" // Personaliza el texto para las filas por página
                                        labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`} // Personaliza el texto para las filas mostradas
                                        backIconButtonText="Anterior" // Personaliza el texto del botón de retroceso
                                        nextIconButtonText="Siguiente" // Personaliza el texto del botón de siguiente
                                    />
                                </Grid>
                            ) : null
                        }

                    </Grid>
                </CardContent>


            </CardRoot>
            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }
        </Container >
    )
}

export default UsersFiles;