import { Card, Table, Button, Autocomplete, TableRow } from '@mui/material';
import { styled } from '@mui/system';
import MuiAlert from '@mui/material/Alert';
import { forwardRef } from "react";
import { TextValidator } from "react-material-ui-form-validator";
export const Container = styled("div")(({ theme }) => ({
    margin: "30px",
    [theme.breakpoints.down("sm")]: { margin: "16px" },
    "& .breadcrumb": {
        marginBottom: "30px",
        [theme.breakpoints.down("sm")]: { marginBottom: "16px" },
    },
}));

export const TextField = styled(TextValidator)(() => ({
    width: "100%",
    marginBottom: "16px",
}));

export const AutoComplete = styled(Autocomplete)(() => ({
    width: 300,
    marginBottom: '1px',
}));

export const CardRoot = styled(Card)(() => ({
    height: '100%',
    padding: '20px 24px',
}));

export const CardTitle = styled('div')(({ subtitle }) => ({
    fontSize: '1rem',
    fontWeight: '500',
    textTransform: 'capitalize',
    marginBottom: !subtitle && '16px',
}));


export const BreadcrumbName = styled('h4')(() => ({
    margin: 0,
    fontSize: '16px',
    paddingBottom: '1px',
    verticalAlign: 'middle',
    textTransform: 'capitalize',
}));

export const VisuallyHiddenInput = styled('input')({
    clip: 'rect(0 0 0 0)',
    clipPath: 'inset(50%)',
    height: 1,
    overflow: 'hidden',
    position: 'absolute',
    bottom: 0,
    left: 0,
    whiteSpace: 'nowrap',
    width: 1,
});

export const StyledTable = styled(Table)(() => ({
    width: 'auto',
    whiteSpace: "pre",
    "& thead": {
        "& tr": { "& th": { paddingLeft: 4, paddingRight: 4 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 4, paddingRight: 4, textTransform: "capitalize" } },
    },
}));

export const StyledTableContent = styled(Table)(() => ({
    "& thead": {
        "& tr": {
            "& th": {
                paddingLeft:8,
                paddingRight:8,
                whiteSpace: 'pre-line',
                wordBreak: 'break-word',
                "&:first-of-type": {
                    textAlign: "center",
                    width:100,
                    paddingLeft: 5, // Aplica paddingRight de 5 píxeles solo a la primera columna de encabezado
                }
            }
        },
    },
    "& tbody": {
        "& tr": {
            "& td": {
                paddingLeft:8,
                paddingRight:8,
                whiteSpace: 'pre-line',
                wordBreak: 'break-word',
                "&:first-of-type": {

                    paddingLeft: 5,
                   width:100,
                    textAlign: "center", // Aplica paddingRight de 5 píxeles solo a la primera columna de encabezado
                },
                
            }
        },
    },
}));

export const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last borde
}));
export const StyledButton = styled(Button)(({ theme }) => ({
    margin: theme.spacing(1),
}));

export const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});