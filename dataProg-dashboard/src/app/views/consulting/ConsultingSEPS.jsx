import { Button, Typography, Grid, TextField, TableContainer, Paper, Snackbar, Alert, CardContent } from '@mui/material';
import { Box } from '@mui/system';
import axios from 'axios.js'
import { saveAs } from "file-saver";
import {
    Icon,
    IconButton,
    TableBody,
    TableCell,
    TableHead,

    TableRow
} from "@mui/material";
import { Container, CardRoot, CardTitle, BreadcrumbName, StyledTable, StyledTableRow } from '../core/styles'
import { Fragment, useRef, useState } from 'react';
import { LoadingButton } from '@mui/lab';
import { Span } from 'app/components/Typography';
import { PROV_SEPS, PROV_SEPS_DOWNLOAD } from 'app/apiEndpoints/portal';
import { useEffect } from 'react';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';

const ConsultingSEPS = () => {
    const inputRef = useRef(null);
    const [data, setData] = useState({
        headers: null,
        rows: null
    });

    const [dataControl, setDataControl] = useState({
        isAsc: 0,
        orderBy: null,
    });

    const [form, setForm] = useState({
        values: {
            nombre: '',
            numeroDocumento: '',
            numeroJuicio: ''
        },
        error: null,
        hasSnackbar: false,
        isLoading: false,
        isUpdating: false
    });


    const getProvidences = async () => {
        setData({
            headers: null,
            rows: null,
        })
        setForm(prevForm => ({
            ...prevForm,
            isLoading: true
        }))
        await axios.get(PROV_SEPS, {
            params: {
                ...form.values,
                ...dataControl
            }
        }).then((response) => {
            const { status, data } = response;
            if (status === 200) {
                if (data !== null) {
                    if (data?.data?.length > 0) {
                        setData({
                            headers: ['Nº', ...data?.headers],
                            rows: data?.data?.map((row, ix) => ({ id: ++ix, ...row }))
                        });
                        setDataControl(prevData => ({
                            ...prevData,
                            orderBy: data?.orderBy
                        }))
                    } else {
                        setData({
                            headers: [],
                            rows: []
                        })


                    }
                } else {
                    setData({
                        headers: null,
                        rows: null
                    })
                    setForm(prevForm => ({
                        ...prevForm,
                        error: 'error|Error al mostrar los datos!',
                        hasSnackbar: true
                    }))
                }
            }
        }).catch(({ message }) => {
            setData({
                headers: null,
                rows: null
            })
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        }).finally(() => {
            setForm(prevForm => ({
                ...prevForm,
                isLoading: false
            }))
        });
    }
    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setForm(prevForm => ({
            ...prevForm,
            hasSnackbar: false,
            error: null
        }));
    };


    const handleSubmit = async () => {
        const { nombre, numeroDocumento, numeroJuicio } = form?.values
        if (nombre.trim() === '' && numeroDocumento.trim() === '' && numeroJuicio.trim() === '') {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|Debes llenar uno de los campos antes de continuar`,
                hasSnackbar: true
            }))
        } else {
            await getProvidences();
        }


    }

    useEffect(() => {
        if (data?.rows !== null) {
            getProvidences();
        }
    }, [dataControl?.isAsc, dataControl?.orderBy])


    const handleDownload = async (code) => {
        try {
            const id = code?.split('.')[0];
            const rs = await axios.get(PROV_SEPS_DOWNLOAD(id), {
                responseType: "arraybuffer",
            });
            const blob = new Blob([rs.data], { type: "octet/stream" });
            setForm(prevForm => ({
                ...prevForm,
                error: `info|Descargando archivo....`,
                hasSnackbar: true,
                isUpdating: true
            }))
            saveAs(blob, code);
        } catch ({ message }) {
            setForm(prevForm => ({
                ...prevForm,
                error: `error|${message ?? 'Ocurrio un error del lado del servidor, intentalo más tarde'}`,
                hasSnackbar: true
            }))
        } finally {
            setForm(prevForm => ({
                ...prevForm,

                isUpdating: false
            }))
        }
    }

    const handleOrderBy = async (orderBy) => {

        setDataControl(prevControl => ({
            ...prevControl,
            orderBy
        }));

    }

    const divideParagraph = (text) => {
        const arrayWords = text?.trim().split(' ');

        if (arrayWords?.length <= 10) {
            return <div style={{ width: '100%' }}>{text}</div>;
        }

        const dividedText = arrayWords.reduce((result, word, index) => {
            const currentIndex = Math.floor(index / 10);
            result[currentIndex] = (result[currentIndex] || '') + ' ' + word;
            return result;
        }, []).map((part, index) => <Fragment key={index}>{part}<br /></Fragment>);

        return <div style={{ width: '100%' }}>{dividedText}</div>;

    }

    return (
        <>
            <Container>
                <Box className="breadcrumb">
                    <BreadcrumbName>Consulta de Providencias Judiciales SEPS</BreadcrumbName>
                </Box>

                <CardRoot elevation={6}>


                    <CardTitle>
                        <Grid container spacing={2} justifyContent="center">
                            <Grid item xs={8}>
                                <Grid container spacing={1} alignItems="center">
                                    <Grid item xs={2}>
                                        <Typography variant="body1">Nombre:</Typography>
                                    </Grid>
                                    <Grid item xs>
                                        <TextField autoFocus size="small" variant="outlined" fullWidth value={form?.values?.nombre}
                                            onChange={(e) => setForm(prevForm => ({
                                                ...prevForm,
                                                values: {
                                                    ...prevForm.values,
                                                    nombre: e.target.value
                                                }
                                            }))} />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={8}>
                                <Grid container spacing={1} alignItems="center">
                                    <Grid item xs={2}>
                                        <Typography variant="body1">Número Documento:</Typography>
                                    </Grid>
                                    <Grid item xs>
                                        <TextField
                                         ref={inputRef}
                                            value={form?.values?.numeroDocumento}
                                            onChange={(e) => setForm(prevForm => ({
                                                ...prevForm,
                                                values: {
                                                    ...prevForm.values,
                                                    numeroDocumento: e.target.value
                                                }
                                            }))}
                                            size="small"
                                            variant="outlined"
                                            fullWidth
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={8}>
                                <Grid container spacing={1} alignItems="center">
                                    <Grid item xs={2}>
                                        <Typography variant="body1">Número de Juicio:</Typography>
                                    </Grid>
                                    <Grid item md>
                                        <TextField
                                            value={form?.values?.numeroJuicio}
                                            onChange={(e) => setForm(prevForm => ({
                                                ...prevForm,
                                                values: {
                                                    ...prevForm.values,
                                                    numeroJuicio: e.target.value
                                                }
                                            }))}
                                            size="small"
                                            variant="outlined"
                                            fullWidth
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid container spacing={2} justifyContent="center" mt={4} >

                            <LoadingButton type="button" color="primary" loading={form?.isLoading} variant="contained" onClick={handleSubmit} sx={{ marginRight: 5 }}>
                                <Icon fontSize="small">check</Icon>
                                <Span>Aceptar</Span>
                            </LoadingButton>


                            <Button
                                size="medium"
                                variant="contained"
                                color="inherit"
                                type="button"
                                disabled={form?.isLoading}
                                startIcon={<Icon>cancel</Icon>}
                                onClick={() => {
                                    setForm(prevForm => ({
                                        ...prevForm,
                                        values: {
                                            nombre: '',
                                            numeroDocumento: '',
                                            numeroJuicio: ''
                                        }
                                    }));
                                    setData({
                                        headers: null,
                                        rows: null
                                    })
                                    inputRef.current.focus();
                                    setDataControl({
                                        isAsc: 0,
                                        orderBy: null
                                    });
                                }}>

                                Cancelar
                            </Button>


                        </Grid>

                    </CardTitle>

                </CardRoot>
            </Container>
            <Container sx={{ mt: 4, mb: 6 }}>
                <CardRoot elevation={6}>
                    <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
                        <div style={{ overflowX: 'auto', width: '100%' }}>
                            {
                                data !== null && data?.rows?.length > 0 ? (

                                    <StyledTable sx={{ mt: 4 }}>
                                        <TableHead>
                                            <TableRow>

                                                {
                                                    Object.keys(data?.rows[0]).map((k, index) => (
                                                        <TableCell
                                                            style={{ whiteSpace: 'nowrap', overflow: 'hidden' }}
                                                            sx={{ border: '1px solid #ddd', paddingLeft: index === 0 ? 15 : 0, cursor: index > 0 && index < data?.headers.length - 1 ? 'pointer' : 'inherit', backgroundColor: data?.headers[index] === 'Num. Providencia' ? '#f54141' : 'transparent' }}
                                                            align={index === 0 ? 'center' : 'center'}
                                                            key={k + index} onClick={() => {
                                                                return index > 0 || data?.headers[index] !== 'Descargar' ? handleOrderBy(k) : void 0;
                                                            }}>
                                                            {data?.headers[index]}
                                                            {(dataControl?.orderBy === k && index > 0 && data?.headers[index] !== 'Descargar') && (
                                                                <IconButton sx={{ cursor: "pointer" }} size="small" aria-label="direction" onClick={() => setDataControl(prevControl => ({
                                                                    ...prevControl,
                                                                    isAsc: Number(!prevControl.isAsc)
                                                                }))}>
                                                                    {!dataControl?.isAsc ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />}
                                                                </IconButton>
                                                            )}
                                                        </TableCell>
                                                    ))
                                                }
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>


                                            {
                                                data?.headers?.includes('Num. Providencia') ? (
                                                    data?.rows?.map((row, index) => (
                                                        <StyledTableRow key={index + row?.id} >
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ paddingLeft: 5, border: '1px solid #ddd' }} >
                                                                {row?.id}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.codigoAnexo_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.documento_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.numeroDocumento_pse}
                                                            </TableCell>

                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>

                                                                {divideParagraph(row?.implicados_pse ?? '')}

                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.fechaArchivo_cin}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.ciudad_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>


                                                                {divideParagraph(row?.enteEmisor_pse ?? '')}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.numeroOficio_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.circular_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="center" sx={{ border: '1px solid #ddd' }} >
                                                                {row?.expediente_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="center" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.pagina_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.tipoJuicio_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.numeroJuicio_pse?.substring(0, 80)}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.accion_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {row?.valor_pse}
                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="center" sx={{ border: '1px solid #ddd' }}>{
                                                                row?.file !== '' ? (
                                                                    <IconButton disabled={form?.isUpdating} color="secondary" size="large" onClick={() => handleDownload(row?.file)}>
                                                                        <Icon>download</Icon>
                                                                    </IconButton>
                                                                ) : (
                                                                    <span style={{ backgroundColor: '#6bbafb', paddingLeft: 5, paddingRight: 5, color: 'white' }}>Sin Archivo</span>
                                                                )
                                                            }</TableCell>
                                                        </StyledTableRow>
                                                    ))
                                                ) : (

                                                    data?.rows?.map((row, index) => (
                                                        <StyledTableRow key={index + row?.id} >
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ paddingLeft: 5, border: '1px solid #ddd' }}>{row?.id}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.documento_pse}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.numeroDocumento_pse}</TableCell>

                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {divideParagraph(row?.implicados_pse ?? '')}

                                                            </TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.fechaArchivo_cin}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.ciudad_pse}</TableCell>

                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>
                                                                {divideParagraph(row?.enteEmisor_pse ?? '')}
                                                            </TableCell>

                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.numeroOficio_pse}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.circular_pse}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="center" sx={{ border: '1px solid #ddd' }}>{row?.expediente_pse}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="center" sx={{ border: '1px solid #ddd' }}>{row?.pagina_pse}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.tipoJuicio_pse}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.numeroJuicio_pse.substring(0, 80)}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.accion_pse}</TableCell>
                                                            <TableCell style={{ whiteSpace: 'nowrap', overflow: 'hidden' }} align="left" sx={{ border: '1px solid #ddd' }}>{row?.valor_pse}</TableCell>
                                                            <TableCell >{
                                                                row?.file !== '' ? (
                                                                    <IconButton disabled={form?.isUpdating} color="secondary" size="large" onClick={() => handleDownload(row?.file)}>
                                                                        <Icon>download</Icon>
                                                                    </IconButton>
                                                                ) : (
                                                                    <span style={{ backgroundColor: '#6bbafb', paddingLeft: 5, paddingRight: 5, color: 'white' }}>Sin Archivo</span>
                                                                )
                                                            }</TableCell>
                                                        </StyledTableRow>
                                                    ))
                                                )
                                            }
                                        </TableBody>
                                    </StyledTable>



                                ) : null
                            }
                            {
                                Array.isArray(data?.rows) && data.rows?.length === 0 ? (
                                    <div style={{ width: '100%' }}>
                                        <Alert severity="info">
                                            <Typography alignItems="center" sx={{ width: '100%', textAlign: 'center' }}>
                                                No existen registros con los párametros de búsqueda ingresados
                                            </Typography>
                                        </Alert>
                                    </div>
                                ) : null
                            }
                        </div>
                    </Grid>
                </CardRoot>
            </Container>

            {
                form.hasSnackbar ? (<Snackbar open={form.hasSnackbar} vertical="top" horizontal="right" autoHideDuration={4000} onClose={handleCloseMessage} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                    <Alert onClose={handleCloseMessage} severity={form.error != null ? form.error?.split('|')[0] : 'success'} sx={{ width: '100%' }} variant="filled">
                        {form.error != null ? form.error?.split('|')[1] : 'El proceso se realizo con éxito!'}
                    </Alert>
                </Snackbar>) : null
            }

        </>
    )
}


export default ConsultingSEPS;