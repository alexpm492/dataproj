import Loadable from 'app/components/Loadable';
import { lazy } from 'react';
import { authRoles } from '../../auth/authRoles';

const ConsultingSEPS = Loadable(lazy(() => import('./ConsultingSEPS')));
const ConsultingSB = Loadable(lazy(() => import('./ConsultingSB')));



const consultingRoutes = [
    { path: '/consultaSB', element: <ConsultingSB />, auth: authRoles.admin },
    { path: '/consultaSEPS', element: <ConsultingSEPS />, auth: authRoles.admin },
    //  { path: '/reportes/consultaSEPS', element: <UploadFileSB />, auth: authRoles.guest },


    ConsultingSB
];

export default consultingRoutes;