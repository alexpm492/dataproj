import {
  Box,
  Icon,
  Button,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Grid,
  Input,
  InputAdornment,
  IconButton
} from "@mui/material";
import { LoadingButton } from '@mui/lab';
import SearchIcon from '@mui/icons-material/Search';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { ValidatorForm } from "react-material-ui-form-validator";
import { Span } from "app/components/Typography";

import { useState, useEffect } from "react";
import axios from 'axios.js'
import { PROFILE, PROFILES } from "app/apiEndpoints/portal";
import {StyledTable, StyledButton, StyledTableContent, BreadcrumbName} from '../core/styles'


const UsersSummary = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [userId, setUserId] = useState();
  const [loading, setLoading] = useState(false);
  const [openDialogDelete, setOpenDialogDelete] = useState(false);
  const [search, setSearch] = useState(null);

  /*const handleOpenDialogDelete = () => {
    setOpenDialogDelete(true)
  }*/

  const handleCloseDelete = () => {
    setOpenDialogDelete(false);
  };
  /*const handleUserId = (id) => {
    setUserId(id);
  };*/

  const handleChangePage = (_, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const [usersList, setListUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const response = await axios.get(PROFILES,{
      params: {
        search
      }
    })
    setListUsers(response.data?.data)
  }

  const deleteUser = async () => {
    setLoading(true);
    try {
      await axios.delete(PROFILE + '/' + userId)
      setLoading(false);
      setListUsers((current) =>
        current.filter((row) => row.id !== userId)
      );
      setOpenDialogDelete(false);
    } catch (e) {
      //setOpenMessage(true);
    }
  };
  useEffect(() => {
    if (search === null) {
      getUsers();
    }
  }, [search])

  return (
    <Box width="100%" overflow="auto">
      
      <Grid container justifyContent="flex-end" alignItems="center" spacing={2} mb={4}>


<Grid item>
  <Input
  autoFocus
  disabled={loading}
    value={search ?? ''}
    onChange={({ target }) => setSearch(target.value)}
    placeholder="Buscar..."
    endAdornment={

      <InputAdornment position="end">
        <IconButton onClick={() => {
          if (search != null && search?.trim()?.length > 0) {
            getUsers()
            setPage(0);
            setRowsPerPage(5);
          }
        }}>
          <SearchIcon />
        </IconButton>
      </InputAdornment>

    }
  />
</Grid>
{
  search !== null && search.trim()?.length > 0 ? (
    <Grid item>
      <IconButton color="error" onClick={() => setSearch(null)}>
        <Icon>delete</Icon>
      </IconButton>
    </Grid>
  ) : null
}
</Grid>
      <StyledTableContent sx={{width: '100%'}}>
        <TableHead>
          <TableRow>
            <TableCell align="left" >N.</TableCell>
            <TableCell align="left">Código</TableCell>
            <TableCell align="center">Descripción</TableCell>
            <TableCell align="right">Acción</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {usersList?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((subscriber, index) => (
              <TableRow key={index}>
                <TableCell align="left">{index + 1}</TableCell>
                <TableCell align="left">{subscriber.code}</TableCell>
                <TableCell align="center">{subscriber.description}</TableCell>
                <TableCell align="right">
                  <a href={`/perfiles/editar/${subscriber.id}`}>
                    <Button>
                      <Icon>edit</Icon>
                    </Button>
                  </a>
                  
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </StyledTableContent>

      <TablePagination
  sx={{ px: 2 }}
  page={page}
  component="div"
  rowsPerPage={rowsPerPage}
  count={usersList?.length}
  onPageChange={handleChangePage}
  rowsPerPageOptions={[5, 10, 25,  { value: 5000, label: 'Todos' }]}
  onRowsPerPageChange={handleChangeRowsPerPage}
  nextIconButtonProps={{ "aria-label": "Siguiente Página" }}
  backIconButtonProps={{ "aria-label": "Página Anterior" }}
  labelRowsPerPage="Filas por página:"
  labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
/>

      <Dialog open={openDialogDelete} onClose={handleCloseDelete}>
        <DialogTitle>Eliminar Perfil</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Está seguro de eliminar perfil?
          </DialogContentText>
          <ValidatorForm onSubmit={deleteUser} onError={() => null}>

            <LoadingButton
              type="submit"
              color="primary"
              loading={loading}
              variant="contained"
              sx={{ my: 2 }}
            >
              <Icon>done</Icon>
              <Span sx={{ pl: 1, textTransform: "capitalize" }}>Eliminar</Span>
            </LoadingButton>

            <StyledButton color="inherit" variant="contained" type="button" onClick={handleCloseDelete}>
              <Icon>cancel</Icon>
              <Span sx={{ pl: 1, textTransform: "capitalize" }}>Cerrar</Span>
            </StyledButton>

          </ValidatorForm>
        </DialogContent>
      </Dialog>
    </Box>
  );
};

export default UsersSummary;