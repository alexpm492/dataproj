import { Icon, Card, Button } from '@mui/material';
import { Box, styled } from '@mui/system';
import { Breadcrumb } from "app/components";
import UsersSummary from "./ProfilesSummary";
import {Container, CardRoot, CardTitle, StyledButton, BreadcrumbName} from '../core/styles'


const ProfilesTable = () => {
  return (
    <Container>
        <Box className="breadcrumb">
                <BreadcrumbName>Lista de Perfiles</BreadcrumbName>
            </Box>
      <CardRoot elevation={6}>
        <CardTitle>
          <StyledButton size="small" variant="contained" color="primary" href="/perfiles/nuevo">
            <Icon>add</Icon>
            Nuevo Perfil
          </StyledButton>
        </CardTitle>

     
        <UsersSummary />
      </CardRoot>
    </Container>
  );
};

export default ProfilesTable;
