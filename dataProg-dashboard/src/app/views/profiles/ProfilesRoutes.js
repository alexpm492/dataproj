import Loadable from 'app/components/Loadable';
import { lazy } from 'react';
import { authRoles } from '../../auth/authRoles';

const ProfilesTable = Loadable(lazy(() => import('./ProfilesTable')));
const NewProfile = Loadable(lazy(() => import('./NewProfile')));
const EditProfile = Loadable(lazy(() => import('./EditProfile')));

const usersRoutes = [
  { path: '/perfiles', element: <ProfilesTable />, auth: authRoles.admin },
  { path: '/perfiles/nuevo', element: <NewProfile />, auth: authRoles.admin },
  { path: '/perfiles/editar/:id', element: <EditProfile />, auth: authRoles.admin },
];

export default usersRoutes;