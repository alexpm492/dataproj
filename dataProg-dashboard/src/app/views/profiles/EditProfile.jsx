import {

  Grid,
  Icon,

  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Snackbar,
} from "@mui/material";
import { Breadcrumb } from "app/components";
import { Span } from "app/components/Typography";
import { useEffect, useState } from "react";
import {  ValidatorForm } from "react-material-ui-form-validator";
import axios from 'axios.js'
import { useParams } from 'react-router-dom';
import { LoadingButton } from '@mui/lab';
import { menu_items } from 'app/menu';
import { PROFILE } from "app/apiEndpoints/portal";
import { Container, TextField, StyledButton, Alert, BreadcrumbName } from '../core/styles'

const SimpleForm = () => {
  const { id } = useParams();
  const [state, setState] = useState({
    id: id,
    code: '',
    description: ''
  });

  const menu_items_filtered = menu_items.filter(item => item.show)

  const [menu, setMenu] = useState(menu_items);

  const [loading, setLoading] = useState(false);
  const [openMessage, setOpenMessage] = useState(false);
  const [error, setError] =  useState(null);
  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenMessage(false);
  };


  useEffect(() => {
    axios.get(PROFILE + '/' + id)
      .then(res => {
        setState({ ...state, code: res.data.code, description: res.data.description });
        if (res.data.menu) {
          let menu = menu_items.map((item, index) => {
            Array.from(res.data.menu).forEach(function (itemSave) {
              if (item.path === itemSave.path) {
                item.selected = itemSave.selected
              }
            });
            return item;
          });
          setMenu(menu);
        }
      })
      .catch()
  }, [])

  const handleSubmit = async (values) => {
    setLoading(true);
    try {
      let menu_values = Object.values(menu)
      let profileItem = menu_values.filter(item => item.path === '/perfiles');
      if (profileItem[0] && profileItem[0].selected) {
        menu_values.forEach(function (val, key) {
          if (val.path === '/perfiles/nuevo') {
            val.selected = true;
          }
        });
      }
      let userItem = menu_values.filter(item => item.path === '/usuarios');
      if (userItem[0] && userItem[0].selected) {
        menu_values.forEach(function (val, key) {
          if (val.path === '/usuarios/nuevo') {
            val.selected = true;
          }
        });
      }
      state.menu = menu_values;
      await axios.put(PROFILE+'/' + id, state)
      setLoading(false);
      setOpenMessage(true);
    } catch ({message}) {
      setLoading(false);
      setOpenMessage(true);
      setError(`error|${message ?? 'Ocurrio un error al editar el perfil, verifica los datos y vuelve a intentarlo'}`);
    }
  };

  const handleChange = (event) => {
    event.persist();
    setState({ ...state, [event.target.name]: event.target.value });
  };

  const handleChangeMenu = (item) => (event) => {
    item.selected = event.target.checked
    setMenu({ ...menu, item });
  };

  const {
    code,
    description
  } = state;

  return (
    <Container>
  <Box className="breadcrumb">
                <BreadcrumbName>Editar Perfil</BreadcrumbName>
            </Box>
     

      <ValidatorForm onSubmit={handleSubmit} onError={() => null}>
        <Grid container spacing={6}>
          <Grid item lg={6} md={6} sm={12} xs={12} sx={{ mt: 2 }}>
            <TextField
            autoFocus
              type="text"
              name="code"
              inputProps={{
                minLength: 4,
                maxLength: 15
              }}
              value={code || ""}
              onChange={(e) => {
                const { name, value } = e.target;
                const newValue = value.replace(/[^a-zA-Z0-9 ]/g, '').toUpperCase();
                setState(prevState => ({
                  ...prevState,
                  [name]: newValue
                }));
              }}
              errorMessages={["Campo obligatorio"]}
              label="Código (Min  4, Max  15)"
              validators={["required", "minStringLength: 4", "maxStringLength: 15"]}
            />

            <TextField
              type="text"
              name="description"
              label="Descripción"
              onChange={handleChange}
              value={description || ""}
              validators={["required"]}
              errorMessages={["Campo obligatorio"]}
            />
          </Grid>
          <Grid item lg={6} md={6} sm={12} xs={12} sx={{ mt: 2 }}>
            <FormControl component="fieldset" className="formControl">
              <FormLabel component="legend">Menú</FormLabel>
              <FormGroup>
                {menu_items_filtered.map((item, index) => (
                  <FormControlLabel
                    control={<Checkbox checked={item.selected} onChange={handleChangeMenu(item)} value="item" />}
                    label={item.name} key={index}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Grid>
        </Grid>
        <LoadingButton
          type="submit"
          color="primary"
          loading={loading}
          variant="contained"
          sx={{ my: 2 }}
        >
          <Icon>done</Icon>
          <Span sx={{ pl: 1, textTransform: "capitalize" }}>Guardar</Span>
        </LoadingButton>
        <StyledButton color="inherit" variant="contained" type="button" href="/perfiles" disabled={loading}>
          <Icon>cancel</Icon>
          <Span sx={{ pl: 1, textTransform: "capitalize" }}>Cancelar</Span>
        </StyledButton>
        <Snackbar open={openMessage} vertical="top" horizontal="right" autoHideDuration={6000} onClose={handleCloseMessage}  anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
      <Alert onClose={handleCloseMessage} severity={error != null ? error?.split('|')[0] : 'success'} sx={{ width: '100%' }}>
          {error != null ? error?.split('|')[1] : 'Guardado con éxito!'}
        </Alert>
      </Snackbar>
      </ValidatorForm>
    </Container>
  );
};

export default SimpleForm;