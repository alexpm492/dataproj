import { LoadingButton } from '@mui/lab';
import { Card, Grid, TextField } from '@mui/material';
import { Box, styled } from '@mui/system';
import useAuth from 'app/hooks/useAuth';
import { Formik } from 'formik';
import { useState, forwardRef, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import axios from 'axios.js'
import { PUBLIC_IP } from 'app/apiEndpoints/portal';

const Alert = forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const FlexBox = styled(Box)(() => ({ display: 'flex', alignItems: 'center' }));

const JustifyBox = styled(FlexBox)(() => ({ justifyContent: 'center' }));

const ContentBox = styled(Box)(() => ({
  height: '100%',
  padding: '32px',
  position: 'relative',
  background: 'rgba(0, 0, 0, 0.01)',
}));

const JWTRoot = styled(JustifyBox)(() => ({
  backgroundImage: `url(/assets/images/dataproj/image2.svg)`, // Use the url function to set the background image
  backgroundSize: 'cover', // Adjust the background size based on your requirements
  backgroundRepeat: 'no-repeat', // Adjust the background repeat based on your requirements
  minHeight: '100% !important',

  '& .card': {
    maxWidth: 800,
    minHeight: 400,
    margin: '1rem',
    display: 'flex',
    borderRadius: 12,
    alignItems: 'center',
  },
}));

// inital login credentials
const initialValues = {
  username: '',
  password: '',
  remember: true,
};

// form field validation schema
const validationSchema = Yup.object().shape({
  password: Yup.string()
    .min(8, 'La contraseña debe tener al menos 8 caracteres')
    .required('La contraseña es requerida!'),
  username: Yup.string().required('El usuario es requerido'),
});

const JwtLogin = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [publicIp, setPublicIp] = useState('');

  const [openMessage, setOpenMessage] = useState(false);

  useEffect(()=>{
    getPublicIp();

  },[]);

  const getPublicIp = async () => {
   await fetch(PUBLIC_IP)
      .then(response => response.json())
      .then(data => setPublicIp(data?.ip ?? ''))
      .catch(error =>console.error(error));
  }

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenMessage(false);
  };

  const { login } = useAuth();
  const handleFormSubmit = async (values) => {
    setLoading(true);
    try {
      await login(values.username, values.password, publicIp);
      navigate('/');
    } catch (e) {
      setLoading(false);
      setOpenMessage(true);
    }
  };

  return (
    <JWTRoot >

      <Card className="card">
        <Grid container>
          <Grid item sm={6} xs={12}>
            <JustifyBox p={4} height="100%" sx={{ minWidth: 320 }}>
              <img src="/assets/images/dataproj/logoMantelcoam.svg" width="100%" alt="" />
            </JustifyBox>
          </Grid>

          <Grid item sm={6} xs={12}>
            <ContentBox>
              <Formik
                onSubmit={handleFormSubmit}
                initialValues={initialValues}
                validationSchema={validationSchema}

              >
                {({ values, errors, touched, handleChange, handleBlur, handleSubmit }) => (
                  <form onSubmit={handleSubmit} autoComplete="off" >
                    <TextField
                    autoFocus
                      fullWidth
                      size="small"
                      type="text"
                      sx={{ mb: 3 }}
                      name="username"
                      label="Usuario"
                      variant="outlined"
                      onBlur={handleBlur}
                      value={values.username}
                      onChange={(e) => {
                        // Obtener el valor del campo
                        const value = e.target.value;
                        // Verificar si el valor es una cadena de texto antes de convertirlo a mayúsculas
                        if (typeof value === 'string') {
                          // Aplicar la transformación a mayúsculas antes de actualizar el estado
                          const uppercaseValue = value.toUpperCase();
                          // Actualizar el valor del campo con el valor en mayúsculas
                          e.target.value = uppercaseValue;
                        }
                        // Llamar a handleChange de Formik para actualizar el estado interno
                        handleChange(e);
                      }}
                      helperText={touched.username && errors.username}
                      error={Boolean(errors.username && touched.username)}
                    />

                    <TextField
                      fullWidth
                      size="small"
                      name="password"
                      type="password"
                      label="Password"
                      variant="outlined"
                      onBlur={handleBlur}
                      value={values.password}
                      autoComplete="one-time-code"

                      onChange={handleChange}
                      helperText={touched.password && errors.password}
                      error={Boolean(errors.password && touched.password)}
                      sx={{ mb: 1.5 }}

                    />

                    {/*  <FlexBox justifyContent="space-between">
                      <FlexBox gap={1}>
                        <Checkbox
                          size="small"
                          name="remember"
                          onChange={handleChange}
                          checked={values.remember}
                          sx={{ padding: 0 }}
                        />

                        <Paragraph>Recuerdame</Paragraph>
                      </FlexBox>
                </FlexBox>*/}

                    <LoadingButton
                      type="submit"
                      color="primary"
                      loading={loading}
                      variant="contained"
                      sx={{ my: 2 }}
                    >
                      Ingresar
                    </LoadingButton>
                  </form>
                )}
              </Formik>
              <Snackbar open={openMessage} vertical="top" horizontal="right" autoHideDuration={6000} onClose={handleClose} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                  Usuario y/o contraseña incorrectas!
                </Alert>
              </Snackbar>
            </ContentBox>
          </Grid>
        </Grid>
      </Card>
    </JWTRoot>
  );
};

export default JwtLogin;
