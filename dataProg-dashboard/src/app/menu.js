export const menu_items = [
   //{ name: 'INICIO', path: '/dashboard/default', selected: false, show: true },
    { name: 'ADMINISTRADOR -> Perfil', path: '/perfiles', selected: false, show: true },
    { name: 'ADMINISTRADOR -> Crear Perfil', path: '/perfiles/nuevo', selected: false, show: false },
    { name: 'ADMINISTRADOR -> Usuarios', path: '/usuarios', selected: false, show: true },
    { name: 'ADMINISTRADOR -> Ingresos', path: '/ingresos', selected: false, show: true },
    { name: 'ADMINISTRADOR -> Crear Usuarios', path: '/usuarios/nuevo', selected: false, show: false },
    { name: 'ADMINISTRADOR -> Instituciones', path: '/portal/nuevaInstitucion', selected: false, show: true },


    //{ name: 'PORTAL', path: '/portal', selected: false, show: true },

    { name: 'PORTAL -> Tipos Archivos', path: '/portal/tiposArchivos', selected: false, show: true },
    { name: 'PORTAL -> Nuevo Archivo', path: '/portal/nuevoArchivo', selected: false, show: true },

    { name: 'PORTAL -> Nuevo Grupo', path: '/portal/nuevoGrupo', selected: false, show: true },
    { name: 'PORTAL -> Usuarios', path: '/portal/usuarios', selected: false, show: true },
    { name: 'PORTAL -> Reporte Descargas', path: '/portal/reporteDescargas', selected: false, show: true },

    //{ name: 'CARGA CONSULTAS', path: '/consultas', selected: false, show: true },
    { name: 'CARGA CONSULTAS -> Cargar Insumo SEPS', path: '/consultas/cargarInsumoSEPS', selected: false, show: true },
    { name: 'CARGA CONSULTAS -> Cargar Insumo SB', path: '/consultas/cargarInsumoSB', selected: false, show: true },
    { name: 'CARGA CONSULTAS -> Cargar Providencias SEPS', path: '/consultas/cargarProvidenciaSEPS', selected: false, show: true },
    { name: 'CARGA CONSULTAS -> Cargar Providencias SB', path: '/consultas/cargarProvidenciaSB', selected: false, show: true },
    { name: 'CARGA CONSULTAS -> Reporte de Providencias Judiciales Cargadas', path: '/consultas/providenciasJudiciales', selected: false, show: true },
    { name: 'DESCARGA', path: '/descargas/default', selected: false, show: true },
    { name: 'CONSULTA SB', path: '/consultaSB', selected: false, show: true },
    { name: 'CONSULTA SEPS', path: '/consultaSEPS', selected: false, show: true },
    { name: 'SESION', path: '/sesion', selected: false, show: true },
];