<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    /*CREATE TABLE `files_group` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name_group` varchar(100) NOT NULL,
  `created_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updatet_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_group` (`name_group`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `files_group_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
      */
    public function up()
    {
        Schema::create('files_group', function (Blueprint $table) {
            $table->id();
            $table->string('name_group')->unique();
            $table->string('created_by');

            // Define the foreign key constraint
            $table->foreign('created_by')->references('username')->on('users')->onDelete('restrict')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files_group');
    }
};
