<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */

    /**
     * CREATE TABLE `users_downloads_logs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `arc_id` int NOT NULL,
  `arc_detail` varchar(100) NOT NULL,
  `download_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `arc_id` (`arc_id`),
  CONSTRAINT `users_downloads_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `users_downloads_logs_ibfk_2` FOREIGN KEY (`arc_id`) REFERENCES `dts_archivos` (`id_arc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
     */
    public function up()
    {
        Schema::create('users_downloads_logs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->integer('arc_id')->unsigned();
            $table->string('arc_detail', 100);
            $table->timestamp('download_at');

            // Define foreign key constraints
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('arc_id')->references('id_arc')->on('dts_archivos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_downloads_logs');
    }
};
