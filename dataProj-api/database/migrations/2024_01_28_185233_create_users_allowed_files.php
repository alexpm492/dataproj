<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    /**
     * 
     * CREATE TABLE `users_allowed_files` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `arc_id` int NOT NULL,
  `created_by` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `arc_id` (`arc_id`),
  CONSTRAINT `users_allowed_files_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `users_allowed_files_ibfk_2` FOREIGN KEY (`arc_id`) REFERENCES `dts_archivos` (`id_arc`)
) 
     */

    public function up()
    {
        Schema::create('users_allowed_files', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->integer('arc_id')->unsigned();
            $table->string('created_by', 10);
            $table->timestamp('created_at')->nullable();

            // Define foreign key constraints
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('arc_id')->references('id_arc')->on('dts_archivos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_allowed_files');
    }
};
