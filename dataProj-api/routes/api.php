<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FormDataController;
use App\Http\Controllers\InstitutionController;
use App\Http\Controllers\PortalFileController;
use App\Http\Controllers\PortalFilesDetailsController;
use App\Http\Controllers\PortalGroupFileController;
use App\Http\Controllers\ProvidenceSBController;
use App\Http\Controllers\ProvidenceSEPSController;
use App\Http\Controllers\UploadSupplieController;
use App\Http\Controllers\UserDownloadController;
use App\Http\Controllers\UserFileController;
use App\Http\Controllers\UserLoginLogController;
use App\Models\PortalFile;
use App\Models\ProvidenceSB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [AuthController::class, 'login']);



//test
//Route::get('testSync', [ProvidenceSBController::class, 'testSync']);



Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('logout', [AuthController::class, 'logout']);
    Route::get('profiles', [ProfileController::class, 'profiles']);
    Route::post('profile', [ProfileController::class, 'save']);
    Route::get('profile/{id}', [ProfileController::class, 'get']);
    Route::put('profile/{id}', [ProfileController::class, 'update']);
    Route::delete('profile/{id}', [ProfileController::class, 'delete']);

    Route::get('users', [UserController::class, 'users']);
    Route::post('user', [UserController::class, 'save']);
    Route::put('user', [UserController::class, 'updatePassword']);
    Route::put('user/{id}', [UserController::class, 'update']);
    Route::get('user/{id}', [UserController::class, 'get']);
    Route::delete('user/{id}', [UserController::class, 'delete']);
    Route::patch('changeUserPassword/{id}', [UserController::class, 'changeUserPassword']);



    Route::post('userFiles/{user_id}', [UserFileController::class, 'save']);
    Route::put('userFiles/{user_id}', [UserFileController::class, 'delete']);
    //Route::delete('userFiles', [UserFileController::class, 'delete']);
    Route::get('userFiles/{user_id}', [UserFileController::class, 'get']);

    Route::get('userFiles', [UserFileController::class, 'getUsersWithDownloads']);
    Route::get('userFiles/list/report', [UserFileController::class, 'downloadList']);
    Route::get('userFiles/user/report', [UserDownloadController::class, 'getDownloaded']);



    Route::post('providenceSEPS', [ProvidenceSEPSController::class, 'save']);
    Route::post('providenceSEPS/store/{code}', [ProvidenceSEPSController::class, 'store']);
    Route::get('providenceSEPS/store/{code}', [ProvidenceSEPSController::class, 'getSepsPdf']);
    Route::get('providenceSEPS', [ProvidenceSEPSController::class, 'index']);
    Route::get('providenceSB', [ProvidenceSBController::class, 'index']);
    Route::post('providenceSB', [ProvidenceSBController::class, 'save']);
    Route::post('providenceSB/store', [ProvidenceSBController::class, 'store']);
    Route::get('providenceSB/sync', [ProvidenceSBController::class, 'syncFiles']);
    Route::get('providenceSB/store/{code}/{ext}', [ProvidenceSBController::class, 'getSBDocs']);
    Route::get('providenceSB/control/{id}', [ProvidenceSBController::class, 'controlById']);
    Route::get('providenceSEPS/control/{id}', [ProvidenceSEPSController::class, 'controlById']);
    Route::delete('providenceSB/control/{id}', [ProvidenceSBController::class, 'deleteUpload']);
    Route::delete('providenceSEPS/control/{id}', [ProvidenceSEPSController::class, 'deleteUpload']);


    Route::get('supplies', [UploadSupplieController::class, 'index']);

    //Porta routes
    Route::post('portalFiles', [PortalFileController::class, 'save']);
    Route::get('portalFiles', [PortalFileController::class, 'index']);
    Route::get('portalFiles/days/details/{id}', [PortalFileController::class, 'getDetails']);
    Route::get('portalFiles/{id}', [PortalFileController::class, 'get']);
    Route::put('portalFiles/{id}', [PortalFileController::class, 'update']);


    //Files
    Route::get('portalFiles/details/{id}', [PortalFilesDetailsController::class, 'get']);
    Route::post('portalFiles/details', [PortalFilesDetailsController::class, 'store']);
    Route::delete('portalFiles/details/{id}', [PortalFilesDetailsController::class, 'delete']);
    Route::get('portalFiles/download/{id}', [PortalFilesDetailsController::class, 'download']);


    //groups
    Route::get('group', [PortalGroupFileController::class, 'index']);
    Route::post('group', [PortalGroupFileController::class, 'add']);
    Route::delete('group/{id}', [PortalGroupFileController::class, 'deleteGroup']);
    //Route::get('groups', [PortalFileController::class, 'listGroups']);
    //institution
    Route::get('institution', [InstitutionController::class, 'index']);
    Route::post('institution', [InstitutionController::class, 'add']);
    //ReportDownload
    Route::get('reportDownloads', [UserDownloadController::class, 'index']);

    // Forms Data
    Route::get('dataForms/users', [FormDataController::class, 'getUsersForm']);
    Route::get('dataForms/files', [FormDataController::class, 'getFilesForm']);
    Route::get('dataForms/groups', [FormDataController::class, 'getGroupsForm']);
    Route::get('dataForms/institutions', [FormDataController::class, 'getInstitutionsForm']);
    // LoginUserReport
    Route::get('users/login/report', [UserLoginLogController::class, 'index']);
});
