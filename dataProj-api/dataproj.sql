-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: dataProj
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dts_archivos`
--

DROP TABLE IF EXISTS `dts_archivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dts_archivos` (
  `id_arc` int NOT NULL AUTO_INCREMENT,
  `descripcion_arc` char(100) NOT NULL,
  `estado_arc` char(1) NOT NULL,
  `fch_actualizacion_arc` date NOT NULL,
  `hor_actualizacion_arc` time NOT NULL,
  `group_id` int unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_arc`),
  UNIQUE KEY `descripcion_arc` (`descripcion_arc`),
  KEY `fk_group_id` (`group_id`),
  CONSTRAINT `fk_group_id` FOREIGN KEY (`group_id`) REFERENCES `files_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dts_archivos`
--

LOCK TABLES `dts_archivos` WRITE;
/*!40000 ALTER TABLE `dts_archivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `dts_archivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dts_carga_insumos`
--

DROP TABLE IF EXISTS `dts_carga_insumos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dts_carga_insumos` (
  `id_cin` int NOT NULL AUTO_INCREMENT,
  `fecha_cin` date NOT NULL,
  `hora_cin` time NOT NULL,
  `nombre_cin` char(30) NOT NULL,
  `fechaArchivo_cin` char(10) NOT NULL,
  `tipoInsumo_cin` char(5) NOT NULL,
  `id_usr` int NOT NULL,
  `id_tpv` int NOT NULL,
  PRIMARY KEY (`id_cin`)
) ENGINE=InnoDB AUTO_INCREMENT=5693 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dts_carga_insumos`
--

LOCK TABLES `dts_carga_insumos` WRITE;
/*!40000 ALTER TABLE `dts_carga_insumos` DISABLE KEYS */;
/*!40000 ALTER TABLE `dts_carga_insumos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dts_detalle_archivos`
--

DROP TABLE IF EXISTS `dts_detalle_archivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dts_detalle_archivos` (
  `id_dta` int NOT NULL AUTO_INCREMENT,
  `id_dar` int NOT NULL,
  `id_arc` int NOT NULL,
  `nombre_dta` char(100) NOT NULL,
  `fecha_dta` date NOT NULL,
  `hora_dta` time NOT NULL,
  `descripcion_dta` varchar(250) NOT NULL,
  `fechaArchivo_dta` date NOT NULL,
  `id_usr_carga` int NOT NULL,
  PRIMARY KEY (`id_dta`),
  KEY `id_dar` (`id_dar`),
  KEY `id_arc` (`id_arc`),
  CONSTRAINT `dts_detalle_archivos_ibfk_1` FOREIGN KEY (`id_arc`) REFERENCES `dts_archivos` (`id_arc`),
  CONSTRAINT `dts_detalle_archivos_ibfk_2` FOREIGN KEY (`id_dar`) REFERENCES `dts_dias_archivos` (`id_dar`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dts_detalle_archivos`
--

LOCK TABLES `dts_detalle_archivos` WRITE;
/*!40000 ALTER TABLE `dts_detalle_archivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `dts_detalle_archivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dts_dias_archivos`
--

DROP TABLE IF EXISTS `dts_dias_archivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dts_dias_archivos` (
  `id_dar` int NOT NULL AUTO_INCREMENT,
  `descripcion_dar` char(40) NOT NULL,
  `estado_dar` char(1) NOT NULL,
  PRIMARY KEY (`id_dar`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dts_dias_archivos`
--

LOCK TABLES `dts_dias_archivos` WRITE;
/*!40000 ALTER TABLE `dts_dias_archivos` DISABLE KEYS */;
INSERT INTO `dts_dias_archivos` VALUES (1,'LUNES','A'),(2,'MARTES','A'),(3,'MIERCOLES','A'),(4,'JUEVES','A'),(5,'VIERNES','A'),(6,'SABADO','I'),(7,'DOMINGO','I');
/*!40000 ALTER TABLE `dts_dias_archivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dts_providencias_sb`
--

DROP TABLE IF EXISTS `dts_providencias_sb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dts_providencias_sb` (
  `id_psb` int NOT NULL AUTO_INCREMENT,
  `circular_psb` varchar(100) NOT NULL,
  `fechaCircular_psb` varchar(20) NOT NULL,
  `tiempoRespuesta_psb` varchar(39) NOT NULL,
  `enteEmisor_psb` varchar(250) NOT NULL,
  `ciudad_psb` varchar(100) NOT NULL,
  `remitente_psb` varchar(100) NOT NULL,
  `cargoRemitente_psb` varchar(100) NOT NULL,
  `numeroOficio_psb` varchar(500) DEFAULT NULL,
  `numeroJuicio_psb` varchar(500) DEFAULT NULL,
  `tipoJuicio_psb` varchar(500) DEFAULT NULL,
  `accion_psb` varchar(500) DEFAULT NULL,
  `valor_psb` varchar(50) NOT NULL,
  `implicadosPerJur_psb` varchar(550) NOT NULL,
  `implicadosPerNat_psb` varchar(550) NOT NULL,
  `sinDeterImplicado_psb` varchar(550) NOT NULL,
  `documento_psb` varchar(50) NOT NULL,
  `numeroDocumento_psb` varchar(50) NOT NULL,
  `observacion_psb` text NOT NULL,
  `id_carga_providencia` int NOT NULL,
  PRIMARY KEY (`id_psb`),
  KEY `numeroDocumento_psb_idx` (`numeroDocumento_psb`),
  KEY `numeroJuicio_psb_idx` (`numeroJuicio_psb`),
  KEY `circular_psb_idx` (`circular_psb`),
  KEY `idx_carga_providencia` (`id_carga_providencia`) USING BTREE,
  KEY `sinDeterImplicado_psb_idx` (`sinDeterImplicado_psb`(255)),
  KEY `implicadosPerNat_psb_idx` (`implicadosPerNat_psb`(255)),
  KEY `implicadosPerJur_psb_idx` (`implicadosPerJur_psb`(255)),
  CONSTRAINT `dts_providencias_sb_ibfk_1` FOREIGN KEY (`id_carga_providencia`) REFERENCES `dts_carga_insumos` (`id_cin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dts_providencias_sb`
--

LOCK TABLES `dts_providencias_sb` WRITE;
/*!40000 ALTER TABLE `dts_providencias_sb` DISABLE KEYS */;
/*!40000 ALTER TABLE `dts_providencias_sb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dts_providencias_seps`
--

DROP TABLE IF EXISTS `dts_providencias_seps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dts_providencias_seps` (
  `id_pse` int NOT NULL AUTO_INCREMENT,
  `circular_pse` varchar(1000) DEFAULT NULL,
  `expediente_pse` varchar(1000) DEFAULT NULL,
  `pagina_pse` varchar(5) NOT NULL,
  `fechaCircular_pse` varchar(20) NOT NULL,
  `fechaOficio_pse` varchar(20) NOT NULL,
  `tiempoRespuesta_pse` varchar(250) DEFAULT NULL,
  `enteEmisor_pse` varchar(250) NOT NULL,
  `ciudad_pse` varchar(1000) DEFAULT NULL,
  `remitente_pse` varchar(1000) DEFAULT NULL,
  `cargoRemitente_pse` varchar(1000) DEFAULT NULL,
  `numeroOficio_pse` varchar(100) NOT NULL,
  `numeroJuicio_pse` varchar(1000) DEFAULT NULL,
  `tipoJuicio_pse` varchar(1000) DEFAULT NULL,
  `accion_pse` varchar(1000) DEFAULT NULL,
  `valor_pse` varchar(1000) DEFAULT NULL,
  `implicadosPerJur_pse` varchar(550) NOT NULL,
  `implicadosPerNat_pse` varchar(550) NOT NULL,
  `sinDeterImplicado_pse` varchar(550) NOT NULL,
  `documento_pse` varchar(1000) DEFAULT NULL,
  `numeroDocumento_pse` varchar(50) NOT NULL,
  `seg_ifi_pse` varchar(20) NOT NULL,
  `observacion_pse` text NOT NULL,
  `id_carga_providencia` int NOT NULL,
  `codigoCircular_pse` varchar(1000) DEFAULT NULL,
  `codigoAnexo_pse` varchar(1000) DEFAULT NULL,
  `archivo` varchar(250) DEFAULT '',
  PRIMARY KEY (`id_pse`),
  KEY `circular_pse_idx` (`circular_pse`(255)),
  KEY `numeroDocumento_pse_idx` (`numeroDocumento_pse`),
  KEY `numeroJuicio_pse_idx` (`numeroJuicio_pse`),
  KEY `idx_carga_providencia` (`id_carga_providencia`),
  KEY `implicadosPerJur_pse_idx` (`implicadosPerJur_pse`(255)),
  KEY `implicadosPerNat_pse_idx` (`implicadosPerNat_pse`(255)),
  KEY `sinDeterImplicado_pse_idx` (`sinDeterImplicado_pse`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dts_providencias_seps`
--

LOCK TABLES `dts_providencias_seps` WRITE;
/*!40000 ALTER TABLE `dts_providencias_seps` DISABLE KEYS */;
/*!40000 ALTER TABLE `dts_providencias_seps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dts_tipo_providencias`
--

DROP TABLE IF EXISTS `dts_tipo_providencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dts_tipo_providencias` (
  `id_tpv` int NOT NULL AUTO_INCREMENT,
  `tipo_tpv` char(50) NOT NULL,
  PRIMARY KEY (`id_tpv`),
  UNIQUE KEY `tipo_tpv` (`tipo_tpv`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dts_tipo_providencias`
--

LOCK TABLES `dts_tipo_providencias` WRITE;
/*!40000 ALTER TABLE `dts_tipo_providencias` DISABLE KEYS */;
INSERT INTO `dts_tipo_providencias` VALUES (2,'SBS'),(1,'SEPS');
/*!40000 ALTER TABLE `dts_tipo_providencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_group`
--

DROP TABLE IF EXISTS `files_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `files_group` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name_group` varchar(100) NOT NULL,
  `created_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_group` (`name_group`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_group`
--

LOCK TABLES `files_group` WRITE;
/*!40000 ALTER TABLE `files_group` DISABLE KEYS */;
INSERT INTO `files_group` VALUES (4,'Default','root','2024-01-31 01:00:14','2024-01-31 01:00:14');
/*!40000 ALTER TABLE `files_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institutions`
--

DROP TABLE IF EXISTS `institutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `institutions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1653 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institutions`
--

LOCK TABLES `institutions` WRITE;
/*!40000 ALTER TABLE `institutions` DISABLE KEYS */;
INSERT INTO `institutions` VALUES (1,'MANTELCOA','root','2024-01-31 01:00:14');
/*!40000 ALTER TABLE `institutions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint unsigned NOT NULL,
  `reserved_at` int unsigned DEFAULT NULL,
  `available_at` int unsigned NOT NULL,
  `created_at` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_reset_tokens_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1),(5,'2023_12_01_044155_create_profiles_table',1),(6,'2023_12_01_044216_create_menu_table',1),(7,'2024_02_23_091259_create_jobs_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profiles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu` json NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,'Admin','Administrador','[{\"name\": \"INICIO\", \"path\": \"/dashboard/default\", \"show\": true, \"selected\": true}, {\"name\": \"ADMINISTRADOR -> Perfil\", \"path\": \"/perfiles\", \"show\": true, \"selected\": true}, {\"name\": \"ADMINISTRADOR -> Crear Perfil\", \"path\": \"/perfiles/nuevo\", \"show\": false, \"selected\": true}, {\"name\": \"ADMINISTRADOR -> Usuarios\", \"path\": \"/usuarios\", \"show\": true, \"selected\": true}, {\"name\": \"ADMINISTRADOR -> Ingresos\", \"path\": \"/ingresos\", \"show\": true, \"selected\": true}, {\"name\": \"ADMINISTRADOR -> Crear Usuarios\", \"path\": \"/usuarios/nuevo\", \"show\": false, \"selected\": true}, {\"name\": \"ADMINISTRADOR -> Instituciones\", \"path\": \"/portal/nuevaInstitucion\", \"show\": true, \"selected\": true}, {\"name\": \"PORTAL\", \"path\": \"/portal\", \"show\": true, \"selected\": true}, {\"name\": \"PORTAL -> Reporte Descargas\", \"path\": \"/portal/reporteDescargas\", \"show\": true, \"selected\": true}, {\"name\": \"PORTAL -> Tipos Archivos\", \"path\": \"/portal/tiposArchivos\", \"show\": true, \"selected\": true}, {\"name\": \"PORTAL -> Nuevo Archivo\", \"path\": \"/portal/nuevoArchivo\", \"show\": true, \"selected\": true}, {\"name\": \"PORTAL -> Nuevo Grupo\", \"path\": \"/portal/nuevoGrupo\", \"show\": true, \"selected\": true}, {\"name\": \"PORTAL -> Usuarios\", \"path\": \"/portal/usuarios\", \"show\": true, \"selected\": true}, {\"name\": \"CARGA CONSULTAS\", \"path\": \"/consultas\", \"show\": true, \"selected\": true}, {\"name\": \"CARGA CONSULTAS -> Cargar Insumo SEPS\", \"path\": \"/consultas/cargarInsumoSEPS\", \"show\": true, \"selected\": true}, {\"name\": \"CARGA CONSULTAS -> Cargar Insumo SB\", \"path\": \"/consultas/cargarInsumoSB\", \"show\": true, \"selected\": true}, {\"name\": \"CARGA CONSULTAS -> Cargar Providencias SEPS\", \"path\": \"/consultas/cargarProvidenciaSEPS\", \"show\": true, \"selected\": true}, {\"name\": \"CARGA CONSULTAS -> Cargar Providencias SB\", \"path\": \"/consultas/cargarProvidenciaSB\", \"show\": true, \"selected\": true}, {\"name\": \"CARGA CONSULTAS -> Reporte de Providencias Judiciales Cargadas\", \"path\": \"/consultas/providenciasJudiciales\", \"show\": true, \"selected\": true}, {\"name\": \"DESCARGA\", \"path\": \"/descargas/default\", \"show\": true, \"selected\": true}, {\"name\": \"CONSULTA SB\", \"path\": \"/consultaSB\", \"show\": true, \"selected\": true}, {\"name\": \"CONSULTA SEPS\", \"path\": \"/consultaSEPS\", \"show\": true, \"selected\": true}, {\"name\": \"SESION\", \"path\": \"/sesion\", \"show\": true, \"selected\": true}, {\"name\": \"ADMINISTRADOR -> Ingresos\", \"path\": \"/ingresos\", \"show\": true, \"selected\": true}]','2023-12-17 15:46:07','2024-02-21 14:07:27');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sb_files`
--

DROP TABLE IF EXISTS `sb_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sb_files` (
  `nameFile` varchar(100) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sb_files`
--

LOCK TABLES `sb_files` WRITE;
/*!40000 ALTER TABLE `sb_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `sb_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seps_files`
--

DROP TABLE IF EXISTS `seps_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seps_files` (
  `circular` varchar(100) NOT NULL,
  `nameFile` varchar(100) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seps_files`
--

LOCK TABLES `seps_files` WRITE;
/*!40000 ALTER TABLE `seps_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `seps_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `documentNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `financialInstitution` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `portalDownload` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `consultingJudicialOrders` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `username_index` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ROOT','Administrador1','DataProj','17999999999','Activo','MANTELCOA','Si','Si','root@dataproj.com',NULL,'$2y$12$69QvIRbuN9EeTYr.lJ.fDei6Ehy.3JAk1/8LcflZJ2hLjTmttXHEO','1',NULL,'2023-12-17 15:46:07','2024-02-23 17:23:37');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_allowed_files`
--

DROP TABLE IF EXISTS `users_allowed_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_allowed_files` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `arc_id` int NOT NULL,
  `created_by` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `arc_id` (`arc_id`),
  CONSTRAINT `users_allowed_files_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `users_allowed_files_ibfk_2` FOREIGN KEY (`arc_id`) REFERENCES `dts_archivos` (`id_arc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_allowed_files`
--

LOCK TABLES `users_allowed_files` WRITE;
/*!40000 ALTER TABLE `users_allowed_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_allowed_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_allowed_groups`
--

DROP TABLE IF EXISTS `users_allowed_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_allowed_groups` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `created_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `users_allowed_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `files_group` (`id`),
  CONSTRAINT `users_allowed_groups_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_allowed_groups`
--

LOCK TABLES `users_allowed_groups` WRITE;
/*!40000 ALTER TABLE `users_allowed_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_allowed_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_downloads_logs`
--

DROP TABLE IF EXISTS `users_downloads_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_downloads_logs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `dta_id` int NOT NULL,
  `date_file` varchar(50) NOT NULL,
  `dta_detail` varchar(100) NOT NULL,
  `download_at` timestamp NOT NULL,
  `arc_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `dta_id` (`dta_id`),
  CONSTRAINT `users_downloads_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_downloads_logs`
--

LOCK TABLES `users_downloads_logs` WRITE;
/*!40000 ALTER TABLE `users_downloads_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_downloads_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_login_log`
--

DROP TABLE IF EXISTS `users_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_login_log` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `ip` varchar(250) NOT NULL,
  `user_agent` varchar(1000) NOT NULL,
  `login_at` datetime NOT NULL,
  `public_ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `users_login_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_login_log`
--

LOCK TABLES `users_login_log` WRITE;
/*!40000 ALTER TABLE `users_login_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_login_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-22  6:23:21
