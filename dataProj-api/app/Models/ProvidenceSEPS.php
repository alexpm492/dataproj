<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProvidenceSEPS extends Model
{
    use HasFactory;
    protected $table = 'dts_providencias_seps';
    public $timestamps = false;
    protected $fillable = [
        'circular_pse',
        'expediente_pse',
        'pagina_pse',
        'fechaCircular_pse',
        'fechaOficio_pse',
        'tiempoRespuesta_pse',
        'enteEmisor_pse',
        'ciudad_pse',
        'remitente_pse',
        'cargoRemitente_pse',
        'numeroOficio_pse',
        'numeroJuicio_pse',
        'tipoJuicio_pse',
        'accion_pse',
        'valor_pse',
        'implicadosPerJur_pse',
        'implicadosPerNat_pse',
        'sinDeterImplicado_pse',
        'documento_pse',
        'numeroDocumento_pse',
        'seg_ifi_pse',
        'observacion_pse',
        'id_carga_providencia',
        'codigoCircular_pse',
        'codigoAnexo_pse',
    ];
}
