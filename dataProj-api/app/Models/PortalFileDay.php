<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortalFileDay extends Model
{
    use HasFactory;
    protected $table = 'dts_dias_archivos';

    protected $primaryKey = 'id_dar';
    public $timestamps = false;
    protected $fillable = [
        'descripcion_dar',
        'estado_dar',
    ];
}
