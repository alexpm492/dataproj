<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UploadSupplie extends Model
{
    use HasFactory;
    protected $table = 'dts_carga_insumos';
    public $timestamps = false;
    protected $primaryKey = 'id_cin';
    protected $fillable = [
        'fecha_cin',
        'hora_cin',
        'nombre_cin',
        'fechaArchivo_cin',
        'tipoInsumo_cin',
        'id_usr',
        'id_tpv'
    ];

    public function typeProvidence(): BelongsTo
    {
        return $this->belongsTo(TypeProvidence::class, 'id_tpv', 'id_tpv');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usr', 'id');
    }
}
