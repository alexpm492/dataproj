<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserDownloadLog extends Model
{
    use HasFactory;

    protected $table = 'users_downloads_logs';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'dta_id',
        'dta_detail',
        'download_at'
    ];

    public function portalUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function portalFile(): BelongsTo
    {
        return $this->belongsTo(PortalFile::class, 'dta_id', 'id_dta');
    }
}
