<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SEPSFile extends Model
{
    use HasFactory;
    protected $table = 'seps_files';

    protected $primaryKey = 'circular';

    public $timestamps = false;
    protected $fillable = [
        'circular',
        'nameFile',
        'created_by',
        'created_at',
    ];

    public function providenceSeps(): BelongsTo
    {
        return $this->belongsTo(ProvidenceSEPS::class, 'codigoAnexo_pse', 'circular');
    }
}
