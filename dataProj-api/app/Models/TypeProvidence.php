<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeProvidence extends Model
{
    use HasFactory;

    protected $table = 'dts_tipo_providencias';
    public $timestamps = false;
    protected $fillable = [
        'id_tpv',
        'tipo_tpv'
    ];
}
