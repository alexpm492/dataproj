<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserFile extends Model
{
    use HasFactory;

    protected $table = 'users_allowed_files';

    protected $primaryKey = 'id';

    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'arc_id',
        'created_by',
        'created_at',
    ];

    public function portalUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function portalFile(): BelongsTo
    {
        return $this->belongsTo(PortalFile::class, 'id_arc', 'arc_id');
    }
}
