<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PortalFileDetail extends Model
{
    use HasFactory;

    protected $table = 'dts_detalle_archivos';

    protected $primaryKey = 'id_dta';

    public $timestamps = false;
    protected $fillable = [
        'id_dar',
        'id_arc',
        'id_usr_carga',
        'nombre_dta',
        'fecha_dta',
        'hora_dta',
        'descripcion_dta',
        'fechaArchivo_dta',

    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id', 'id_usr_carga');
    }

    public function day(): BelongsTo
    {
        return $this->belongsTo(PortalFileDay::class, 'id_dar', 'id_dar');
    }

    public function portalFile(): BelongsTo
    {
        return $this->belongsTo(PortalFile::class, 'id_arc', 'id_arc');
    }
}
