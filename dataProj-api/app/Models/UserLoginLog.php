<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserLoginLog extends Model
{
    use HasFactory;
    protected $table = 'users_login_log';

    protected $primaryKey = 'id';

    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'ip',
        'user_agent',
        'login_at',
        'public_ip'
    ];

    public function portalUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }
}
