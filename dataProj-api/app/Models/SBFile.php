<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SBFile extends Model
{
    use HasFactory;

    protected $table = 'sb_files';

    public $timestamps = false;
    protected $fillable = [
        'nameFile',
        'created_by',
        'created_at',
    ];
}
