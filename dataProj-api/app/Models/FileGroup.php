<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FileGroup extends Model
{
    use HasFactory;

    protected $table = 'files_group';

    protected $fillable = [
        'name_group',
        'created_by',
    ];

    public function user(): BelongsTo
    {
        return $this->BelongsTo(User::class, 'created_by', 'username');
    }
}
