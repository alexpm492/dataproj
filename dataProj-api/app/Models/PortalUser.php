<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PortalUser extends Model
{
    use HasFactory;


    protected $table = 'dts_usuario_portal';

    protected $primaryKey = 'id_upo';

    public $timestamps = false;
    protected $fillable = [
        'id_usr',
        'estado_upo',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id_usr', 'id');
    }
}
