<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProvidenceSB extends Model
{
    use HasFactory;
    protected $table = 'dts_providencias_sb';
    public $timestamps = false;
    protected $fillable = [
        'circular_psb',
        'fechaCircular_psb',
        'tiempoRespuesta_psb',
        'enteEmisor_psb',
        'ciudad_psb',
        'remitente_psb',
        'cargoRemitente_psb',
        'numeroOficio_psb',
        'numeroJuicio_psb',
        'tipoJuicio_psb',
        'accion_psb',
        'valor_psb',
        'implicadosPerJur_psb',
        'implicadosPerNat_psb',
        'sinDeterImplicado_psb',
        'documento_psb',
        'numeroDocumento_psb',
        'observacion_psb',
        'id_carga_providencia',
    ];
}
