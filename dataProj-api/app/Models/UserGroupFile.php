<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserGroupFile extends Model
{
    use HasFactory;

    protected $table = 'users_allowed_groups';

    protected $primaryKey = 'id';

    public $timestamps = false;
    protected $fillable = [
        'group_id',
        'user_id',
        'created_by',
        'created_at',
        'updated_at'
    ];

    public function portalUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function portalFileGroup(): BelongsTo
    {
        return $this->belongsTo(FileGroup::class, 'group_id', 'id');
    }
}
