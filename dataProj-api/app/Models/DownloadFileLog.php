<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DownloadFileLog extends Model
{
    use HasFactory;

    protected $table = 'users_downloads_logs';

    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'dta_id',
        'arc_id',
        'date_file',
        'dta_detail',
        'download_at',
    ];
}
