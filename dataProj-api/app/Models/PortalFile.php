<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PortalFile extends Model
{
    use HasFactory;
    protected $table = 'dts_archivos';

    protected $primaryKey = 'id_arc';
    public $timestamps = false;

    protected $fillable = [
        'descripcion_arc',
        'estado_arc',
        'fch_actualizacion_arc',
        'hor_actualizacion_arc',
        'group_id'
    ];

    public function detailFiles(): HasMany
    {
        return $this->hasMany(PortalFileDetail::class, 'id_arc', 'id_arc');
    }

    public function groupFile(): BelongsTo
    {
        return $this->belongsTo(FileGroup::class, 'group_id', 'id');
    }
}
