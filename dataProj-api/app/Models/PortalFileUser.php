<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortalFileUser extends Model
{
    use HasFactory;

    protected $table = 'dts_usuario_archivos';

    protected $primaryKey = 'id_uar';

    public $timestamps = false;
    protected $fillable = [
        'id_upo',
        'id_arc',
        'id_usr_carga',
        'nombre_dta',
        'fecha_dta',
        'hora_dta',
        'descripcion_dta',
        'fechaArchivo_dta',

    ];
}