<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use App\Models\UserGroupFile;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function users(Request $request)
    {
        $selectedRows = [
            'users.id',
            'username',
            'lastname',
            'name',
            'documentNumber',
            'financialInstitution',
            'status',
            'users.created_at',
            'profiles.description',

        ];
        $users = [];
        if (!$request->search) $users = User::select($selectedRows)->join('profiles', 'profiles.id', '=', 'profile_id')->get();
        else {
            $search = $request->search;
            $keywords = explode(' ', $search);

            $query = User::query();

            foreach ($keywords as $keyword) {
                $query->where(function ($query) use ($keyword) {
                    $query->where('username', 'like', '%' . $keyword . '%')
                        ->orWhere('name', 'like', '%' . $keyword . '%')
                        ->orWhere('lastname', 'like', '%' . $keyword . '%')
                        ->orWhere('financialInstitution', 'like', '%' . $keyword . '%')
                        ->orWhere('documentNumber', 'like', '%' . $keyword . '%');
                });
            }

            $users = $query->select($selectedRows)->join('profiles', 'profiles.id', '=', 'profile_id')->get();
        }
        return response()
            ->json([
                'data' => $users,
            ]);
    }

    public function get(Request $request)
    {
        $user = User::where('id', $request['id'])->firstOrFail();
        if ($user) {
            if (isset($user['profile_id'])) {
                $profile = Profile::where('id', $user['profile_id'])->firstOrFail();
                if ($profile) {
                    $user['profile'] = $profile;
                }
                $group = UserGroupFile::where('user_id', $request['id'])->first();
                if ($group) $user['group'] = $group->group_id;
            }
            return response()
                ->json($user);
        }
        return response()->json(['message' => 'Usuario no existe'], 500);
    }

    public function save(Request $request)
    {
        $user = $request->user();
        $userExist = User::where('username', strtoupper($request->username))->orWhere('email', $request->email)->first();
        if ($userExist) {
            throw new Exception('Los datos ingresados pertecen a otro usuario, verificalos e intenta nuevamente');
        }
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users|max:20',
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'documentNumber' => 'required|string|max:255',
            'status' => ['required', Rule::in(['Activo', 'Inactivo']),],
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'financialInstitution' => 'required',
            'portalDownload' => 'required',
            'consultingJudicialOrders' => 'required',
            'profile_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        DB::beginTransaction();
        $user = '';
        try {
            $user = User::create([
                'username' => strtoupper($request->username) ?? '',
                'name' => $request->name ?? '',
                'lastname' => $request->lastname ?? '',
                'documentNumber' => $request->documentNumber ?? '',
                'status' => $request->status ?? '',
                'email' => $request->email ?? '',
                'password' => Hash::make($request->password ?? ''),
                'financialInstitution' => $request->financialInstitution ?? '',
                'portalDownload' => $request->portalDownload ?? '',
                'consultingJudicialOrders' => $request->consultingJudicialOrders ?? '',
                'profile_id' => $request->profile_id ?? '',
            ]);
            $profile = Profile::where('id', $request->profile_id)->first();
            if ($profile && $profile->code !== 'Admin') {
                UserGroupFile::create([
                    'group_id' => $request->group,
                    'user_id' => $user->id,
                    'created_by' => $user->username,
                    'created_at' => now()
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()
                ->json(['message' => $e->getMessage()], 200);
        }
        return response()
            ->json(['data' => $user], 200);
    }

    public function update($id, Request $request)
    {
        $user_log = $request->user();
        $newData = $request->all();
        $user = User::where('id', $id)->first();
        if ($user) {
            $user->update($newData);
            $group = $request->group;
            if ($group == 'Todos') {
                $group = UserGroupFile::where('user_id', $id)->first();
                if ($group) $group->delete();
            } else {
                $group = UserGroupFile::where('user_id', $id)->first();
                if ($group) {

                    $group->update([
                        'group_id' => $request->group,
                        'updated_at' => now()
                    ]);
                } else {
                    UserGroupFile::create([
                        'group_id' => $request->group,
                        'user_id' => $id,
                        'created_by' => $user_log->username,
                        'created_at' => now()
                    ]);
                }
            }
            return $user;
        }
        return response()->json(['message' => 'Usuario no existe'], 500);
    }

    public function changeUserPassword($id, Request $request)
    {
        if ($request->isMethod('patch')) {

            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:8',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $userData = $request->all();
            $user = User::where('id', $id)->first();
            if ($user) {
                $user->password = $userData['password'];
                $user->save();
                return $user;
            }
            return response()->json(['message' => 'Usuario no existe'], 500);
        }
    }

    public function delete($id)
    {
        $user = User::where('id', $id)->first();
        if ($user) {
            User::destroy($id);
            return $id;
        }
        return response()->json(['message' => 'Usuario no existe'], 500);
    }

    public function updatePassword(Request $request)
    {
        $user = $request->user();
        $validator = Validator::make($request->all(), [
            'current_password' => 'required|string',
            'new_password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        if (!Hash::check($request->current_password, $user->password)) {
            throw ValidationException::withMessages([
                'message' => ['Clave actual invalida'],
            ]);
        }

        // If the current password is valid, update to the new password
        $user->password = Hash::make($request->new_password);
        $user->save();
        return response()->json(['message' => 'Actualización correcta'], 202);
    }
}
