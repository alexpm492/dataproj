<?php

namespace App\Http\Controllers;

use App\Models\FileGroup;
use App\Models\PortalFile;
use App\Models\PortalFileDetail;
use App\Models\UserGroupFile;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PortalGroupFileController extends Controller
{
    public function index()
    {
        $data = FileGroup::select(['id', 'name_group', 'created_by', 'created_at'])->get();
        return response()->json(['data' => $data], 200);
    }

    public function add(Request $request)
    {
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'name_group' => 'required|unique:App\Models\FileGroup,name_group|max:100',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        $now = date("Y-m-d H:i:s");
        FileGroup::create([
            'name_group' => $request->name_group,
            'created_by' => $user->username,
            'created_at' => $now
        ]);

        return response()->json(['message' => 'Grupo: ' . $request->name_group . ' registrado correctamente'], 202);
    }

    public function deleteGroup($id)
    {
        /*$defaultGroup = FileGroup::where('name_group', 'default')->first();
        if ($defaultGroup) {
            $group = FileGroup::where('id', $id)->first();
            if (!$group) throw new Exception("Error. No existe el grupo", 500);
            //delete allowed group
            DB::beginTransaction();
            try {
                DB::table('dts_archivos')
                    ->where('group_id',  intval($id))
                    ->update(['group_id' => $defaultGroup->id]);
                //PortalFile::where('group_id', $id)->update(['group_id', $defaultGroup->id]);
                DB::table('users_allowed_groups')
                    ->where('group_id',  intval($id))
                    ->delete();
                $group->delete();
                DB::commit();
                return response()->json(['message' => 'Grupo eliminado correctamente'], 202);
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }*/
        $list = PortalFile::where('group_id', intval($id))->get()->toArray();
        if (count($list) > 0) throw new Exception('Error. No se puede eliminar el grupo, esta asignado');

        $group = FileGroup::where('id', $id)->first();

        $group->delete();


        return response()->json(['message' => 'Grupo eliminado correctamente'], 202);
    }
}
