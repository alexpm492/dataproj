<?php

namespace App\Http\Controllers;

use App\Models\FileGroup;
use App\Models\Institutions;
use App\Models\PortalFile;
use App\Models\User;
use Mockery\Instantiator;

class FormDataController extends Controller
{
    public function getUsersForm()
    {
        $userList = User::selectRaw('id, CONCAT(lastname, " ", name) as label')->get()->toArray();
        // Return JSON response
        return response()->json($userList, 200);
    }

    public function getFilesForm()
    {
        $fileList = PortalFile::select(['id_arc as id', 'descripcion_arc as label'])->get()->toArray();
        // Return JSON response
        return response()->json($fileList, 200);
    }

    public function getGroupsForm()
    {
        $listGroups = FileGroup::select(['id', 'name_group'])->get();
        return response()->json($listGroups, 200);
    }

    public function getInstitutionsForm()
    {
        $list = Institutions::select(['id', 'name'])->orderBy('name', 'ASC')->get();
        return response()->json($list, 200);
    }
}
