<?php

namespace App\Http\Controllers;

use App\Models\DownloadFileLog;
use App\Models\PortalFile;
use App\Models\PortalFileDay;
use App\Models\PortalFileDetail;
use App\Models\User;
use Carbon\Carbon;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PortalFilesDetailsController extends Controller
{
    private const PORTAL_FILES_DIR = 'portalFiles';
    private const FILE_EXTENSIONS = [
        'txt'  => 'text/plain',
        'pdf'  => 'application/pdf',
        'zip'  => 'application/zip',
        'rar'  => 'application/x-rar-compressed',
        'ppt'  => 'application/vnd.ms-powerpoint',
        'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'doc'  => 'application/msword',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'xls'  => 'application/vnd.ms-excel',
        'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        '7z'   => 'application/x-7z-compressed',
        'tar'  => 'application/x-tar',
    ];
    public function get($id)
    {
        $days = PortalFileDay::where('estado_dar', 'A')->get();
        if (count($days) > 0) {
            $availableDays = [];
            foreach ($days as $day) {
                $availableDays[] = $day->id_dar;
            }

            $data = PortalFileDetail::where('id_arc', $id)
                ->whereIn('id_dar', $availableDays)->orderBy('fechaArchivo_dta', 'ASC')->get();
            return response()->json(['data' => $data]);
        }
    }

    public function store(Request $request)
    {
        $user_id = $request->user()->id;
        $validator = Validator::make($request->all(), [
            'id_arc' => 'required|integer',
            'descripcion_dta' => 'max:250',
            'fechaArchivo_dta' => 'required|date',
            'archivo' => 'required|mimes:txt,pdf,zip,rar,ppt,pptx,doc,docx,xls,xlsx,7zip,tar|max:512000'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        // Verify Days
        $date = Carbon::parse($request->fechaArchivo_dta);

        $dayName = strtoupper($date->format('l'));
        $days = [
            'MONDAY' => 'LUNES',
            'TUESDAY' => 'MARTES',
            'WEDNESDAY' => 'MIERCOLES',
            'THURSDAY' => 'JUEVES',
            'FRIDAY' => 'VIERNES',
            'SATURDAY' => 'SABADO',
            'SUNDAY' => 'DOMINGO',
        ];

        /* $portalFileDate = PortalFileDetail::where(['id_arc' => $request->id_arc, 'fechaArchivo_dta' => $request->fechaArchivo_dta])->first();
        if ($portalFileDate) throw new HttpException(500, 'El archivo ');*/

        $portalDay = PortalFileDay::where('descripcion_dar', $days[$dayName])->where('estado_dar', 'A')->first();
        if (!$portalDay) throw new HttpException(500, 'Error la fecha escogida para el archivo esta incorrecta');
        //Verify if PortalFile is enabled
        $portalFile = PortalFile::where('id_arc', $request->id_arc)->where('estado_arc', 'A')->first();
        if (!$portalFile) throw new HttpException(500, 'Error archivo no se encuentra habilitado en el portal');

        $user = User::where('id', $user_id)->where('status', 'Activo')->first();
        if (!$user) throw new HttpException(500, 'Error. No se ha encontrado el usuario');

        $detailFile = PortalFileDetail::where([
            'id_arc' => $request->id_arc,
            'id_dar' => $portalDay->id_dar,
            'fechaArchivo_dta' => $request->fechaArchivo_dta
        ])->first();
        $now = date("Y-m-d H:i:s");
        $now = explode(" ", $now);

        $file = $request->file('archivo');
        $portalFileDetail = null;
        if ($detailFile) {
            throw new HttpException(500, 'Error. Existe el archivo ya cargado: ' . $detailFile->nombre_dta . ' fecha: ' . $request->fechaArchivo_dta);
            /*if (Storage::exists(self::PORTAL_FILES_DIR . '/' . $detailFile->id_dta . '_' . $detailFile->nombre_dta)) Storage::delete(self::PORTAL_FILES_DIR . '/' . $detailFile->id_dta . '_' . $detailFile->nombre_dta);
            $detailFile->delete();
            $portalFileDetail =   PortalFileDetail::create([
                'id_arc' => $request->id_arc,
                'id_dar' => $portalDay->id_dar,
                'id_usr_carga' => $user_id,
                'nombre_dta' =>  $file->getClientOriginalName(),
                'fecha_dta' =>  $now[0],
                'hora_dta' => $now[1],
                'descripcion_dta' => $request->descripcion_dta ?? '',
                'fechaArchivo_dta' => $request->fechaArchivo_dta,
            ]);*/
        } else {
            $detailFile = PortalFileDetail::where([
                'id_arc' => $request->id_arc,
                'id_dar' => $portalDay->id_dar,

            ])->first();
            if ($detailFile) $detailFile->delete();

            $portalFileDetail =   PortalFileDetail::create([
                'id_arc' => $request->id_arc,
                'id_dar' => $portalDay->id_dar,
                'id_usr_carga' => $user_id,
                'nombre_dta' =>  $file->getClientOriginalName(),
                'fecha_dta' =>  $now[0],
                'hora_dta' => $now[1],
                'descripcion_dta' => $request->descripcion_dta ?? '',
                'fechaArchivo_dta' => $request->fechaArchivo_dta,
            ]);
        }


        $nameFile =  $portalFileDetail->getKey() . '_' . $file->getClientOriginalName();

        $file->storeAs(self::PORTAL_FILES_DIR, $nameFile);
        return response()
            ->json(['message' => 'Se ha subido el archivo: ' . $file->getClientOriginalName() . ' a: ' .  $portalFile->descripcion_arc], 202);
    }

    public function delete($id)
    {
        $portalFileDetail = PortalFileDetail::where('id_dta', $id)->first();
        if (!$portalFileDetail) throw new ModelNotFoundException('Error. No se encontrado el archivo', 404);
        // Remove row
        $portalFileDetail->delete();

        if (Storage::exists(self::PORTAL_FILES_DIR . '/' . $portalFileDetail->nombre_dta)) Storage::delete(self::PORTAL_FILES_DIR . '/' . $portalFileDetail->nombre_dta);

        return response()->json(['message' => 'Se ha eliminado archivo correctamente'], 200);
    }

    public function download($file_id, Request $request)
    {
        $portalFileDetail = PortalFileDetail::where('id_dta', $file_id)->first();
        if (!$portalFileDetail) throw new ModelNotFoundException('Error. No se encontrado el archivo', 404);


        $filePath = self::PORTAL_FILES_DIR . '/' . $file_id . '_' . $portalFileDetail->nombre_dta;
        if (!Storage::disk('local')->exists($filePath)) throw new ModelNotFoundException('Error. No se encontrado el archivo', 404);
        $user_id = $request->user()->id;
        $extension = pathinfo($portalFileDetail->nombre_dta, PATHINFO_EXTENSION);
        $contentType = array_key_exists($extension, self::FILE_EXTENSIONS) ? self::FILE_EXTENSIONS[$extension] :  'application/octet-stream';
        // Create log 
        DownloadFileLog::create([
            'user_id' => $user_id,
            'dta_id' =>  $portalFileDetail->id_dta,
            'arc_id' =>  $portalFileDetail->id_arc,
            'date_file' => $portalFileDetail->fechaArchivo_dta,
            'dta_detail' => $portalFileDetail->nombre_dta,
            'download_at' => date("Y-m-d H:i:s"),
        ]);
        //Download file
        return Storage::download($filePath, $portalFileDetail->nombre_dta, ['Content-Type: ' . $contentType]);
    }
}
