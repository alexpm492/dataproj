<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UserLoginLog;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('username', 'password'))) {
            return response()
                ->json(['message' => 'Unauthorized'], '401');
        }

        $user = User::where('username', $request['username'])->where('status', 'Activo')->firstOrFail();
        if (isset($user['profile_id'])) {
            $profile = Profile::where('id', $user['profile_id'])->firstOrFail();
            if ($profile) {
                $profile->menu = json_decode($profile->menu, true);
                $user['profile'] = $profile;
            }
        }
        // delete another token if exists another session
        $user->tokens()->where('tokenable_id', $user->id)->delete();
        // create log 

        UserLoginLog::create([
            'user_id' => $user->id,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'user_agent' => $request->header('User-Agent'),
            'login_at' => date("Y-m-d H:i:s"),
            'public_ip' => $request->publicIp ?? ''
        ]);
        $token = $user->createToken($user['id'])->plainTextToken;


        return response()
            ->json([
                'message' => 'Hi ' . $user->name,
                'accessToken' => $token,
                'token_type' => 'Bearer',
                'user' => $user
            ]);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response()
            ->json(['message' => 'successfully logout']);
    }
}
