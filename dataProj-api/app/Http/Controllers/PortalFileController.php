<?php

namespace App\Http\Controllers;

use App\Models\FileGroup;
use App\Models\PortalFile;
use App\Models\PortalFileDetail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PortalFileController extends Controller
{


    public function index(Request $request)
    {
        $user = $request->user();
        $pagination = config('constants.pagination');

        $selectedRows = [
            'dts_archivos.descripcion_arc',
            'files_group.name_group',
            'dts_archivos.estado_arc',
        ];
        $validator = Validator::make($request->all(), $pagination +
            ['orderBy' => Rule::in(array_map(function ($row) {
                return explode('.', $row)[1];
            }, $selectedRows) + ['last_update'])]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }



        //$query = PortalFile::with(['detailFiles']);

        $query = PortalFile::select($selectedRows)
            ->join('files_group', 'dts_archivos.group_id', '=', 'files_group.id')
            ->addSelect(DB::raw("(SELECT COALESCE(dar.nombre_dta, '') FROM dts_detalle_archivos dar WHERE dts_archivos.id_arc = dar.id_arc ORDER BY id_dta DESC LIMIT 1) AS last_update"))
            ->addSelect('dts_archivos.id_arc')
            ->orderBy($request->orderBy ?? 'dts_archivos.descripcion_arc', !$request->isAsc ? 'ASC' : 'DESC');


        if ($user->username !== 'ROOT') {
            $query->join('users_allowed_groups', 'dts_archivos.group_id', '=', 'users_allowed_groups.group_id')
                ->where('users_allowed_groups.user_id', $user->id);
        }

        if ($request->search != null) {
            $query->where(function ($query) use ($request) {
                $query->orWhere('dts_archivos.descripcion_arc', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('files_group.name_group', 'LIKE', '%' . $request->search . '%');
            });
        }


        $data = $query->paginate($request->pageLimit);

        $nextPageUrl = $data->nextPageUrl();

        return response()->json([
            'data' => $data->items(),
            'currentPage' => $data->currentPage(),
            'perPage' => $data->perPage(),
            'total' => $data->total(),
            'nextPage' => $nextPageUrl ? intval(explode('=', $nextPageUrl)[1]) : null,
            'orderBy' => $request->orderBy ?? 'descripcion_arc'
        ], 200);
    }

    public function get($id)
    {
        $portalFile = PortalFile::where('id_arc', $id)->firstOrFail();
        if ($portalFile) {
            return response()->json(['data' => $portalFile]);
        }
        throw new ModelNotFoundException('Error. No se ha encontrado el archivo', 404);
    }

    public function getDetails($id)
    {
        $detalleArchivos = PortalFileDetail::select('dts_detalle_archivos.id_dta', 'dts_dias_archivos.descripcion_dar', 'dts_detalle_archivos.fechaArchivo_dta', 'dts_detalle_archivos.nombre_dta', 'dts_detalle_archivos.descripcion_dta', 'dts_detalle_archivos.fecha_dta', 'dts_detalle_archivos.hora_dta')
            ->join('dts_dias_archivos', 'dts_dias_archivos.id_dar', '=', 'dts_detalle_archivos.id_dar')
            ->where('id_arc', $id)
            ->orderBy('fechaArchivo_dta', 'ASC')
            ->get();



        return response()->json(['data' => $detalleArchivos]);
    }

    public function save(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'descripcion' => 'required|unique:App\Models\PortalFile,descripcion_arc|max:100',
            'group_id' => 'required|integer',
            //'estado_arc' => ['required', Rule::in(['A', 'X']),],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        $now = date("Y-m-d H:i:s");
        $now = explode(" ", $now);
        PortalFile::create([
            'descripcion_arc' => $request->descripcion ?? '',
            'estado_arc' => $request->estado ?? 'A',
            'group_id' => $request->group_id,
            'fch_actualizacion_arc' =>  $now[0],
            'hor_actualizacion_arc' => $now[1]
        ]);

        return response()->json(['message' => 'Archivo: ' . $request->descripcion_arc . ' registrado correctamente'], 202);
    }


    public function update($id, Request $request)
    {
        $newData = $request->all();
        $portalFile = PortalFile::where('id_arc', $id)->first();
        if (!$portalFile) throw new ModelNotFoundException('Error. Archivo no encontrado', 404);

        $portalFile->update($newData);

        return response()->json(['message' => 'Datos de archivo actualizado'], 202);
    }

    /* public function addGroup(Request $request)
    {

        $username = $request->user()->username;
        $validator = Validator::make($request->all(), [
            'name_group' => 'required|unique:App\Models\FileGroup,name_group|max:100',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        FileGroup::create([
            'name_group' => $request->name_group,
            'created_by' =>  $username,
            //'uptated_at' => null

        ]);

        return response()->json(['message' => 'Grupo creado correctamente'], 202);
    }*/
}
