<?php

namespace App\Http\Controllers;

use App\Jobs\UploadFilesSEPS;
use App\Models\ProvidenceSEPS;
use App\Models\SEPSFile;
use App\Models\UploadSupplie;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProvidenceSEPSController extends Controller
{
    private const SEPS_DIR = 'sepsPdf';

    public function index(Request $request)
    {
        $code = $request->user()->profile->code;
        $selectedRows1 = $code === 'Admin' ? [
            'codigoAnexo_pse'
        ] : [];
        $selectedRows = [
            'documento_pse',
            'numeroDocumento_pse',
            'implicados_pse',
            'fechaArchivo_cin',
            'ciudad_pse',
            'enteEmisor_pse',
            'numeroOficio_pse',
            'circular_pse',
            'expediente_pse',
            'pagina_pse',
            'tipoJuicio_pse',
            'numeroJuicio_pse',
            'accion_pse',
            'valor_pse',
        ];

        $selectedRows = array_merge($selectedRows1, $selectedRows);

        $headers1 = $code === 'Admin' ? [
            "Num. Providencia"
        ] : [];
        $headers = [
            "Tipo Documento",
            "Num. Documento",
            'Implicado',
            "Fecha",
            "Ciudad",
            "Emisor",
            "Num. Oficio",
            "Circular",
            "Expediente",
            "Página",
            "Tipo Juicio",
            "Num. Juicio",
            "Acción",
            "Valor",
            "Descargar"
        ];

        $headers = array_merge($headers1, $headers);
        //$user = $request->user()->username;
        $validator = Validator::make($request->all(), [
            'nombre' => 'required_without_all:numeroDocumento,numeroJuicio',
            'numeroDocumento' => 'required_without_all:nombre,numeroJuicio',
            'numeroJuicio' => 'required_without_all:nombre,numeroDocumento',
        ] + ['orderBy' =>  Rule::in(array_map(function ($item) {
            $parts = explode(' as ', $item);
            return $parts[0];
        }, $selectedRows)), 'isAsc' => 'required|boolean']);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        $filters = [];
        if ($request->nombre != null) {
            $filters[] = ['implicadosPerNat_pse', 'LIKE', '%' . $request->nombre . '%'];
        }
        if ($request->numeroDocumento != null) {
            $filters[] = ['numeroDocumento_pse', '=', $request->numeroDocumento];
        }
        if ($request->numeroJuicio != null) {
            $filters[] = ['numeroJuicio_pse', '=', $request->numeroJuicio];
        }

        // filter raws

        $newRows = [];
        foreach ($selectedRows as $value) {
            if ($value === 'implicados_pse') $newRows[] =  DB::raw('CONCAT(implicadosPerJur_pse, " ", implicadosPerNat_pse, " ", sinDeterImplicado_pse) AS implicados_pse');
            else $newRows[] = $value;
        }

        $order = $request->orderBy;
        if ($order === null) {
            if ($code === 'Admin') $order = 'fechaArchivo_cin';
            else $order = 'fechaArchivo_cin';
        } else if ($order === 'implicados_pse') $order = "implicados_pse " . ($request->isAsc ? 'ASC' : 'DESC');
        $newRows[] = DB::raw('COALESCE((SELECT nameFile FROM seps_files sp WHERE sp.circular = dts_providencias_seps.codigoAnexo_pse LIMIT 1), "") as file');
        $query = ProvidenceSEPS::select($newRows)
            ->where(function ($query) use ($filters, $request) {
                foreach ($filters as $filter) {
                    $query->where($filter[0], $filter[1], $filter[2]);
                }
                // Additional condition for 'implicadosPerJur_pse'
                if ($request->nombre != null) {
                    $query->orWhere('implicadosPerJur_pse', 'LIKE', '%' . $request->nombre . '%');
                    $query->orWhere('sinDeterImplicado_pse', 'LIKE', '%' . $request->nombre . '%');
                }
            })->join('dts_carga_insumos', 'dts_carga_insumos.id_cin', '=', 'id_carga_providencia');

        if ($request->orderBy === 'implicados_pse') {
            $query->orderByRaw($order);
        } else $query->orderBy($order, $request->isAsc ? 'ASC' : 'DESC');
        $data = $query->get();

        //->orderBy()
        return response()->json(['data' => $data, 'headers' => $headers, 'orderBy' => $request->orderBy !== null && strpos($request->orderBy, 'implicados_pse') !== false ? 'implicados_pse' : $order], 200);
    }


    public function save(Request $request)
    {
        $user = $request->user();
        $validator = Validator::make($request->all(), [
            'archivo' => 'required|mimes:txt|max:102400',
            'nombreArchivo' => 'required',
            'id_tpv' => 'required|integer'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        $fileUploadExist = UploadSupplie::where('nombre_cin', $request->nombreArchivo . '.txt')->first();
        if ($fileUploadExist) throw new Exception('Error. El archivo ' . $request->nombreArchivo . '.txt' . ' ya ha sido cargado al sistema');
        //validar fecha
        $fechaArchivo = explode(' ', $request->nombreArchivo)[0];

        // Create a DateTime object using the specified format
        if (!DateTime::createFromFormat('Y-m-d', $fechaArchivo) || DateTime::createFromFormat('Y-m-d', $fechaArchivo)->format('Y-m-d') != $fechaArchivo) throw new Exception("Error. Fecha de archivo invalida");

        // Read the contents of the file
        $contenidoArchivo = file_get_contents($request->file('archivo')->path());

        // Detect the encoding of the text
        $encoding = mb_detect_encoding($contenidoArchivo, 'UTF-8, ISO-8859-1, Windows-1252', true);

        // Convert the text to UTF-8
        $utf8Contenido = mb_convert_encoding($contenidoArchivo, 'UTF-8', $encoding);

        // Split the content into an array of lines
        $lines = explode(PHP_EOL, $utf8Contenido);
        $dataProvidences = [];
        for ($i = 2; $i < count($lines); $i++) {
            if (trim($lines[$i]) != "") {
                $temp = explode("\t", $lines[$i]);
                //$temp = array_slice($temp, 0, -6);
                $temp[16] = implode(' ', array_slice($temp, 16, 4));

                for ($j = 17; $j < 20; $j++) {
                    unset($temp[$j]);
                }
                $temp = array_values($temp);
                $temp = count($temp) > 22 ? array_splice($temp, 0, 22) : $temp;
                //id_carga_providencia
                array_push($temp, 0);
                //codigo_circular_pse
                $circularCode = explode("SEPS-SGD-SGE-", $temp[0])[1];
                array_push($temp, $circularCode);
                //codigoAnexo_pse
                array_push($temp, $circularCode . '-' . $temp[1]);
                //Add data
                $dataProvidences[] = array_map('trim', str_replace('"', ' ', $temp));
            }
        }

        $providenceFields =
            [
                'circular_pse',
                'expediente_pse',
                'pagina_pse',
                'fechaCircular_pse',
                'fechaOficio_pse',
                'tiempoRespuesta_pse',
                'enteEmisor_pse',
                'ciudad_pse',
                'remitente_pse',
                'cargoRemitente_pse',
                'numeroOficio_pse',
                'numeroJuicio_pse',
                'tipoJuicio_pse',
                'accion_pse',
                'valor_pse',
                'implicadosPerJur_pse',
                'implicadosPerNat_pse',
                'sinDeterImplicado_pse',
                'documento_pse',
                'numeroDocumento_pse',
                'seg_ifi_pse',
                'observacion_pse',
                'id_carga_providencia',
                'codigoCircular_pse',
                'codigoAnexo_pse',

            ];
        if (count($providenceFields) != count($dataProvidences[0])) throw new Exception('Error. Datos no coinciden con campos tabla');
        $newRowsProvidences = [];
        foreach ($dataProvidences as $dataRow) {
            $row = [];

            foreach ($dataRow as $j => $value) {
                $row[$providenceFields[$j]] = $value;
            }

            $newRowsProvidences[] = $row;
        }

        $now = date("Y-m-d H:i:s");
        $now = explode(" ", $now);
        $newSupply = [
            'fecha_cin' => $now[0],
            'hora_cin'  => $now[1],
            'nombre_cin' => $request->nombreArchivo . '.txt',
            'fechaArchivo_cin' => $fechaArchivo,
            'tipoInsumo_cin' => explode(" ", $request->nombreArchivo)[1],
            'id_usr' => $user->id,
            'id_tpv' => $request->id_tpv
        ];
        // Make as collection to chuck data

        // Init transaccion
        DB::beginTransaction();
        try {
            // insert in supplies
            $insertedId = DB::table('dts_carga_insumos')->insertGetId($newSupply);
            foreach ($newRowsProvidences as &$dataRow) {
                $dataRow['id_carga_providencia'] = $insertedId;
            }
            unset($dataRow);
            $newRowsProvidences = collect($newRowsProvidences);
            $chuncks = $newRowsProvidences->chunk(500);
            foreach ($chuncks as $key => $value) {
                DB::table('dts_providencias_seps')->insert($value->toArray());
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['message' => "Proceso SEPS correcto, se han registrado " . count($dataProvidences) . " registros."], 202);
        //return response()->json($newRowsProvidences, 202);
    }


    public function controlById(Request $request)
    {

        $supply = UploadSupplie::where('id_cin', $request['id'])->first();
        if ($supply) {
            $tableHeaders = [
                'circular_pse' => ["Circular"],
                'expediente_pse' => ["Expediente"],
                'pagina_pse' => ["Página"],
                'fechaCircular_pse' => ["Fecha"],
                'fechaOficio_pse' => ["Fecha Oficio"],
                'tiempoRespuesta_pse' => ["Tiempo Respuesta"],
                'enteEmisor_pse' => ["Emisor"],
                'ciudad_pse' => ["Ciudad"],
                'remitente_pse' => ["Remitente"],
                'cargoRemitente_pse' => ["Cargo Remitente"],
                'numeroOficio_pse' => ["Num. Oficio"],
                'numeroJuicio_pse' => ["Num. Juicio"],
                'tipoJuicio_pse' => ["Tipo Juicio"],
                'accion_pse' => ["Acción"],
                'valor_pse' => ["Valor"],
                'documento_pse' => ["Tipo Documento"],
                'numeroDocumento_pse' => ["Num. Documento"],
                'observacion_pse' => ["Observacion"],
                'codigoAnexo_pse' => ["Num. Providencia"],
                'implicadosPerJur_pse' => ["Implicado Persona Juridica"],
                'implicadosPerNat_pse' => ["Implicado Persona Natural"],
                'sinDeterImplicado_pse' => ["Implicado Sin Determinación"],
            ];

            $last10Rows = ProvidenceSEPS::select(array_keys($tableHeaders))->where('id_carga_providencia', $request['id'])->limit(10)->get();

            return response()->json(['data' => $last10Rows], 200);
        } else {
            throw new ModelNotFoundException('Error. No existe el archivo solicitado', 404);
        }
    }

    public function deleteUpload(Request $request)
    {
        $supply = UploadSupplie::where('id_cin', $request['id'])->first();
        if ($supply) {

            try {
                $supply->delete();
                ProvidenceSEPS::where('id_carga_providencia', $request['id'])->delete();
            } catch (Exception $e) {
                return response()->json(['message' => $e->getMessage()], 500);
            }

            return response()->json(['message' => 'Correcto'], 202);
        } else {
            throw new ModelNotFoundException('Error. No existe el archivo solicitado', 404);
        }
    }

    public function store($code, Request $request)
    {
        set_time_limit(60 * 5);
        $validator = Validator::make($request->all(), [
            'files.*' => 'required|mimes:pdf|max:512000',
        ]);


        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        $files = $request->file('files');

        if (!Storage::exists(self::SEPS_DIR)) Storage::makeDirectory(self::SEPS_DIR);
        $newFilesUploaded = 0;
        $username = $request->user()->username;
        foreach ($files  as $file) {
            $originalName = $file->getClientOriginalName();
            $path = self::SEPS_DIR . "/" . $code . "-" .  $originalName;
            if (!Storage::exists($path)) {
                $file->storeAs(self::SEPS_DIR, $code . "-" .  $originalName);
                $newFilesUploaded++;
                SEPSFile::create([
                    'circular' => pathinfo($code . "-" .  $originalName, PATHINFO_FILENAME),
                    'nameFile' => $code . "-" .  $originalName,
                    'created_by' => $username,
                    'created_at' => date("Y-m-d H:i:s"),
                ]);
            }
        }


        return response()->json(['message' => $newFilesUploaded > 0 ? ('Se ha subido: #' .  $newFilesUploaded . ' archivos codigo: ' . $code . ' se ha subido correctamente') : 'No se han subido nuevos archivos'], 202);
    }


    /* public function store($code, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'files.*' => 'required|mimes:pdf|max:512000',
        ]);


        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        // $files = $request->file('files');

        if (!Storage::exists(self::SEPS_DIR)) Storage::makeDirectory(self::SEPS_DIR);

        $username = $request->user()->username;

        $filesDetails = [];
        foreach ($request->file('files') as $file) {
            $filesDetails[] = [
                'contents' => base64_encode(file_get_contents($file->getRealPath())),
                'originalName' => $file->getClientOriginalName(),
                'path' => self::SEPS_DIR . "/" . $code . "-" .  $file->getClientOriginalName()
            ];
        }

        $username = $request->user()->username;


        UploadFilesSEPS::dispatch($filesDetails, $username);


        return response()->json(['message' => 'Archivos SEPS registrados correctamente'], 202);
    }*/

    public function getSepsPdf($code)
    {

        // Construct the full path to the file in the subdirectory
        $filePath = self::SEPS_DIR . '/' . $code . '.pdf';
        if (Storage::disk('local')->exists($filePath)) {
            return Storage::download($filePath, $code . '.pdf', ['Content-Type: application/pdf']);
        } else {
            // File does not exist, throw a 404 exception
            throw new Exception('Error. Documento seps no encontrado', 404);
        }
    }
}
