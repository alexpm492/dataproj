<?php

namespace App\Http\Controllers;

use App\Models\UserLoginLog;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserLoginLogController extends Controller
{
    public function index(Request $request)
    {
        $selectedRows = [
            'users.username',
            'users.lastname',
            'users.name',
            'users.financialInstitution',
            'users.status',
            'users_login_log.ip',
            'users_login_log.public_ip',
            'users_login_log.user_agent',
            'users_login_log.login_at',

        ];

        $pagination = config('constants.pagination');
        $validator = Validator::make($request->all(), [
            'dateBegin' => 'required|date',
            'dateEnd' => 'required|date',
            'user_id' => 'numeric',
            'orderBy' => Rule::in(array_map(function ($row) {
                return explode('.', $row)[1];
            }, $selectedRows))
        ] + $pagination);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        $filters = [];
        if ($request->user_id != null) $filters[] = ['users_login_log.user_id', '=', $request->user_id];

        $query = UserLoginLog::select($selectedRows)
            ->whereBetween('users_login_log.login_at', [$request->dateBegin . ' 00:00:00', $request->dateEnd . ' 23:59:59'])
            ->orderBy($request->orderBy ?? 'users_login_log.login_at', $request->isAsc ? 'ASC' : 'DESC')
            ->where($filters)
            ->join('users', 'users.id', '=', 'users_login_log.user_id');


        $data = $query->paginate($request->pageLimit);
        $nextPageUrl = $data->nextPageUrl();

        return response()
            ->json([
                'data' => $data->items(),
                'currentPage' => $data->currentPage(),
                'perPage' => $data->perPage(),
                'total' => $data->total(),
                'nextPage' => $nextPageUrl ? intval(explode('=', $nextPageUrl)[1]) : null,
                'orderBy' => $request->orderBy ?? 'login_at'
            ], 200);
    }
}
