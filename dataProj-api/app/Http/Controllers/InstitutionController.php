<?php

namespace App\Http\Controllers;

use App\Models\Institutions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InstitutionController extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        if (!$request->search)  $data = Institutions::all();
        else {
            $search = $request->search;
            $data = Institutions::where('name', 'like', '%' . $search . '%')->get();
        }
        return response()
            ->json([
                'data' => $data,
            ]);
    }
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required||unique:App\Models\Institutions,name|max:100'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        $now = date("Y-m-d H:i:s");
        $user = $request->user();
        Institutions::create([
            'name' =>  strtoupper($request->name),
            'created_by' => $user->username,
            'created_at' => $now
        ]);

        return response()->json(['message' => 'Institucion: ' . $request->name . ' registrado correctamente'], 202);
    }
}
