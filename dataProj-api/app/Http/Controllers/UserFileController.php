<?php

namespace App\Http\Controllers;

use App\Models\PortalFile;
use App\Models\User;
use App\Models\UserFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserFileController extends Controller
{
    //Allow access to download a selected files
    public function save($user_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'arc_ids' => 'required|array',
            'arc_ids.*' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        // Get ids
        $userAllowedFiles = UserFile::where('user_id', $user_id)->pluck('arc_id')->toArray();
        $uniqueNumbers = array_diff($request->arc_ids, $userAllowedFiles);
        $userName = User::where('id', $request->user()->id)->first();
        $newRows = [];
        foreach ($uniqueNumbers as $arc_id) {
            $newRows[] = [
                'user_id' => $user_id,
                'arc_id' => $arc_id,
                'created_by' => $userName->username,
                'created_at' => date("Y-m-d H:i:s")
            ];
        }

        UserFile::insert($newRows);

        return response()
            ->json(['message' => 'Se agregado permisos correctamente'], 202);
    }

    // delete access to download selected files
    public function delete($user_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'allowed_ids' => 'required|array',
            'allowed_ids.*' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        UserFile::where('user_id', $user_id)->whereIn('arc_id', $request->allowed_ids)->delete();

        return response()
            ->json(['message' => 'Se ha eliminado los permisos correctamente'], 202);
    }

    // list access to download files 
    public function get($user_id)
    {
        $userFiles = UserFile::where('user_id', $user_id)->pluck('arc_id')->toArray();

        $userAllowed = PortalFile::select(['id_arc', 'descripcion_arc', 'estado_arc'])->whereIn('id_arc', $userFiles)->where('estado_arc', 'A')->get()->toArray();
        $userNotAllowed = PortalFile::select(['id_arc', 'descripcion_arc', 'estado_arc'])->whereNotIn('id_arc', $userFiles)->where('estado_arc', 'A')->get('estado_arc')->toArray();
        $newRows = [];
        foreach ($userAllowed as $row) {
            $newRows[] = [
                'id' => $row['id_arc'],
                'description' => $row['descripcion_arc'],
                'allowed' => true,
                'status_file' => $row['estado_arc'] == 'A' ? true : false
            ];
        }
        foreach ($userNotAllowed as $row) {
            $newRows[] = [
                'id' => $row['id_arc'],
                'description' => $row['descripcion_arc'],
                'allowed' => false,
                'status_file' => $row['estado_arc'] == 'A' ? true : false
            ];
        }

        $user = User::select(['name', 'lastname', 'financialInstitution'])->where('id',  $user_id)->first();

        usort($newRows, function ($a, $b) {
            return strcmp($a['description'], $b['description']);
        });
        return response()
            ->json(['data' => $newRows, 'user' => $user], 200);
    }

    //list of users with access to download files
    public function getUsersWithDownloads(Request $request)
    {
        $selectedRows = ['id', 'lastname', 'name', 'financialInstitution', 'status'];
        $pagination = config('constants.pagination');
        $validator = Validator::make($request->all(), [
            'orderBy' =>  Rule::in($selectedRows),
        ] + $pagination);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        $search = $request->search;

        $query = User::select($selectedRows)->where('portalDownload', 'Si')->orderBy($request->orderBy ??  'lastname', $request->isAsc ? 'ASC' : 'DESC');
        if ($search) {
            $query->where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                ->orWhere('financialInstitution', 'LIKE', '%' . $search . '%');
        }

        $data = $query->paginate($request->pageLimit);
        $nextPageUrl = $data->nextPageUrl();

        return response()
            ->json([
                'data' => $data->items(),
                'currentPage' => $data->currentPage(),
                'perPage' => $data->perPage(),
                'total' => $data->total(),
                'nextPage' => $nextPageUrl ? intval(explode('=', $nextPageUrl)[1]) : null,
                'orderBy' => $request->orderBy ?? 'lastname'
            ], 200);
    }

    //list of allowed files with files to download
    public function downloadList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        $user_id = $request->user()->id;
        $files = UserFile::select([
            'users_allowed_files.arc_id',
            'dts_archivos.descripcion_arc',
            'dts_detalle_archivos.id_dta',
            'dts_detalle_archivos.nombre_dta',
            'dts_detalle_archivos.descripcion_dta',
            'dts_detalle_archivos.fecha_dta',
            'dts_detalle_archivos.hora_dta'
        ])->where('users_allowed_files.user_id', $user_id)
            ->where('dts_detalle_archivos.fechaArchivo_dta', $request->date)
            ->join('dts_archivos', 'users_allowed_files.arc_id', '=', 'dts_archivos.id_arc')
            ->leftjoin('dts_detalle_archivos', 'users_allowed_files.arc_id', '=', 'dts_detalle_archivos.id_arc')
            ->get()->toArray();

        /*$groupedData = [];
        foreach ($files as $item) {
            $arcId = $item['arc_id'];
            unset($item['arc_id']);
            $groupedData[$arcId][] = $item;
        }*/

        $groupedData = [];
        foreach ($files as $item) {
            if (!isset($groupedData[$item['descripcion_arc']])) {
                $groupedData[$item['descripcion_arc']] = ['name' => $item['descripcion_arc'], 'files' => []];
            }

            if ($item['nombre_dta'] !== null) {
                $groupedData[$item['descripcion_arc']]['files'][] = [
                    'id_dta' => $item['id_dta'],
                    'nombre_dta' => $item['nombre_dta'],
                    'descripcion_dta' => $item['descripcion_dta'],
                    'fecha_dta' => $item['fecha_dta'],
                    'hora_dta' => $item['hora_dta']
                ];
            }
        }

        // Convert associative array to indexed array
        $groupedData = array_values($groupedData);
        return response()
            ->json([
                'data' =>
                $groupedData,

            ], 200);
    }
}
