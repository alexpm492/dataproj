<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessFilesJob;
use App\Jobs\UploadFilesSB;
use App\Models\Job;
use App\Models\PortalFile;
use App\Models\ProvidenceSB;
use App\Models\SBFile;
use App\Models\SEPSFile;
use App\Models\UploadSupplie;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Illuminate\Validation\Rule;

class ProvidenceSBController extends Controller
{
    private const SB_DIR = 'SB_DOCS';
    private const SB_UPLOADS = 'SB_UPLOADS';
    private const FILE_EXTENSIONS = [
        'txt'  => 'text/plain',
        'pdf'  => 'application/pdf',
        'zip'  => 'application/zip',
        'rar'  => 'application/x-rar-compressed',
        'ppt'  => 'application/vnd.ms-powerpoint',
        'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'doc'  => 'application/msword',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'xls'  => 'application/vnd.ms-excel',
        'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        '7z'   => 'application/x-7z-compressed',
        'tar'  => 'application/x-tar',
    ];
    public function index(Request $request)
    {
        $code = $request->user()->profile->code;
        $selectedRows1 = $code === 'Admin' ? [
            'circular_psb as circular'
        ] : [];


        $selectedRows = [
            'documento_psb',
            'numeroDocumento_psb',
            'implicados_psb',
            'fechaArchivo_cin',
            'ciudad_psb',
            'enteEmisor_psb',
            'numeroOficio_psb',
            'tipoJuicio_psb',
            'numeroJuicio_psb',
            'circular_psb',
            'accion_psb',
            'valor_psb',
        ];
        $selectedRows = array_merge($selectedRows1, $selectedRows);
        $validator = Validator::make($request->all(), [
            'nombre' => 'required_without_all:numeroDocumento,numeroJuicio',
            'numeroDocumento' => 'required_without_all:nombre,numeroJuicio',
            'numeroJuicio' => 'required_without_all:nombre,numeroDocumento',
        ] + ['orderBy' =>  Rule::in(array_map(function ($item) {
            $parts = explode(' as ', $item);
            return $parts[0];
        }, $selectedRows)), 'isAsc' => 'required|boolean']);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        $filters = [];
        if ($request->nombre != null) {
            $filters[] = ['implicadosPerNat_psb', 'LIKE', '%' . $request->nombre . '%'];
        }
        if ($request->numeroDocumento != null) {
            $filters[] = ['numeroDocumento_psb', '=', $request->numeroDocumento];
        }
        if ($request->numeroJuicio != null) {
            $filters[] = ['numeroJuicio_psb', '=', $request->numeroJuicio];
        }
        $headers1 = $code === 'Admin' ? [
            "Num. Providencia"
        ] : [];



        $headers = [
            "Tipo Documento",
            "Num. Documento",
            'Implicado',
            "Fecha",
            "Ciudad",
            "Emisor",
            "Num. Oficio",
            "Tipo Juicio",
            "Num. Juicio",
            "Circular",
            "Acción",
            "Valor",
            "Descargar"
        ];
        $headers = array_merge($headers1, $headers);

        $newRows = [];
        foreach ($selectedRows as $value) {
            if ($value === 'implicados_psb') $newRows[] =  DB::raw('CONCAT(implicadosPerJur_psb, " ", implicadosPerNat_psb, " ", sinDeterImplicado_psb) AS implicados_psb');
            //if ($value === 'fechaCircular_psb') $newRows[] = 'fecha_cin as "fechaCircular_psb"';
            else $newRows[] = $value;
        }
        $newRows[] = DB::raw('COALESCE((SELECT nameFile FROM sb_files sb WHERE  SUBSTRING_INDEX(sb.nameFile, ".", 1) = dts_providencias_sb.circular_psb LIMIT 1), "") file');
        $order = $request->orderBy;
        if ($order === null) {
            if ($code === 'Admin') $order = $code === 'Admin' ? 'fechaArchivo_cin' : 'fechaArchivo_cin';
            else $order = 'fechaArchivo_cin';
        } else if ($order === 'implicados_psb') $order = "implicados_psb " . ($request->isAsc ? 'ASC' : 'DESC');

        $query = ProvidenceSB::select($newRows)
            ->where(function ($query) use ($filters, $request) {
                foreach ($filters as $filter) {
                    $query->where($filter[0], $filter[1], $filter[2]);
                }
                if ($request->nombre != null) {
                    $query->orWhere('implicadosPerJur_psb', 'LIKE', '%' . $request->nombre . '%');
                    $query->orWhere('sinDeterImplicado_psb', 'LIKE', '%' . $request->nombre . '%');
                }
            })->join('dts_carga_insumos', 'dts_carga_insumos.id_cin', '=', 'id_carga_providencia');
        if ($request->orderBy === 'implicados_psb') {
            $query->orderByRaw($order);
        } else $query->orderBy($order, $request->isAsc ? 'ASC' : 'DESC');
        $data = $query->get()->toArray();
        // transform circular_psb for some cases
        if (count($data) > 0 && $code === 'Admin') {
            //code sg
            foreach ($data as &$row) {
                self::updateCircularPsb($row);
            }
        }
        unset($row);

        return response()->json(['data' => $data, 'headers' => $headers, 'orderBy' => $request->orderBy !== null && strpos($request->orderBy, 'implicados_psb') !== false ? 'implicados_psb' : $order], 200);
    }


    public function save(Request $request)
    {

        $user_id = $request->user()->id;
        $validator = Validator::make($request->all(), [
            'archivo' => 'required|mimes:txt|max:5120',
            'nombreArchivo' => 'required',
            'id_tpv' => 'required|integer'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        $fileUploadExist = UploadSupplie::where('nombre_cin', $request->nombreArchivo . '.txt')->first();
        if ($fileUploadExist) throw new Exception('Error. El archivo ' . $request->nombreArchivo . '.txt' . ' ya ha sido cargado al sistema');
        //validar fecha
        $fechaArchivo = explode(' ', $request->nombreArchivo)[0];

        // Create a DateTime object using the specified format
        if (!DateTime::createFromFormat('Y-m-d', $fechaArchivo) || DateTime::createFromFormat('Y-m-d', $fechaArchivo)->format('Y-m-d') != $fechaArchivo) throw new Exception("Error. Fecha de archivo invalida");

        // Read the contents of the file
        $contenidoArchivo = file_get_contents($request->file('archivo')->path());

        // Detect the encoding of the text
        $encoding = mb_detect_encoding($contenidoArchivo, 'UTF-8, ISO-8859-1, Windows-1252', true);

        // Convert the text to UTF-8
        $utf8Contenido = mb_convert_encoding($contenidoArchivo, 'UTF-8', $encoding);

        // Split the content into an array of lines
        $lines = explode(PHP_EOL, $utf8Contenido);
        $dataProvidences = [];
        for ($i = 2; $i < count($lines); $i++) {
            if (trim($lines[$i]) != "") {
                $temp = explode("\t", $lines[$i]);
                $temp[13] = implode(' ', array_slice($temp, 13, 4));

                for ($j = 14; $j < 17; $j++) {
                    unset($temp[$j]);
                }
                $temp = array_values($temp);
                $temp = count($temp) > 18 ? array_splice($temp, 0, 18) : $temp;
                //id_carga_providencia
                array_push($temp, 0);
                //Add data
                $dataProvidences[] = array_map('trim', str_replace('"', ' ', $temp));
            }
        }

        $providenceFields =
            [
                'circular_psb',
                'fechaCircular_psb',
                'tiempoRespuesta_psb',
                'enteEmisor_psb',
                'ciudad_psb',
                'remitente_psb',
                'cargoRemitente_psb',
                'numeroOficio_psb',
                'numeroJuicio_psb',
                'tipoJuicio_psb',
                'accion_psb',
                'valor_psb',
                'implicadosPerJur_psb',
                'implicadosPerNat_psb',
                'sinDeterImplicado_psb',
                'documento_psb',
                'numeroDocumento_psb',
                'observacion_psb',
                'id_carga_providencia',

            ];
        if (count($providenceFields) != count($dataProvidences[0])) throw new Exception('Error. Datos no coinciden con campos tabla');
        $newRowsProvidences = [];
        foreach ($dataProvidences as $dataRow) {
            $row = [];

            foreach ($dataRow as $j => $value) {
                $row[$providenceFields[$j]] = $value;
            }

            $newRowsProvidences[] = $row;
        }

        $now = date("Y-m-d H:i:s");
        $now = explode(" ", $now);
        $newSupply = [
            'fecha_cin' => $now[0],
            'hora_cin'  => $now[1],
            'nombre_cin' => $request->nombreArchivo . '.txt',
            'fechaArchivo_cin' => $fechaArchivo,
            'tipoInsumo_cin' => "SB",
            'id_usr' => $user_id,
            'id_tpv' => $request->id_tpv
        ];

        // Init transaccion
        DB::beginTransaction();
        try {
            // insert in supplies
            $insertedId = DB::table('dts_carga_insumos')->insertGetId($newSupply);
            foreach ($newRowsProvidences as &$dataRow) {
                $dataRow['id_carga_providencia'] = $insertedId;
                $dataRow['tipoJuicio_psb'] = $dataRow['tipoJuicio_psb'];
                $dataRow['numeroJuicio_psb'] = $dataRow['numeroJuicio_psb'];
            }
            unset($dataRow);

            $newRowsProvidences = collect($newRowsProvidences);
            $chuncks = $newRowsProvidences->chunk(500);
            foreach ($chuncks as $key => $value) {
                DB::table('dts_providencias_sb')->insert($value->toArray());
            }
            //DB::table('dts_providencias_sb')->insert($dataRow);
            //DB::table('dts_providencias_sb')->insert($newRowsProvidences);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['message' => "Proceso SB correcto, se han registrado " . count($dataProvidences) . " registros."], 202);
    }

    public function controlById(Request $request)
    {


        $supply = UploadSupplie::where('id_cin', $request['id'])->first();
        if ($supply) {
            $tableHeaders = [
                'circular_psb' => ["Circular"],
                'fechaCircular_psb' => ["Fecha"],
                'tiempoRespuesta_psb' => ["Tiempo Respuesta"],
                'enteEmisor_psb' => ["Emisor"],
                'ciudad_psb' => ["Ciudad"],
                'remitente_psb' => ["Remitente"],
                'cargoRemitente_psb' => ["Cargo Remitente"],
                'numeroOficio_psb' => ["Num. Oficio"],
                'numeroJuicio_psb' => ["Num. Juicio"],
                'tipoJuicio_psb' => ["Tipo Juicio"],
                'accion_psb' => ["Acción"],
                'valor_psb' => ["Valor"],
                'documento_psb' => ["Tipo Documento"],
                'numeroDocumento_psb' => ["Num. Documento"],
                'observacion_psb' => ["Observacion"],
                'implicadosPerJur_psb' => ["Implicado Persona Juridica"],
                'implicadosPerNat_psb' => ["Implicado Persona Natural"],
                'sinDeterImplicado_psb' => ["Implicado Sin Determinación"],
            ];

            $last10Rows = ProvidenceSB::select(array_keys($tableHeaders))->where('id_carga_providencia', $request['id'])->limit(10)->get();

            return response()->json(['data' => $last10Rows], 200);
        } else {
            throw new ModelNotFoundException('Error. No existe el archivo solicitado', 404);
        }
    }

    public function deleteUpload(Request $request)
    {
        $supply = UploadSupplie::where('id_cin', $request['id'])->first();
        if ($supply) {

            try {
                ProvidenceSB::where('id_carga_providencia', $request['id'])->delete();
                $supply->delete();
            } catch (Exception $e) {
                return response()->json(['message' => $e->getMessage()], 500);
            }

            return response()->json(['message' => 'Correcto'], 202);
        } else {
            throw new ModelNotFoundException('Error. No existe el archivo solicitado', 404);
        }
    }


    public function store(Request $request)
    {
        set_time_limit(60 * 5);
        $validator = Validator::make($request->all(), [
            'files.*' => 'required|required|mimes:txt,pdf,zip,rar,ppt,pptx,doc,docx,xls,xlsx,7zip,tar|max:512000',
        ]);


        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        $files = $request->file('files');

        if (!Storage::exists(self::SB_DIR)) Storage::makeDirectory(self::SB_DIR);
        $newFilesUploaded = 0;
        $username = $request->user()->username;

        foreach ($files  as $file) {
            $originalName = $file->getClientOriginalName();
            $path = self::SB_DIR . "/" .  $originalName;
            if (!Storage::exists($path)) {
                $file->storeAs(self::SB_DIR, $originalName);
                $newFilesUploaded++;
                SBFile::create([
                    'nameFile' => $originalName,
                    'created_by' => $username,
                    'created_at' => date("Y-m-d H:i:s"),
                ]);
            }
        }


        return response()->json(['message' => $newFilesUploaded > 0 ? ('Se ha subido: #' .  $newFilesUploaded . '  se ha subido correctamente') : 'No se han subido nuevos archivos'], 202);
    }

    /*public function store(Request $request)
    {
        set_time_limit(60 * 5);
        $validator = Validator::make($request->all(), [
            'files.*' => 'required|required|mimes:txt,pdf,zip,rar,ppt,pptx,doc,docx,xls,xlsx,7zip,tar|max:512000',
        ]);


        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        $files = $request->file('files');

        if (!Storage::exists(self::SB_DIR)) Storage::makeDirectory(self::SB_DIR);

        $username = $request->user()->username;

        UploadFilesSB::dispatch($files, $username);

        return response()->json(['message' => 'Se han subido los archivos SB correctamente'], 202);
    }*/


    public function getSBDocs($code, $ext)
    {

        // Construct the full path to the file in the subdirectory
        $filePath = self::SB_DIR . '/' . $code . '.' . $ext;
        if (Storage::disk('local')->exists($filePath)) {
            // return response()->download($filePath, $code . '.pdf', ['Content-Type: application/pdf']);

            $extension = pathinfo($code . '.' . $ext, PATHINFO_EXTENSION);
            $contentType = array_key_exists($extension, self::FILE_EXTENSIONS) ? self::FILE_EXTENSIONS[$extension] :  'application/octet-stream';
            return Storage::download($filePath, $code, ['Content-Type: ' . $contentType]);
        } else {
            // File does not exist, throw a 404 exception
            throw new NotFoundResourceException('Error. Documento sb no encontrado', 404);
        }
    }

    public function syncFiles(Request $request)
    {
        $job = Job::first();
        if ($job) throw new Exception('Hay un proceso de sincronización actualmente, intentalo más tarde');
        $archivos = Storage::files(self::SB_UPLOADS);
        $nombresArchivos = array_map('basename', $archivos);
        $sbFiles = SBFile::whereIn('nameFile', $nombresArchivos)->pluck('nameFile')->toArray();
        $nombresNotInDatabase = array_diff($nombresArchivos, $sbFiles);
        if (count($nombresNotInDatabase) > 0) {
            $username = $request->user()->username;
            ProcessFilesJob::dispatch($nombresNotInDatabase, $username);
            return response()->json(['message' => 'Se sincronizaran: ' . count($nombresNotInDatabase) . ' archivos'], 202);
        } else {
            throw new Exception('Error. no hay archivos para sincronizar');
        }
    }

    private function updateCircularPsb(&$row)
    {
        $circularPsb = $row['circular'];
        if (strpos($circularPsb, 'E') !== false) {
            return;
        }
        $elementsCode = explode("-", $circularPsb);

        if (strpos($circularPsb, 'SB-SG') !== false && count($elementsCode) >= 5) {
            $circularPsb = count($elementsCode) === 6 ? $elementsCode[2] . "-" . $elementsCode[3] . $elementsCode[5]  : $elementsCode[2] . "-" . $elementsCode[3];
        } elseif (strpos($circularPsb, 'SB-SR') !== false) {
            $circularPsb = 'SR-' . $elementsCode[2] . "-" . $elementsCode[3];
        } elseif (strpos($circularPsb, 'SB-IRC') !== false) {
            $dateCircular = substr($elementsCode[2], 2, 2);
            $circularPsb = count($elementsCode) === 5 ? 'IRC-' . $dateCircular . "-" . $elementsCode[3] . "-" . $elementsCode[4]  : 'IRC-' . $dateCircular . "-" . $elementsCode[3];
        } elseif (strpos($circularPsb, 'SB-IRP') !== false) {
            $circularPsb = 'IRP' . $elementsCode[2] . $elementsCode[3];
        } elseif (strpos($circularPsb, 'IRG') !== false) {
            $circularPsb = 'IRG' . "-" . $elementsCode[1] . "-" . $elementsCode[2];
        } elseif (strpos($circularPsb, 'IRC') !== false) {
            $circularPsb = 'IRC' . "-" . $elementsCode[1] . "-" . $elementsCode[2];
        }
        $file = SBFile::select('nameFile')->whereRaw("SUBSTRING_INDEX(nameFile, '.', 1) = ?", [$circularPsb])->first();
        if ($file) $row['file'] = $file->nameFile;
        $row['circular'] = $circularPsb;
    }


    public function testSync()
    {
        $pseCodes = ProvidenceSB::distinct('circular_psb')->get()->toArray();
        foreach ($pseCodes as &$code) {
            self::updateCircularPsb($code);
            $fileName = $code['circular_psb'] . '.txt';
            $content = $code['circular_psb'];
            Storage::disk('local')->put('SB_DOCS/' . $fileName, $content);
        }
        unset($code);
        return response()->json(['message' => 'Se registraron: ' . count($pseCodes)], 202);
    }
}
