<?php

namespace App\Http\Controllers;

use App\Models\PortalFile;
use App\Models\PortalFileDetail;
use App\Models\UserDownloadLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserDownloadController extends Controller
{
    public function index(Request $request)
    {
        $selectedRows = [
            'users_downloads_logs.id',
            'users.lastname',
            'users.name',
            'users.status',
            'users.financialInstitution',
            'dts_archivos.descripcion_arc',
            'users_downloads_logs.dta_detail',
            'users_downloads_logs.download_at'
        ];
        $pagination = config('constants.pagination');
        $validator = Validator::make($request->all(), [
            'dateBegin' => 'required|date',
            'dateEnd' => 'required|date',
            'user_id' => 'numeric',
            'arc_id' => 'numeric',
            'orderBy' => Rule::in(array_map(function ($row) {
                return explode('.', $row)[1];
            }, $selectedRows))
        ] + $pagination);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }

        $filters = [];
        if ($request->user_id != null) $filters[] = ['users_downloads_logs.user_id', '=', $request->user_id];
        if ($request->arc_id != null) $filters[] = ['dts_archivos.id_arc', '=', $request->arc_id];


        /*$dateBegin = Carbon::parse($request->dateBegin);
        $dateEnd = Carbon::parse($request->dateEnd);*/
        $query = UserDownloadLog::select($selectedRows)
            ->whereBetween('users_downloads_logs.download_at', [$request->dateBegin . ' 00:00:00', $request->dateEnd . ' 23:59:59'])
            ->orderBy($request->orderBy ?? 'users_downloads_logs.download_at', $request->isAsc ? 'ASC' : 'DESC')
            ->where($filters)
            /*->join('dts_detalle_archivos', 'users_downloads_logs.dta_id', '=', 'dts_detalle_archivos.id_dta')*/
            ->join('dts_archivos', 'dts_archivos.id_arc', '=', 'users_downloads_logs.arc_id')
            ->join('users', 'users_downloads_logs.user_id', '=', 'users.id');
        if ($request->search != null) {
            $query->OrWhere('users.lastname', $request->search);
            $query->OrWhere('users.name', $request->search);
            $query->OrWhere('financialInstitution', $request->search);
        }
        $data = $query->paginate($request->pageLimit);
        $nextPageUrl = $data->nextPageUrl();

        return response()
            ->json([
                'data' => $data->items(),
                'currentPage' => $data->currentPage(),
                'perPage' => $data->perPage(),
                'total' => $data->total(),
                'nextPage' => $nextPageUrl ? intval(explode('=', $nextPageUrl)[1]) : null,
                'orderBy' => $request->orderBy ?? 'download_at'
            ], 200);
    }

    public function getDownloaded(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        $user_id = $request->user()->id;
        /*$data = PortalFileDetail::select([
            'dts_archivos.descripcion_arc as name',
            'dts_detalle_archivos.fechaArchivo_dta as date',
            DB::raw('COALESCE((SELECT id FROM users_downloads_logs udl WHERE udl.dta_id = dts_detalle_archivos.id_dta AND udl.date_file = dts_detalle_archivos.fechaArchivo_dta LIMIT 1), 0) as wasDownloaded')
        ])
            ->join('dts_archivos', 'dts_archivos.id_arc', '=', 'dts_detalle_archivos.id_arc')
            ->join('users_allowed_files', 'users_allowed_files.arc_id', '=', 'dts_archivos.id_arc')
            ->where('users_allowed_files.user_id', $user_id)
            ->where('dts_detalle_archivos.fechaArchivo_dta', $request->date)
            ->orderByRaw("CONVERT(dts_detalle_archivos.fechaArchivo_dta, DATE) ASC")
            ->get();*/
        /*$dates = PortalFileDetail::select(['fechaArchivo_dta'])
            ->join('users_allowed_files', 'users_allowed_files.arc_id', '=', 'dts_archivos.id_arc')
            ->join('dts_detalle_archivos', 'dts_detalle_archivos.id_arc', '=', 'dts_archivos.id_arc')
            ->where('users_allowed_files.user_id', $user_id)->get()->toArray();*/

        /*$dates = PortalFile::select(['fechaArchivo_dta'])
            ->join('users_allowed_files', 'users_allowed_files.arc_id', '=', 'dts_archivos.id_arc')
            ->join('dts_detalle_archivos', 'dts_detalle_archivos.id_arc', '=', 'dts_archivos.id_arc')
            ->where('users_allowed_files.user_id', $user_id)->get()->toArray();*/


        $ids = UserDownloadLog::select(['dta_id'])->where('user_id', $user_id)->whereBetween('download_at', [now()->subDays(30), now()])->get()->toArray();

        $data = PortalFile::select(['descripcion_arc', 'dts_detalle_archivos.fechaArchivo_dta'])
            ->join('users_allowed_files', 'users_allowed_files.arc_id', '=', 'dts_archivos.id_arc')
            ->join('dts_detalle_archivos', 'dts_detalle_archivos.id_arc', '=', 'dts_archivos.id_arc')
            ->where('users_allowed_files.user_id', $user_id)
            //->where('dts_detalle_archivos.fechaArchivo_dta', $request->date)
            ->whereNotIn('dts_detalle_archivos.id_dta', $ids)->orderBy('dts_detalle_archivos.fechaArchivo_dta', 'ASC')->get();

        return response()
            ->json([
                'data' => $data
            ], 200);
    }
}
