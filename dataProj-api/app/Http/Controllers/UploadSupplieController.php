<?php

namespace App\Http\Controllers;

use App\Models\ProvidenceSB;
use App\Models\ProvidenceSEPS;
use App\Models\UploadSupplie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UploadSupplieController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'dateBegin' => 'required|date',
            'dateEnd' => 'required|date',
            'typeProvidence' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json($errors, 500);
        }
        $from = date($request->dateBegin . ' 00:00:00');
        $to = date($request->dateEnd . ' 23:59:59');
        $data = UploadSupplie::with('typeProvidence')->with(['user' => function ($query) {
            $query->select('id', 'username');
        }])->whereBetween('fecha_cin', [$from, $to]);
        if (intval($request->typeProvidence) != 0) $data = $data->where('tipoInsumo_cin', intval($request->typeProvidence) === 1 ? 'SEPS' : 'SB');
        $data = $data->get();
        return response()
            ->json([
                'data' => $data,
            ]);
    }

    /*public function getFistTenRow(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_tpv' => 'required|integer',
            'id_carga_providencia' => 'required|integer'
        ]);
        $data = [];
        if($request->id_tpv == '1'){
            $data = ProvidenceSEPS::where('id_carga_providencia', $request->id_carga_providencia)->take(10)->get();
        }else {
            $data = ProvidenceSB::where('id_carga_providencia', $request->id_carga_providencia)->take(10)->get();
        }
        


    }*/
}
