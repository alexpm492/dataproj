<?php

namespace App\Jobs;

use App\Models\SBFile;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class UploadFilesSB implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $files;
    protected $username;
    private const SB_DIR = 'SB_DOCS';
    public function __construct($files, $username)
    {
        $this->files = $files;
        $this->username = $username;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        foreach ($this->files  as $file) {
            $originalName = $file->getClientOriginalName();
            $path = self::SB_DIR . "/" .  $originalName;
            if (!Storage::exists($path)) {
                $file->storeAs(self::SB_DIR, $originalName);
                SBFile::create([
                    'nameFile' => $originalName,
                    'created_by' => $this->username,
                    'created_at' => now(),
                ]);
            }
        }
    }
}
