<?php

namespace App\Jobs;

use App\Models\SBFile;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ProcessFilesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    protected $files;
    protected $username;
    private const SB_DIR = 'SB_DOCS';
    private const SB_UPLOADS = 'SB_UPLOADS';

    public function __construct($files, $username)
    {
        $this->files = $files;
        $this->username = $username;
    }

    public function handle()
    {
        $collectionFiles = collect($this->files);
        $chunks = $collectionFiles->chunk(500);
        /*foreach ($this->files as $value) {
            Storage::move(self::SB_UPLOADS . '/' . $value, self::SB_DIR . '/' . $value);
            SBFile::create([
                'nameFile' => $value,
                'created_by' => $this->username,
                'created_at' => now(),
            ]);
        }*/
        foreach ($chunks as $key => $group) {
            foreach ($group as $value) {
                Storage::move(self::SB_UPLOADS . '/' . $value, self::SB_DIR . '/' . $value);
                SBFile::create([
                    'nameFile' => $value,
                    'created_by' => $this->username,
                    'created_at' => now(),
                ]);
            }
        }
    }
}
