<?php

namespace App\Jobs;

use App\Models\SEPSFile;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class UploadFilesSEPS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    protected $fileDetails;
    protected $username;
    private const SEPS_DIR = 'sepsPdf';

    public function __construct($fileDetails, $username)
    {
        $this->fileDetails = $fileDetails;
        $this->username = $username;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        foreach ($this->fileDetails as $fileDetails) {
            $originalName = $fileDetails['originalName'];
            $path = $fileDetails['path'];
            $contents = base64_decode($fileDetails['contents']); // Decode file contents

            if (!Storage::exists($path)) {
                Storage::put($path, $contents);

                SEPSFile::create([
                    'circular' => pathinfo($path, PATHINFO_FILENAME),
                    'nameFile' => $originalName,
                    'created_by' => $this->username,
                    'created_at' => now(),
                ]);
            }
        }
    }
}
