<?php
return [
    'pagination' => [
        'page' => 'required|integer',
        'pageLimit' => 'required|integer',
        'isAsc' => 'required|boolean',
        'search' => 'min:1'
    ]
];
